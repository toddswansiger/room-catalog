//
//  SPGAppDelegate.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/15/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KSReachability.h"

@class KSReachability;

@interface SPGAppDelegate : UIResponder <UIApplicationDelegate, JLCoreDataManagedObjectDataSource, JLTaskManagerErrorHandlingDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic, readonly) NSString *versionDescriptionString;
@property (assign, nonatomic) SPGEnvironment environment;
@property (strong, nonatomic, readonly) NSString *environmentURL;
@property (strong, nonatomic, readonly) NSString *environmentAPIKey;
@property (strong, nonatomic, readonly) NSString *environmentSecretKey;
@property (strong, nonatomic) KSReachability *reachability;
@property (strong, nonatomic) UILocalNotification *authenticationExpiredNotification;
@property (strong, nonatomic) UILocalNotification *authenticationWillExpireWarningNotification;
@property (strong, nonatomic) UILocalNotification *syncDataWillExpireNotification;

- (NSManagedObjectContext*)managedObjectContext;
- (void)scheduleTokenExpirationUserNotificationWithExpirationDate:(NSDate *)expiration;
- (void)scheduleSyncDataExpirationNotificationWithExpirationDate:(NSDate *)expiration;
- (void)handleApplicationUpdateRequest;

@end
