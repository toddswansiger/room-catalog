//
//  SPGAuthenticationManager.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/4/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "JLTaskManager.h"

@class SPGUser;

@interface SPGAuthenticationManager : JLTaskManager

@property (nonatomic, strong, readonly) NSString* userID;
@property (nonatomic, strong, readonly) NSString* password;
@property (nonatomic, strong, readonly) NSString* token;
@property (nonatomic, strong, readonly) NSDate* expireDate;

+ (void)authenticateUser:(NSString*)user withPassword:(NSString*)password block:(JLTaskObserverBlock)block;
+ (void)logout:(JLTaskObserverBlock)block;

- (BOOL)isUserAuthenticated;
+ (BOOL)isAuthenticationRunning;
- (void)setToken:(NSString*)token tokenStatus:(NSString*)tokenStatus tokenExpireDate:(NSDate*)expireDate forUser:(NSString*)user;

+ (SPGUser *)user;
+ (NSString *)currentUserName;
+ (BOOL)isNewVersionAvailable;
+ (void)setNewVersionAvailable:(BOOL)newVersionAvailable;

@end
