//
//  SPGPropertyManager.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/17/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "JLTaskManager.h"

@class SPGProperty, SPGUser;

typedef enum {
    SPGRoomUpdateType_Unknown = 0,
    SPGRoomUpdateType_Add,
    SPGRoomUpdateType_Update,
    SPGRoomUpdateType_Delete,
    SPGRoomUpdateType_Flag
}SPGRoomUpdateType;

@interface SPGPropertyManager : JLTaskManager

+ (BOOL)hasDownloadedPropertyData;
+ (void)setHasDownloadedPropertyData;
+ (void)getPropertiesForUserWithInfo:(SPGUser*)userInfo block:(JLTaskObserverBlock)block;
+ (void)getRoomFeaturesForPropertyWithInfo:(SPGProperty*)propInfo block:(JLTaskObserverBlock)block;
+ (void)updateRoom:(SPGRoomSummary*)room withAttribute:(SPGAttributeCategory*)attribute mapping:(SPGAttributeMapping*)mapping updateType:(SPGRoomUpdateType)updateType block:(JLTaskObserverBlock)block;
+ (void)cancelGetRoomFeaturesForPropertyWithInfo:(SPGProperty*)propInfo;
+ (void)flagRoom:(SPGRoomSummary*)room block:(JLTaskObserverBlock)block;
+ (JLTaskObserver*)addGenericObserverForUpdateRoomWithBlock:(JLTaskObserverBlock)block;

@end
