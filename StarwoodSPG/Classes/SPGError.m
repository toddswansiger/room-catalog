//
//  SPGError.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/4/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGError.h"

NSString const * kSPGErrorMsg = @"SPGErrorMsg";
NSString const * kSPGErrorCode = @"SPGErrorCode";
NSString const * kSPGErrorDomain = @"StarwoodSPG";


@implementation SPGError

+ (instancetype)errorWithCode:(NSString*)code message:(NSString*)message
{
    SPGError* error = [[self class] errorWithDomain:[NSString stringWithFormat:@"%@", kSPGErrorDomain] code:code.intValue userInfo:@{kSPGErrorMsg : message, kSPGErrorCode : code}];
    
    [[SPGAnalyticsManager sharedManager] logEvent:@"Error" withParameters:@{
                                                                            @"Code": code,
                                                                            @"Message" : message
                                                                            }];
    
    return error;
}

- (NSString*)spgErrorCode
{
    return self.userInfo[kSPGErrorCode];
}

- (NSString*)spgErrorMessage
{
    return self.userInfo[kSPGErrorMsg];
}

- (NSString*)spgErrorDomain
{
    return self.userInfo[kSPGErrorDomain];
}
@end
