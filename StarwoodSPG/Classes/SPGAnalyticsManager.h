//
//  SPGAnalyticsManager.h
//  StarwoodSPG
//
//  Created by Eric Appel on 6/30/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPGAnalyticsManager : NSObject

+ (SPGAnalyticsManager *)sharedManager;

- (void)logEvent:(NSString *)event;
- (void)logEvent:(NSString *)event withParameters:(NSDictionary *)params;

- (void)setStartingPercentageComplete:(CGFloat)percentage forPropertyID:(NSString *)propertyID;
- (void)updateCompletedPercentage:(CGFloat)percentage forPropertyID:(NSString *)propertyID;
- (void)logPercentageEvents;

@end
