//
//  SPGError.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/4/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString const * kSPGErrorMsg;
extern NSString const * kSPGErrorCode;
extern NSString const * kSPGErrorDomain;

@interface SPGError : NSError
+ (instancetype)errorWithCode:(NSString*)code message:(NSString*)message;
- (NSString*)spgErrorCode;
- (NSString*)spgErrorMessage;
- (NSString*)spgErrorDomain;
@end
