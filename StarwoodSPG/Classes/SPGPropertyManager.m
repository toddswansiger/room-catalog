//
//  SPGPropertyManager.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/17/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGPropertyManager.h"
#import "SPGAppDelegate.h"

static NSString *DidDownloadPropertyDataKey = @"com.spg.didDownloadData";

@implementation SPGPropertyManager

+ (NSString *)environmentURL
{
    SPGAppDelegate *delegate = (SPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *environmentURL = [delegate environmentURL];
    return environmentURL;
}

+ (NSString *)environmentAPIKey
{
    SPGAppDelegate *delegate = (SPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *environmentKey = [delegate environmentAPIKey];
    return environmentKey;
}

+ (NSString *)environmentSecretKey
{
    SPGAppDelegate *delegate = (SPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *environmentKey = [delegate environmentSecretKey];
    return environmentKey;
}

+ (BOOL)hasDownloadedPropertyData
{
    BOOL didDownload = [[NSUserDefaults standardUserDefaults] boolForKey:DidDownloadPropertyDataKey];
    return didDownload;
}

+ (void)setHasDownloadedPropertyData
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:DidDownloadPropertyDataKey];
}

+ (void)getPropertiesForUserWithInfo:(SPGUser*)userInfo block:(JLTaskObserverBlock)block
{
    NSString* tokenStr = [SPGAuthenticationManager sharedInstance].token ?: kDefaultToken;
    NSString* user = [SPGAuthenticationManager sharedInstance].userID;
    
    //sample service endpoint: https://api3.qa.starwoodhotels.com/associate/property/roomFeaturesHierarchy/1010?apiKey=95E3893EDDC243FE&userToken=TOKEN
    
    
    NSDictionary* taskInfo = @{@"envUrl" : [SPGPropertyManager environmentURL],
                               @"apiKey" : [SPGPropertyManager environmentAPIKey],
                               @"secretKey" : [SPGPropertyManager environmentSecretKey],
                               @"userToken" : tokenStr ?: @"",
                               @"user" : user ?: @""};
    
    [self runTaskWithName:@"getProperties" taskInfo:taskInfo observerBlock:block];
}

+ (void)getRoomFeaturesForPropertyWithInfo:(SPGProperty*)propInfo block:(JLTaskObserverBlock)block
{
    NSString* propertyId = propInfo.propertyId.copy;
    NSString* token = [SPGAuthenticationManager sharedInstance].token;
    
    //sample service endpoint: https://api3.qa.starwoodhotels.com/associate/property/roomFeaturesHierarchy/1010?apiKey=95E3893EDDC243FE&userToken=TOKEN
    
    NSDictionary* taskInfo = @{@"envUrl" : [SPGPropertyManager environmentURL],
                               @"propertyId" : propertyId ?: @"",
                               @"apiKey" : [SPGPropertyManager environmentAPIKey],
                               @"secretKey" : [SPGPropertyManager environmentSecretKey],
                               @"userToken" : token ?: @"TOKEN"};

    [self runTaskWithName:@"getRoomFeatures" taskInfo:taskInfo observerBlock:block];
}

+ (void)cancelGetRoomFeaturesForPropertyWithInfo:(SPGProperty*)propInfo
{
    NSString* taskName = @"getRoomFeatures";
    [self interruptTaskWithName:taskName];
}

+ (void)updateRoom:(SPGRoomSummary*)room withAttributes:(NSArray*)attributes updateType:(SPGRoomUpdateType)updateType block:(JLTaskObserverBlock)block
{
    NSString* tokenStr = [SPGAuthenticationManager sharedInstance].token ?: kDefaultToken;
    NSArray* jsonAttributes = [attributes valueForKeyPath:@"jsonValue"];
    NSDictionary* taskInfo = @{@"attributes" : jsonAttributes ?: @[],
                               @"room" : room.jsonValue ?: @{},
                               @"updateType" : @(updateType),
                               @"envUrl" : [SPGPropertyManager environmentURL],
                               @"apiKey" : [SPGPropertyManager environmentAPIKey],
                               @"secretKey" : [SPGPropertyManager environmentSecretKey],
                               @"propertyId" : room.propertyId,
                               @"userToken" : tokenStr ?: @""
                               };
    
    [self runTaskWithName:@"updateRoomFeatures" taskInfo:taskInfo observerBlock:block];
}

+ (void)updateRoom:(SPGRoomSummary*)room withAttribute:(SPGAttributeCategory*)attribute mapping:(SPGAttributeMapping*)mapping updateType:(SPGRoomUpdateType)updateType block:(JLTaskObserverBlock)block
{
    NSString* tokenStr = [SPGAuthenticationManager sharedInstance].token ?: kDefaultToken;
    NSDictionary* taskInfo = @{@"attribute" : attribute.jsonValue ?: @{},
                               @"mapping" : mapping.jsonValue  ?: @{},
                               @"room" : room.jsonValue  ?: @{},
                               @"propertyId" : room.propertyId ?: @"",
                               @"updateType" : @(updateType),
                               @"envUrl" : [SPGPropertyManager environmentURL],
                               @"apiKey" : [SPGPropertyManager environmentAPIKey],
                               @"secretKey" : [SPGPropertyManager environmentSecretKey],
                               @"userToken" : tokenStr ?: @""
                               };

    [self runTaskWithName:@"updateRoomFeatures" taskInfo:taskInfo observerBlock:block];
}

+ (void)flagRoom:(SPGRoomSummary*)room block:(JLTaskObserverBlock)block
{
    [self updateRoom:room withAttribute:nil mapping:nil updateType:SPGRoomUpdateType_Flag block:block];
}

+ (JLTaskObserver*)addGenericObserverForUpdateRoomWithBlock:(JLTaskObserverBlock)block
{
    NSString* taskName = @"updateRoomFeatures";
    return [self addGenericObserverForTaskWithName:taskName observerBlock:block];
}

@end
