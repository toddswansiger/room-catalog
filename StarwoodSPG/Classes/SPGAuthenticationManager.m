//
//  SPGAuthenticationManager.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/4/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGAuthenticationManager.h"
#import "SPGUser+Helper.h"
#import "SPGAppDelegate.h"
#import "SPGContstants.h"

#define kSPGVersionAvailableKey @"SPGVersionAvailableKey"

@interface SPGAuthenticationManager ()

@property (nonatomic, strong, readwrite) NSString* userID;
@property (nonatomic, strong, readwrite) NSString* password;
@property (nonatomic, strong, readwrite) NSString* token;
@property (nonatomic, strong, readwrite) NSDate* expireDate;

@end

@implementation SPGAuthenticationManager

+ (NSString *)environmentURL
{
    SPGAppDelegate *delegate = (SPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *environmentURL = [delegate environmentURL];
    return environmentURL;
}

+ (NSString *)environmentAPIKey
{
    SPGAppDelegate *delegate = (SPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *environmentKey = [delegate environmentAPIKey];
    return environmentKey;
}

+ (NSString *)environmentSecretKey
{
    SPGAppDelegate *delegate = (SPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *environmentKey = [delegate environmentSecretKey];
    return environmentKey;
}

+ (void)authenticateUser:(NSString*)user withPassword:(NSString*)password block:(JLTaskObserverBlock)block
{
    NSDictionary* taskInfo = @{@"envUrl" : [SPGAuthenticationManager environmentURL],
                               @"apiKey" : [SPGAuthenticationManager environmentAPIKey],
                               @"secretKey" : [SPGAuthenticationManager environmentSecretKey],
                               @"user" : user,
                               @"password" : password
                               };

    [self runTaskWithName:@"authenticateUser" taskInfo:taskInfo observerBlock:block];
}

- (void)setToken:(NSString*)token tokenStatus:(NSString*)tokenStatus tokenExpireDate:(NSDate*)expireDate forUser:(NSString*)user
{
    if(user.length && token.length)
    {
        self.userID = user;
        self.token = token;
        self.expireDate = expireDate;
    }
}

- (BOOL)isUserAuthenticated
{
    return (self.token.length && [self.expireDate timeIntervalSinceNow] > 0);
}

+ (BOOL)isAuthenticationRunning
{
    NSURL* taskUrl = [self taskUrlWithName:@"authenticateUser"];
    return [self isTaskInQueueWithURL:taskUrl taskInfo:nil];
}

+ (void)logout:(JLTaskObserverBlock)block
{
    [[SPGAnalyticsManager sharedManager] logPercentageEvents];

    __weak SPGAuthenticationManager* weakSelf = [SPGAuthenticationManager sharedInstance];
    
    NSDictionary* taskInfo = @{
                               @"envUrl" : [SPGAuthenticationManager environmentURL],
                               @"apiKey" : [SPGAuthenticationManager environmentAPIKey],
                               @"secretKey" : [SPGAuthenticationManager environmentSecretKey],
                               @"user" : weakSelf.userID ?: @"",
                               };
    
    [SPGAuthenticationManager runTaskWithName:@"logoutUser" taskInfo:taskInfo observerBlock:^(JLTaskStatus taskStatus, NSDictionary *info, NSError *error) {
        if(taskStatus == JLTaskStatus_Finished)
        {
            [[UIApplication sharedApplication] cancelAllLocalNotifications];
            weakSelf.token = nil;
            weakSelf.userID = nil;
            weakSelf.password = nil;
            weakSelf.expireDate = nil;
            

        }
        
        if(block)
            block(taskStatus, info, error);
    }];
}

+ (SPGUser*)user
{
    SPGAuthenticationManager* authManager = [SPGAuthenticationManager sharedInstance];
    SPGUser* user = nil;
    
    if (authManager.userID.length)
    {
        NSManagedObjectContext* moc = [JLDataManager newManagedObjectContext];
        user = (SPGUser *)[JLDataManager firstEntityForName:[[SPGUser class] description] withPredicate:[NSPredicate predicateWithFormat:@"userID == %@", authManager.userID] inManagedObjectContext:moc];
    }
    
    return user;
}

+ (NSString *)currentUserName
{
    SPGUser *currentUser = [SPGAuthenticationManager user];
    NSString *firstName = [currentUser firstName] ?: @"";
    NSString *lastName = [currentUser lastName] ?: @"";
    NSString *name = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
    return name;
}

+ (BOOL)isNewVersionAvailable
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSNumber* versionAvailableNum = [defaults objectForKey:kSPGVersionAvailableKey];
    return versionAvailableNum.boolValue;
}

+ (void)setNewVersionAvailable:(BOOL)newVersionAvailable
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@(newVersionAvailable) forKey:kSPGVersionAvailableKey];
    if (newVersionAvailable)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:SPGAPIKeyDeprecatedNotification object:nil];
    }
}

@end
