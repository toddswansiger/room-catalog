//
//  SPGManagers.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/4/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

typedef NS_ENUM (NSUInteger, SPGEnvironment)
{
    SPGEnvironmentQA,
    SPGEnvironmentStaging,
    SPGEnvironmentProduction
};

// forcing QA build
#define QA_BUILD 0

// QA

#define kQAEnvironmentURL @"api3.qa.starwoodhotels.com"
//#define kQAAPIKey @"d8sn39dngal10vmd7xznd8xnmdo92mx9" //Old key without changes
#define kQAAPIKey @"d80ss21ienjcn97439i217r34cb3r8z2" //New key for HMAC-SHA1 changes
#define kQASecretKey @"zlHWF6JYvy/Zc8Rk6ZRDZIHg3Po=IG45jfi3io2T" //HMAC-SHA1 signature key

// STAGING
#define kStagingEnvironmentURL @"stg-api.starwoodhotels.com"
//#define kStagingAPIKey @"d8sn39dngal10vmd7xznd8xnmdo92mx9"
#define kStagingAPIKey @"d80ss21ienjcn97439i217r34cb3r8z2"
#define kStagingSecretKey @"zlHWF6JYvy/Zc8Rk6ZRDZIHg3Po=IG45jfi3io2T"
// PRODUCTION

#define kProductionEnvironmentURL @"api.starwoodhotels.com"
#define kProductionAPIKey @"d8psdvjhsdpkvjh0ldjk240dvk2hkolq"
//#define kProductionSecretKey @"zlHWF6JYvy/Zc8Rk6ZRDZIHg3Po=IG45jfi3io2T"
#define kProductionSecretKey @"jdjfdiAy/Yidl0j276kf5mfi=je8*hdhjd83aste"

#define kDefaultToken @"TOKEN"

// FLURRY
#define kFlurryAPIKey @"GFFM5QVBJW276C77VP6G"

#import "SPGError.h"
#import "SPGPropertyManager.h"
#import "SPGAuthenticationManager.h"
#import "SPGStyleManager.h"
#import "SPGSyncManager.h"
#import "SPGAnalyticsManager.h"
