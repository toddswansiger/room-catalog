//
//  SPGAnalyticsManager.m
//  StarwoodSPG
//
//  Created by Eric Appel on 6/30/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGAnalyticsManager.h"
#import "SPGManagers.h"
#import "Flurry.h"

static const BOOL kPrintEvents = NO;

static UIBackgroundTaskIdentifier BackgroundUploadIdentifier;

@interface SPGAnalyticsManager ()

/**
 *  Completion percentages for each property the user updates
 *  @{
 *      propertyID : @{
 *                        @"startingPercent" : [NSNumber numberWithFloat],
 *                        @"completedPercent" : [NSNumber numberWithFloat]
 *                    }
 *  }
 */
@property (nonatomic, strong) NSMutableDictionary *propertyCompletionPercentageDictionary;


@end

@implementation SPGAnalyticsManager

+ (SPGAnalyticsManager *)sharedManager
{
    static SPGAnalyticsManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[SPGAnalyticsManager alloc] init];
    });
    return manager;
}

- (id)init
{
    self = [super init];
    if (self) {
        _propertyCompletionPercentageDictionary = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)logEvent:(NSString *)event
{
    [Flurry logEvent:event];
    
    if (kPrintEvents) {
        [self printEvent:event withParams:nil];
    }
}

- (void)logEvent:(NSString *)event withParameters:(NSDictionary *)params
{
    [Flurry logEvent:event withParameters:params];
    
    if (kPrintEvents) {
        [self printEvent:event withParams:params];
    }
}

- (void)printEvent:(NSString *)event withParams:(NSDictionary *)params
{
    NSString *divider = @"=========================================================================================";
    if (params) {
        NSLog(@"\n%@\n\n %@ \n %@\n\n%@", divider, event, params, divider);
    } else {
        NSLog(@"\n%@\n\n %@ \n\n%@", divider, event, divider);
    }
}

- (void)setStartingPercentageComplete:(CGFloat)percentage forPropertyID:(NSString *)propertyID
{
    NSMutableDictionary *percentageDictionary = [[NSMutableDictionary alloc] init];
    [percentageDictionary setObject:[NSNumber numberWithFloat:percentage] forKey:@"startingPercent"];
    [self.propertyCompletionPercentageDictionary setObject:percentageDictionary forKey:propertyID];
}

- (void)updateCompletedPercentage:(CGFloat)percentage forPropertyID:(NSString *)propertyID
{
    if ([self.propertyCompletionPercentageDictionary objectForKey:propertyID]) {
        [[self.propertyCompletionPercentageDictionary objectForKey:propertyID] setObject:[NSNumber numberWithFloat:percentage] forKey:@"completedPercent"];
    } else
    {
//        [NSException raise:@"Tried to set property completion percentage with no starting percentage" format:@"No object for propertyID key %@", propertyID];
    }
}

- (void)logPercentageEvents
{
    NSLog(@"log percents");
    
    if (self.propertyCompletionPercentageDictionary.allKeys.count > 0) {
        BackgroundUploadIdentifier = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
            BackgroundUploadIdentifier = UIBackgroundTaskInvalid;
        }];
        
        for (NSString *propertyID in self.propertyCompletionPercentageDictionary.allKeys) {
            NSDictionary *percents = [NSDictionary dictionaryWithDictionary:[self.propertyCompletionPercentageDictionary objectForKey:propertyID]];
            CGFloat startingPercent = [[percents objectForKey:@"startingPercent"] floatValue];
            CGFloat completedPercent = [[percents objectForKey:@"completedPercent"] floatValue];
            if (!startingPercent == completedPercent) {
                [self logEvent:@"Property_Values_Completed" withParameters:@{
                                                                             @"PropertyID": propertyID,
                                                                             @"Values_Completed_By_User" : [NSString stringWithFormat:@"%.0f%%", completedPercent - startingPercent],
                                                                             @"Total_Values_Completed" : [NSString stringWithFormat:@"%.0f%%", completedPercent],
                                                                             @"User" : [NSString stringWithFormat:@"%@ %@", [SPGAuthenticationManager user].firstName, [SPGAuthenticationManager user].lastName]
                                                                             }];
            }
        }
        
        [[UIApplication sharedApplication] endBackgroundTask:BackgroundUploadIdentifier];
    }
}

@end
