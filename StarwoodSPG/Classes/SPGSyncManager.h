//
//  SPGSyncManager.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/25/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "JLTaskManager.h"

#warning Set sync interval to 1 min for testing
#define kSyncIntervalDefault 1.0 * 60.0//(24.0 * 60.0 * 60.0)

@class KSReachability;

@interface SPGSyncManager : JLTaskManager

+ (NSDate *)lastSyncDate;
+ (NSDate *)syncExpiryDate;
+ (BOOL)syncDurationExpired; // Meaning we must sync before the user can continue using the app.
+ (void)sync;
+ (void)syncRoomFeaturesForPropertyWithID:(NSString*)propertyId block:(JLTaskObserverBlock)block;
+ (BOOL)isSyncing;
+ (JLTaskObserver*)addGenericObserverForSyncRoomFeaturesWithBlock:(JLTaskObserverBlock)block;

@end
