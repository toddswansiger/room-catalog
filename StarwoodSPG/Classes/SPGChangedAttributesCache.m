//
//  SPGChangedAttributesCache.m
//  StarwoodSPG
//
//  Created by Nick Lee on 7/3/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGChangedAttributesCache.h"
#import "StandardPaths.h"

static NSString * const kCacheFileName = @"cached.spg";

@interface SPGChangedAttributesCache ()

@property (nonatomic, strong, readonly) NSMutableSet *set;
@property (nonatomic, strong, readonly) NSString *path;

- (void)persistSet;

@end

@implementation SPGChangedAttributesCache

@synthesize set = _set;
@dynamic path;

+ (instancetype)sharedCache
{
    static SPGChangedAttributesCache *cache;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cache = [[SPGChangedAttributesCache alloc] init];
    });
    return cache;
}

- (id)init
{
    if (self = [super init]) {
        
    }
    return self;
}

- (NSString *)path
{
    return [[[NSFileManager defaultManager] privateDataPath] stringByAppendingPathComponent:kCacheFileName];
}

- (NSMutableSet *)set
{
    if (!_set) {
        _set = [NSKeyedUnarchiver unarchiveObjectWithFile:[self path]];
    }
    
    if (!_set) {
        _set = [[NSMutableSet alloc] init];
    }
    
    return _set;
}

- (id<NSCopying>)keyForCategoryCode:(NSString *)categoryCode roomNumber:(NSString *)roomNumber buildingID:(NSString *)buildingID propertyID:(NSString *)propertyID floorNumber:(NSString *)floorNumber
{
    NSDictionary *key = @{
                          @"code":categoryCode ?: @"",
                          @"roomNumber" : roomNumber ?: @"",
                          @"buildingID" : buildingID ?: @"",
                          @"propertyID" : propertyID ?: @"",
                          @"floorNumber" : floorNumber ?: @""
                          };
    return key;
}

- (BOOL)isCategoryCodeDirty:(NSString *)categoryCode roomNumber:(NSString *)roomNumber buildingID:(NSString *)buildingID propertyID:(NSString *)propertyID floorNumber:(NSString *)floorNumber
{
    id<NSCopying> key = [self keyForCategoryCode:categoryCode roomNumber:roomNumber buildingID:buildingID propertyID:propertyID floorNumber:floorNumber];
    return [[self set] containsObject:key];
}

- (void)markCategoryCodeDirty:(NSString *)categoryCode roomNumber:(NSString *)roomNumber buildingID:(NSString *)buildingID propertyID:(NSString *)propertyID floorNumber:(NSString *)floorNumber
{
    id<NSCopying> key = [self keyForCategoryCode:categoryCode roomNumber:roomNumber buildingID:buildingID propertyID:propertyID floorNumber:floorNumber];
    [[self set] addObject:key];
}

- (void)persistSet
{
    [NSKeyedArchiver archiveRootObject:[self set] toFile:[self path]];
}

- (void)clearEverything
{
    [[NSFileManager defaultManager] removeItemAtPath:[self path] error:NULL];
    _set = nil;
}

@end
