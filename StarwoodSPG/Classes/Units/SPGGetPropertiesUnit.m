//
//  SPGGetPropertiesUnit.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGGetPropertiesUnit.h"
#import "NSMutableURLRequest+RequestSignature.h"

@implementation SPGGetPropertiesUnit

- (void)doWorkWithError:(NSError *__autoreleasing *)error
{
    NSDictionary* json = nil;
    NSDictionary* serviceInfo = self.task.taskInfo;
    
    NSString* userToken = serviceInfo[@"userToken"];
    NSString* userID = serviceInfo[@"user"];

    NSArray* testArray = [self entitiesForName:[[SPGBuilding class] description] withPredicate:nil];
    
    [self deleteAllObjectsForEntity:[[SPGBuilding class] description] withPredicate:[NSPredicate predicateWithFormat:@"ANY property.users.userID == %@", userID]];

    testArray = [self entitiesForName:[[SPGProperty class] description] withPredicate:nil];
    [self deleteAllObjectsForEntity:[[SPGProperty class] description] withPredicate:[NSPredicate predicateWithFormat:@"ANY users.userID == %@", userID]];

    testArray = [self entitiesForName:[[SPGUser class] description] withPredicate:nil];
    [self deleteAllObjectsForEntity:[[SPGUser class] description] withPredicate:[NSPredicate predicateWithFormat:@"userID == %@", userID]];
    
#if _USE_FILE_FOR_DATA_FETCHING_
    NSString* fileName = @"portalProfile response.json";
    NSString* path = [[NSBundle mainBundle] pathForResource:[fileName stringByDeletingPathExtension] ofType:[fileName pathExtension]];
    
    if(path)
    {
        NSData* data = [NSData dataWithContentsOfFile:path];
        json = data.jsonDictionaryValue;
    }
#else
    
    NSString* hostURL = serviceInfo[@"envUrl"];
    NSString* apiKey = serviceInfo[@"apiKey"];
    NSString* secretKey = serviceInfo[@"secretKey"];
    
    NSString* urlStr = [NSString stringWithFormat:@"https://%@/associate/portalProfile?v=2&apiKey=%@&", hostURL, apiKey];
 
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [request setValue:userToken forHTTPHeaderField:@"userToken"];
    [request signRequestUsingAccessKey:apiKey secretKey:secretKey];

    JLURLConnection* connection = [JLURLConnection startSynchronousConnectionWithRequest:request progressBlock:nil];
    
    if(connection.data.length)
    {
        json = connection.data.jsonDictionaryValue;
    }
#endif
    
#ifndef _USE_FILE_FOR_DATA_FETCHING_
    SPGError* spgError = [self processErrorForConnection:connection];
    if(!spgError)
    {
#endif
        NSDictionary* profileDict = json;
        
        if(profileDict.count)
        {
            NSDictionary *errorDict = [profileDict objectForKey:@"response"];
            NSDictionary *errorStatusDict = [errorDict objectForKey:@"status"];
            NSString *statusMessage = [errorStatusDict objectForKey:@"message"];
            NSNumber *statusCode = [errorStatusDict objectForKey:@"statusCode"];
            NSLog(@"statusMessage = %@", statusMessage);
            NSLog(@"statusCode = %@", statusCode);

            if([statusMessage isEqualToString:@"Unauthorized"] || [statusCode integerValue] == 401)
            {
                dispatch_async(dispatch_get_main_queue(), ^(){
                    [SPGAlertView showAlertWithTitle:@"Unauthorized" message:@"At this point, you should be redirected to the appropriate download location for the new version." completionBlock:^(NSInteger buttonIndex, JLAlertView *alertView) {
                        
                    } cancelButtonTitle:@"Ok" otherButtonTitles:nil];

                });
            }
            else
            {
                SPGUser* user = nil;
                
                if(userToken.length)
                {
                    user = (SPGUser*)[self firstEntityForName:[[SPGUser class] description] withPredicate:[NSPredicate predicateWithFormat:@"token == %@", userToken]];
                }
                
                if(!user)
                {
                    user = [self newEntityWithName:[[SPGUser class] description]];
                }
                
                // this will create new properties for this user if
                // properties with ID have not already been created,
                // else it will update the properties info
                [user mapModelWithData:profileDict];
                user.userID = userID.copy;
            }
        }
    
#ifndef _USE_FILE_FOR_DATA_FETCHING_
    }
    else
    {
        *error = spgError;
    }
#endif
    
    [self save];
}

@end
