//
//  SPGUpdateRoomFeaturesUnit.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/19/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGUpdateRoomFeaturesUnit.h"

@implementation SPGUpdateRoomFeaturesUnit
- (SPGFeatureCode*)existingCodeWithCategory:(NSString*)category room:(SPGRoomSummary*)room
{
    SPGFeatureCode* code = nil;
    if(category.length)
    {
        code = (SPGFeatureCode*)[self firstEntityForName:[SPGFeatureCode class] withPredicate:[NSPredicate predicateWithFormat:@"categoryCode == %@", category]];
    }
    
    return code;
}

- (void)doWorkWithError:(NSError *__autoreleasing *)error
{
    NSDictionary* taskInfo = self.task.taskInfo;
    SPGRoomUpdateType updateType = [taskInfo[@"updateType"] intValue];
    NSDictionary* jsonAttribute = taskInfo[@"attribute"];
    NSDictionary* jsonMapping = taskInfo[@"mapping"];
    NSString* mappingCode = [jsonMapping valueForKey:@"code"];
    NSString* categoryCode = [jsonAttribute valueForKey:@"code"];
    NSDictionary* jsonRoom = taskInfo[@"room"];
    NSString* roomNumKey = @"roomNum";
    NSString* buildingNameKey = @"buildingName";
    NSString* floorNumKey = @"floorNum";
    NSString* roomNum = jsonRoom[roomNumKey];
    NSString* buildingName = jsonRoom[buildingNameKey];
    buildingName = buildingName.length ? buildingName : kBuildingNameUnknown;
    NSString* floorNum = jsonRoom[floorNumKey];
    NSString* propertyId = taskInfo[@"propertyId"];
    
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"roomFeature.roomFeatureSummary.propertyId == %@ && %K == %@ && %K == %@ && %K == %@",propertyId, roomNumKey, roomNum, buildingNameKey, buildingName, floorNumKey, floorNum];
    NSArray* rooms = [self entitiesForName:[[SPGRoomSummary class] description] withPredicate:predicate];
    
    if(rooms.count)
    {
        SPGRoomSummary* room = rooms[0];
        if(updateType == SPGRoomUpdateType_Add)
        {
            NSArray* attributeMappings = [self entitiesForName:[[SPGAttributeMapping class] description] withPredicate:[NSPredicate predicateWithFormat:@"attributeCategory.property.propertyId == %@ && mappingCode == %@ && attributeCategory.categoryCode == %@", propertyId, mappingCode, categoryCode]];
            
            NSMutableArray* newCodes = [NSMutableArray arrayWithCapacity:attributeMappings.count];
            NSArray* featureCodes = room.featureCodes.allObjects;
            for(SPGAttributeMapping* mapping in attributeMappings)
            {
                SPGAttributeCategory* attribute = mapping.attributeCategory;
                
                if(!attribute)
                    continue;
                // create a new feature code for each attribute and add it to the room
                SPGFeatureCode* code = nil;
                
                NSArray* existingCodes = [featureCodes filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"categoryCode == %@", attribute.categoryCode]];
                
                code = existingCodes.count ? existingCodes[0] : nil;
                
                if(!code)
                {
                    code = [self newEntityWithName:[[SPGFeatureCode class] description]];
                }
                
                code.categoryCode = attribute.categoryCode.copy;
                code.codeName = mapping.mappingCode.copy;
                
                [newCodes addObject:code];
            }
            room.featureCodes = [room.featureCodes setByAddingObjectsFromArray:newCodes];
        }
        else if(updateType == SPGRoomUpdateType_Delete)
        {
            NSArray* attributeMappings = [self entitiesForName:[[SPGAttributeCategory class] description] withPredicate:[NSPredicate predicateWithFormat:@"property.propertyId == %@ && categoryCode == %@", propertyId, categoryCode]];
            
            NSArray* mappingCodes = [attributeMappings valueForKey:@"categoryCode"];
            NSArray* featurCodes = room.featureCodes.allObjects;
            NSArray* codesToDelete = [featurCodes filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(categoryCode IN %@)", mappingCodes]];
            NSMutableArray* updatedFeatureCodes = featurCodes.mutableCopy;
            [updatedFeatureCodes removeObjectsInArray:codesToDelete];
            
            room.featureCodes = [NSSet setWithArray:updatedFeatureCodes];
        }
        else if(updateType == SPGRoomUpdateType_Flag)
        {
            room.isFlagged = @(!room.isFlagged.boolValue);
        }
        
        NSString* transactionId = room.transactionId;
        NSArray* existingTransactions = [self entitiesForName:[[SPGSyncTransaction class] description] withPredicate:[NSPredicate predicateWithFormat:@"transactionId == %@", transactionId]];

        // need to add this room to a sync transaction table
        // ignore isCompleted flag
        if(updateType != SPGRoomUpdateType_Flag /*&& room.isCompleted*/)
        {
            if(!existingTransactions.count)
            {
                SPGSyncTransaction* syncTransaction = [self newEntityWithName:[[SPGSyncTransaction class] description]];
                syncTransaction.transactionId = room.transactionId;
            }
        }
    }
    
    [self save];
}
@end
