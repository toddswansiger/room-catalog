//
//  SPGUserLogoutUnit.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/11/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGUserLogoutUnit.h"
#import "CoreData+MagicalRecord.h"
#import "SPGChangedAttributesCache.h"

@implementation SPGUserLogoutUnit

- (void)doWorkWithError:(NSError *__autoreleasing *)error
{
    // delete all content for user
    NSDictionary* serviceInfo = self.task.taskInfo;
    NSString* userID = serviceInfo[@"user"];
    
    // delete building objects created for user
    [self deleteAllObjectsForEntity:[[SPGBuilding class] description] withPredicate:[NSPredicate predicateWithFormat:@"ANY property.users.userID == %@", userID]];

    NSArray* propertiesArray = [self entitiesForName:[[SPGProperty class] description] withPredicate:[NSPredicate predicateWithFormat:@"ANY users.userID == %@", userID]];

    //delete all relationships for each property for the user
    for(SPGProperty* property in propertiesArray)
    {
        NSString* propertyID = property.propertyId;
        
        // delete all content for property
        [self deleteAllObjectsForEntity:[[SPGFeatureCode class] description] withPredicate:[NSPredicate predicateWithFormat:@"ANY roomFeatures.roomFeatureSummary.propertyId == %@", propertyID]];
        [self deleteAllObjectsForEntity:[[SPGRoomSummary class] description] withPredicate:[NSPredicate predicateWithFormat:@"roomFeature.roomFeatureSummary.propertyId == %@", propertyID]];
        [self deleteAllObjectsForEntity:[[SPGRoomFeature class] description] withPredicate:[NSPredicate predicateWithFormat:@"roomFeatureSummary.propertyId == %@", propertyID]];
        [self deleteAllObjectsForEntity:[[SPGRoomFeatureSummary class] description] withPredicate:[NSPredicate predicateWithFormat:@"propertyId == %@", propertyID]];
        
        [self deleteAllObjectsForEntity:[[SPGAttributeMapping class] description] withPredicate:[NSPredicate predicateWithFormat:@"attributeCategory.property.propertyId == %@", propertyID]];
        
        [self deleteAllObjectsForEntity:[[SPGAttributeCategory class] description] withPredicate:[NSPredicate predicateWithFormat:@"property.propertyId == %@", propertyID]];
    
    }
    
    //now delete all of the properties
    [self deleteAllObjectsForEntity:[[SPGProperty class] description] withPredicate:[NSPredicate predicateWithFormat:@"ANY users.userID == %@", userID]];
    
    // delete the user
    [self deleteAllObjectsForEntity:[[SPGUser class] description] withPredicate:[NSPredicate predicateWithFormat:@"userID == %@", userID]];
    
    [[SPGChangedAttributesCache sharedCache] clearEverything];
    
    [self save];
}
@end
