//
//  SPGGetBuildingsUnit.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/23/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGGetBuildingsUnit.h"

@implementation SPGGetBuildingsUnit

- (void)doWorkWithError:(NSError *__autoreleasing *)error
{
    NSDictionary* serviceInfo = self.task.taskInfo;
    NSString* propertyID = serviceInfo[@"propertyId"];

    SPGProperty* property = (SPGProperty*)[self firstEntityForName:[[SPGProperty class]description] withPredicate:[NSPredicate predicateWithFormat:@"propertyId == %@", propertyID]];
    
    if(property)
    {
        // just extract building names from room summary objects
        NSArray* rooms = [self entitiesForName:[[SPGRoomSummary class] description] withPredicate:nil];
        
        // setting a default building name for all rooms, prevents a room from being ignored when filtering
        [self setDefaultBuildingNameForRooms:rooms];
        
        NSArray* filteredRooms = [rooms filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"propertyId == %@", property.propertyId]];
        NSArray* buildingNames = [filteredRooms valueForKeyPath:@"@distinctUnionOfObjects.buildingName"];
        
        if(buildingNames.count)
        {
            //let's clear all old data
            [self deleteAllObjectsForEntity:[[SPGBuilding class] description] withPredicate:[NSPredicate predicateWithFormat:@"property.propertyId == %@", propertyID]];

            for(NSString* buildingName in buildingNames)
            {
                SPGBuilding* building = [self newEntityWithName:[[SPGBuilding class] description]];
                building.property = property;
                building.buildingName = buildingName.copy;
                
                //get all rooms with this building name
                NSArray* roomsForBuilding = [rooms filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"buildingName == %@ && roomFeature.roomFeatureSummary.propertyId == %@", building.buildingName, property.propertyId]];
                
                building.rooms = [NSSet setWithArray:roomsForBuilding];
            }
        }

    }
    [self save];
}


- (void)setDefaultBuildingNameForRooms:(NSArray*)rooms
{
    NSString* defaultName = kBuildingNameUnknown;
    
    for(SPGRoomSummary* room in rooms)
    {
        if(!room.buildingName.length)
        {
            room.buildingName = defaultName;
        }
    }
}
@end
