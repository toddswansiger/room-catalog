//
//  SPGTaskUnit.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/24/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "JLTaskUnit.h"

@interface SPGTaskUnit : JLTaskUnit

- (SPGError*)processErrorForConnection:(JLURLConnection*)connection;
@end
