//
//  SPGGetPropertiesUnit.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//
#import "SPGTaskUnit.h"
#import "SPGUnits.h"

@interface SPGGetPropertiesUnit : SPGTaskUnit

@end
