//
//  SPGAuthenticationUnit.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/4/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGAuthenticationUnit.h"
#import "NSMutableURLRequest+RequestSignature.h"

@implementation SPGAuthenticationUnit

- (void)doWorkWithError:(NSError *__autoreleasing *)error
{
    NSDictionary* serviceInfo = self.task.taskInfo;
    NSString* username = serviceInfo[@"user"];
    NSString* password = serviceInfo[@"password"];
    NSString* hostURL = serviceInfo[@"envUrl"];
    NSString* apiKey = serviceInfo[@"apiKey"];
    NSString* endpoint = nil;
    NSDictionary* json = nil;
    
    if(username.length && password.length)
    {
        //api3.qa.starwoodhotels.com/token/associate
        NSString* urlStr = [NSString stringWithFormat:@"https://%@/token/associate", hostURL];
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        endpoint = urlStr;
        NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];

        [request setHTTPBody:[NSString stringWithFormat:@"username=%@&password=%@&apiKey=%@", username, password, apiKey].dataValue];
        request.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;

        JLURLConnection* connection = [JLURLConnection startSynchronousConnectionWithRequest:request progressBlock:nil];
        if(connection.data.length)
        {
            json = connection.data.jsonDictionaryValue;
        }
        
        //ttestus1/proptestusr597
        SPGError* spgError = [self processErrorForConnection:connection];
        if(!spgError)
        {
            NSString* token = [json valueForKeyPath:@"authorizationResponse.token"];
            NSString* tokenStatus = [json valueForKeyPath:@"authorizationResponse.tokenStatus"];
            NSString* tokenExpirationDateTime = [json valueForKeyPath:@"authorizationResponse.tokenExpirationDateTime"];
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
            NSDate* expireDate = [dateFormatter dateFromString:tokenExpirationDateTime];
            NSString* userID = [json valueForKeyPath:@"authorizationResponse.username"];
            if([tokenStatus isEqualToString:@"VALID"])
            {
                [[SPGAuthenticationManager sharedInstance] setToken:token tokenStatus:tokenStatus tokenExpireDate:expireDate forUser:username];
                
                // need to create a user
                SPGUser* user = nil;
                
                if(userID.length)
                {
                    user = (SPGUser*)[self firstEntityForName:[[SPGUser class] description] withPredicate:[NSPredicate predicateWithFormat:@"userID == %@", userID]];
                }
                
                if(!user)
                {
                    user = [self newEntityWithName:[[SPGUser class] description]];
                }
                
                user.userID = userID.copy;
                user.token = token.copy;
            }
            else
            {
                *error = [SPGError errorWithCode:@"" message:@"Invalid Token"];
            }
        }
        else
        {
            *error = spgError;
        }
        
        [self save];
    }
}

@end
