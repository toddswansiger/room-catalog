//
//  SPGGetRoomFeaturesUnit.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGGetRoomFeaturesUnit.h"
#import "SPGRoomFeature+Helper.h"
#import "NSMutableURLRequest+RequestSignature.h"

@implementation SPGGetRoomFeaturesUnit

- (void)clearOldRoomDataForPropertyWithID:(NSString*)propertyID
{
    //let's clear all old data
    [self deleteAllObjectsForEntity:[[SPGFeatureCode class] description] withPredicate:[NSPredicate predicateWithFormat:@"ANY roomFeatures.roomFeatureSummary.propertyId == %@", propertyID]];
    [self deleteAllObjectsForEntity:[[SPGRoomSummary class] description] withPredicate:[NSPredicate predicateWithFormat:@"roomFeature.roomFeatureSummary.propertyId == %@", propertyID]];
    [self deleteAllObjectsForEntity:[[SPGRoomFeature class] description] withPredicate:[NSPredicate predicateWithFormat:@"roomFeatureSummary.propertyId == %@", propertyID]];
    [self deleteAllObjectsForEntity:[[SPGRoomFeatureSummary class] description] withPredicate:[NSPredicate predicateWithFormat:@"propertyId == %@", propertyID]];
}

- (void)setDefaultBuildingNameForRooms:(NSArray*)rooms
{
    NSString* defaultName = kBuildingNameUnknown;
    
    for(SPGRoomSummary* room in rooms)
    {
        if(!room.buildingName.length)
        {
            room.buildingName = defaultName;
        }
    }
}

- (void)doWorkWithError:(NSError *__autoreleasing *)error
{
    NSDictionary* json = nil;
    NSDictionary* serviceInfo = self.task.taskInfo;
    NSString* hostURL = serviceInfo[@"envUrl"];
    NSString* propertyID = serviceInfo[@"propertyId"];
    NSString* apiKey = serviceInfo[@"apiKey"];
    NSString* secretKey = serviceInfo[@"secretKey"];
    NSString* userToken = serviceInfo[@"userToken"];
    NSArray* propertiesArray = self.state[@"syncPropIDs"] ?: @[];
    
    //get a sync flag
    BOOL inSync = [self.state[@"inSync"] boolValue];

//    [self clearOldRoomDataForPropertyWithID:propertyID];
    NSString* endpoint = nil;

    if(propertyID.length && ![propertiesArray containsObject:propertyID])
    {
        propertiesArray = [propertiesArray arrayByAddingObject:propertyID];
    }
    
    for(NSString* propertyID in propertiesArray)
    {
    
#if _USE_FILE_FOR_DATA_FETCHING_
        NSString* fileName = @"getRoomFeaturesHierarchy response.json";
        NSString* path = [[NSBundle mainBundle] pathForResource:[fileName stringByDeletingPathExtension] ofType:[fileName pathExtension]];
        
        if(path)
        {
            endpoint = path;
            NSData* data = [NSData dataWithContentsOfFile:path];
            json = data.jsonDictionaryValue;
        }
#else
        
        NSString* urlStr = [NSString stringWithFormat:@"https://%@/associate/property/roomFeaturesHierarchy/%@?v=2&apiKey=%@", hostURL, propertyID, apiKey];

        endpoint = urlStr;
        NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
        [request setValue:userToken forHTTPHeaderField:@"userToken"];
        [request signRequestUsingAccessKey:apiKey secretKey:secretKey];
        
        [request setHTTPMethod:@"GET"];
        request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
        [request setValue:[NSString stringWithFormat:@"gzip;q=0"] forHTTPHeaderField:@"Accept-Encoding"];
        __weak SPGGetRoomFeaturesUnit* weakSelf = self;
        JLURLConnection* connection = [JLURLConnection startSynchronousConnectionWithRequest:request progressBlock:^(JLURLConnection *urlConnection, JLURLConnectionState state, JLError *errorStatus)
                                       {
                                           if(state == JLURLConnectionState_Running || state == JLURLConnectionState_Finished)
                                           {
                                               double progress = urlConnection.progress;
                                               [weakSelf.task setProgress:progress forUnit:weakSelf];
                                           }
                                       }];
        
        if(connection.data.length)
        {
            json = connection.data.jsonDictionaryValue;
        }
#endif
        
#ifndef _USE_FILE_FOR_DATA_FETCHING_
        SPGError* spgError = [self processErrorForConnection:connection];
        if(!spgError)
        {
#endif
            NSDictionary* featureSummaryDict = [json valueForKeyPath:@"roomFeaturesHierarchyResponse.featureSummary"];
            
            if(featureSummaryDict.count)
            {
                SPGRoomFeatureSummary* featureSummary = nil;
                if(!inSync)
                {
                    // clear data only if we know new data is ready
                    [self clearOldRoomDataForPropertyWithID:propertyID];
                    featureSummary = [self newEntityWithName:[[SPGRoomFeatureSummary class] description]];
                }
                else
                {
                    NSString* entityName = [[SPGRoomFeatureSummary class] description];
                    Class aClass = NSClassFromString(entityName);
                    if([aClass respondsToSelector:@selector(existingObjectForData:context:)])
                    {
                        featureSummary = [aClass existingObjectForData:featureSummaryDict context:self.managedObjectContext];
                    }
                }
                
                if(featureSummary)
                {
                    [featureSummary mapModelWithData:featureSummaryDict];
                    // need to remap buidling names for updated rooms
                    
                    for(SPGRoomFeature* feature in featureSummary.roomFeatures)
                    {
                        NSArray* rooms = feature.roomSummaries.allObjects;
                        [self setDefaultBuildingNameForRooms:rooms];
                    }
                }
            }
            
            [self save];
        
#ifndef _USE_FILE_FOR_DATA_FETCHING_
        }
        else
        {
            *error = spgError;
        }
#endif
    }
}
@end
