//
//  SPGGetAttributeCategoriesUnit.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/23/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGGetAttributeCategoriesUnit.h"
#import "NSMutableURLRequest+RequestSignature.h"

@implementation SPGGetAttributeCategoriesUnit
- (void)doWorkWithError:(NSError *__autoreleasing *)error
{
    NSDictionary* json = nil;
    
    NSDictionary* serviceInfo = self.task.taskInfo;
    NSString* propertyID = serviceInfo[@"propertyId"];
    NSString* endpoint = nil;
    
#if _USE_FILE_FOR_DATA_FETCHING_
    NSString* fileName = @"getAttributeCategories response.json";
    NSString* path = [[NSBundle mainBundle] pathForResource:[fileName stringByDeletingPathExtension] ofType:[fileName pathExtension]];
    
    if(path)
    {
        endpoint = path;
        NSData* data = [NSData dataWithContentsOfFile:path];
        json = data.jsonDictionaryValue;
    }
#else

    NSString* hostURL = serviceInfo[@"envUrl"];
    NSString* apiKey = serviceInfo[@"apiKey"];
    NSString* secretKey = serviceInfo[@"secretKey"];
    NSString* userToken = serviceInfo[@"userToken"];
    
    NSString* urlStr = [NSString stringWithFormat:@"https://%@/associate/property/attributeCategories/%@?v=2&apiKey=%@&includeMetaData=1", hostURL, propertyID, apiKey];
    endpoint = urlStr;

    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [request setValue:userToken forHTTPHeaderField:@"userToken"];
    [request signRequestUsingAccessKey:apiKey secretKey:secretKey];
    
    JLURLConnection* connection = [JLURLConnection startSynchronousConnectionWithRequest:request progressBlock:nil];
    
    if(connection.data.length)
    {
        json = connection.data.jsonDictionaryValue;
    }
#endif
    
#ifndef _USE_FILE_FOR_DATA_FETCHING_
    SPGError* spgError = [self processErrorForConnection:connection];
    if(!spgError)
    {
#endif
        NSArray* attrCategoryResults = [json valueForKeyPath:@"attributeCategoriesResponse.attributeCategoryResults.attributeCategoryResult"];
        NSDictionary* attrCategoryDict = attrCategoryResults.count ? attrCategoryResults[0] : nil;

        if(attrCategoryDict.count)
        {
            //let's clear all old data
            [self deleteAllObjectsForEntity:[[SPGAttributeMapping class] description] withPredicate:[NSPredicate predicateWithFormat:@"attributeCategory.property.propertyId == %@", propertyID]];
            
            [self deleteAllObjectsForEntity:[[SPGAttributeCategory class] description] withPredicate:[NSPredicate predicateWithFormat:@"property.propertyId == %@", propertyID]];
            
            SPGProperty* property = (SPGProperty*)[self firstEntityForName:[[SPGProperty class] description] withPredicate:[NSPredicate predicateWithFormat:@"propertyId == %@", propertyID]];
            
            if(property)
            {
                [property mapModelWithData:attrCategoryDict];
            }
            else
            {
                NSLog(@"ERROR in SPGGetAttributeCategoriesUnit - No Property");
            }
        }
        
        [self save];
        
#ifndef _USE_FILE_FOR_DATA_FETCHING_
    }
    else
    {
        *error = spgError;
    }
#endif
}

@end
