//
//  SPGSyncRoomFeaturesUnit.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/26/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGSyncRoomFeaturesUnit.h"
#import "SPGChangedAttributesCache.h"
#import "NSMutableURLRequest+RequestSignature.h"

@implementation SPGSyncRoomFeaturesUnit

- (void)doWorkWithError:(NSError *__autoreleasing *)error
{
    // let's just push immediately
    NSDictionary* serviceInfo = self.task.taskInfo;
    
    //set a sync flag
    self.state[@"inSync"] = @(YES);
    
    NSString* propertyId = serviceInfo[@"propertyId"];
    
    // NOTE: a nil propertyId can be passed by the automatic sync call.  default behavior is to sync all
    // properties who have pending transactions.
    NSArray* filteredProperties = [self entitiesForName:[[SPGProperty class] description] withPredicate:(propertyId.length ? [NSPredicate predicateWithFormat:@"propertyId == %@", propertyId] : nil)];
    NSArray* pendingTransactions = [self entitiesForName:[[SPGSyncTransaction class] description] withPredicate:nil];
    
    NSMutableArray* modifiedProperties = [NSMutableArray array];
    
    for(SPGProperty* property in filteredProperties)
    {
        NSArray* pendingTransactionIDs = [pendingTransactions valueForKeyPath:@"transactionId"];
        NSArray* completedRooms = [self entitiesForName:[[SPGRoomSummary class] description] withPredicate:nil];
        
        // only select rooms in this property
        completedRooms = [completedRooms filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"propertyId == %@ && (transactionId IN %@)", property.propertyId, pendingTransactionIDs]];
        
        if(!completedRooms.count)
            continue;
        
        [modifiedProperties addObject:property.propertyId.copy];
        
        // convert rooms to JSON
        NSMutableArray* roomSummaryArray = [NSMutableArray array];
        int roomCount = 0;
        for(SPGRoomSummary* room in completedRooms)
        {
            NSMutableDictionary* roomJSON = room.jsonValue.mutableCopy;
            
            NSArray *featureCodes = [roomJSON valueForKeyPath:@"featureCodesArray.featureCode"];
            
            NSArray *filteredFeatureCodes = [featureCodes filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSDictionary *evaluatedObject, NSDictionary *bindings) {
               
                NSString *categoryCode = [evaluatedObject objectForKey:@"categoryCode"];
                
                BOOL dirty = [[SPGChangedAttributesCache sharedCache] isCategoryCodeDirty:categoryCode
                                                                               roomNumber:room.roomNum
                                                                               buildingID:room.building.buildingName
                                                                               propertyID:room.building.property.propertyId
                                                                              floorNumber:room.floorNum];
                
                return dirty;
                
            }]];
            
            if(filteredFeatureCodes.count)
            {
                [roomJSON setObject:@{@"featureCode": filteredFeatureCodes} forKey:@"featureCodesArray"];
            }
            else
            {
                [roomJSON removeObjectForKey:@"featureCodesArray"];
            }
            
            [roomSummaryArray addObject:roomJSON];
            roomCount++;
        }
        
        if(roomSummaryArray.count)
        {
            NSMutableDictionary* roomJSONDict = @{
                                                  @"saveRoomFeaturesRequest" :
                                                      @{
                                                          @"propertyId" : property.propertyId,
                                                          @"roomSummaryArray" :
                                                              @{
                                                                  @"roomSummary" : roomSummaryArray
                                                                  }
                                                          }
                                                  }.mutableCopy;
            
            NSDictionary* serviceInfo = self.task.taskInfo;
            NSString* hostURL = serviceInfo[@"envUrl"];
            NSString* apiKey = serviceInfo[@"apiKey"];
            NSString* secretKey = serviceInfo[@"secretKey"];
            NSString* userToken = serviceInfo[@"userToken"];
            NSString* endpoint = nil;
            NSDictionary* json = nil;

            NSString* urlStr = [NSString stringWithFormat:@"https://%@/associate/property/roomFeatures?v=2&apiKey=%@", hostURL, apiKey];
            urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            endpoint = urlStr;
            NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            NSString* roomJSONValue = roomJSONDict.jsonStringValue;
            NSLog(@"Property Room Feature JSON = %@", roomJSONValue);
            NSData* bodyData = [NSString stringWithFormat:@"%@", roomJSONValue].dataValue;
            [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[bodyData length]] forHTTPHeaderField:@"Content-Length"];
            
            [request setHTTPBody:bodyData];
            request.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
            
            [request setValue:userToken forHTTPHeaderField:@"userToken"];
            [request signRequestUsingAccessKey:apiKey secretKey:secretKey];

            // TODO: remove timeout interval
            request.timeoutInterval = 10.0;
            JLURLConnection* connection = [JLURLConnection startSynchronousConnectionWithRequest:request progressBlock:nil];
            
            if(connection.data.length)
            {
                json = connection.data.jsonDictionaryValue;
            }
            
            SPGError* spgError = [self processErrorForConnection:connection];
            NSLog(@"spgError = %@", spgError);
            BOOL success = NO;
            if(!spgError)
            {
                if([json valueForKeyPath:@"roomFeaturesHierarchyResponse.status"])
                {
                    id obj = [json valueForKeyPath:@"roomFeaturesHierarchyResponse.status"];
                    if( [obj isKindOfClass:[NSNumber class]] || [obj isKindOfClass:[NSString class]])
                    {
                        NSInteger status = [obj integerValue];
                        if(status)
                        {
                            //Success!
                            success = YES;
                            for(SPGSyncTransaction* syncTransaction in pendingTransactions)
                            {
                                [self.managedObjectContext deleteObject:syncTransaction];
                            }
                        }
                    }
                }
            }
            else
            {
                *error = spgError;
            }
            
            [self save];
        }
    }
    
    if(modifiedProperties.count)
    {
        //add to list for get room features API
        self.state[@"syncPropIDs"] = modifiedProperties;
    }
}
@end
