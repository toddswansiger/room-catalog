//
//  SPGTaskUnit.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/24/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGTaskUnit.h"
#import "KSReachability.h"

@implementation SPGTaskUnit

- (void)processVersionInfoWithError:(SPGError*)error
{
    // this will get reset in the unit
    if([error.spgErrorCode isEqualToString:@"OTA192"])
    {
        if(![SPGAuthenticationManager isNewVersionAvailable])
        {
            [SPGAuthenticationManager setNewVersionAvailable:YES];
        }
    }
    // commented out code for resetting flag if error is not OTA192
//    else if(error && error.spgErrorCode.intValue != -1 && [SPGAuthenticationManager isNewVersionAvailable])
//    {
//        [SPGAuthenticationManager setNewVersionAvailable:NO];
//    }
}

- (SPGError*)processErrorForConnection:(JLURLConnection*)connection
{
    SPGError* error = nil;
    
    NSDictionary* json = connection.data.jsonDictionaryValue;
    
    if(json.count)
    {
        NSDictionary* errorDict = [json valueForKeyPath:@"profileResponse.errors"] ?: [json valueForKeyPath:@"response.errors"];
        NSString* errorCode = nil;
        NSString* errorMsg = nil;
        NSDictionary* authErrorDict = [json valueForKeyPath:@"authorizationResponse.errors"];

        if(authErrorDict.count)
        {
            errorCode = authErrorDict[@"code"];
            errorMsg = authErrorDict[@"message"];
            error = [SPGError errorWithCode:errorCode message:errorMsg];
        }
        else if(errorDict.count)
        {
            errorCode = errorDict[@"code"];
            errorMsg = errorDict[@"message"];
            error = [SPGError errorWithCode:errorCode message:errorMsg];
        }
    }
    else if(connection.errorStatus)
    {
        if(connection.errorStatus.code == -1009 || !self.spgAppDelegate.reachability.reachable)
        {
            NSString* errorCode = @"-1009";
            NSString* errorMsg = @"You are offline";
            error = [SPGError errorWithCode:errorCode message:errorMsg];
        }
        else
        {
            NSString* errorCode = @"-1";
            NSString* errorMsg = @"Unknown Connection Erorr";
            error = [SPGError errorWithCode:errorCode message:errorMsg];
        }
    }
    
    // use error to determine version info status
    // e.g. deprecated apiKeys means new version of app is available
    [self processVersionInfoWithError:error];
    
    return error;
}

@end
