//
//  SPGSyncManager.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/25/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGSyncManager.h"
#import "SPGAppDelegate.h"

#define kSPGSyncManagerSyncDateKey @"SPGSyncManagerLastSyncDate"

#define _USE_SYNC_TIMER_ 0

@interface SPGSyncManager ()

@property (nonatomic, readwrite) NSTimer *timer;
@property (nonatomic, strong) NSDate* lastSyncDate;
@property (nonatomic) BOOL isReachable;
@property (nonatomic, strong) NSString* offlineErrorTitle;
@property (nonatomic, strong) NSString* offlineErrorMsg;
@property (nonatomic) BOOL alertShown;
+ (void)sync;

@end

@implementation SPGSyncManager

static BOOL hasSetupObservers = NO;

+ (NSString *)environmentURL
{
    SPGAppDelegate *delegate = (SPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *environmentURL = [delegate environmentURL];
    return environmentURL;
}

+ (NSString *)environmentAPIKey
{
    SPGAppDelegate *delegate = (SPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *environmentKey = [delegate environmentAPIKey];
    return environmentKey;
}

+ (NSString *)environmentSecretKey
{
    SPGAppDelegate *delegate = (SPGAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *environmentKey = [delegate environmentSecretKey];
    return environmentKey;
}

+ (instancetype)sharedInstance
{
    SPGSyncManager* instance = [super sharedInstance];

    @synchronized(self)
    {
        if(instance && !hasSetupObservers)
        {
            hasSetupObservers = YES;
            [self setupObservers:instance];
        }
    }
    return instance;
}

- (NSTimer*)timer
{
    if(_timer == nil) {
        _timer = [[NSTimer alloc] initWithFireDate:[NSDate dateWithTimeIntervalSinceNow:60]
                                          interval:300
                                            target:self
                                          selector:@selector(sync)
                                          userInfo:nil
                                           repeats:YES];
        
        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
    }
    return _timer;
}

- (id)init
{
    self = [super init];
    if(self)
    {
        self.lastSyncDate = (NSDate*)[[NSUserDefaults standardUserDefaults] objectForKey:kSPGSyncManagerSyncDateKey];
        
        self.offlineErrorTitle = @"You are offline";
        self.offlineErrorMsg = @"Your changes will not sync with Valhalla Portal until you reconnect to wifi or data service. To ensure work is not lost, we recommend you do this at the end of your shift.";
    }
    return self;
}


#pragma mark -

+ (void)setupObservers:(SPGSyncManager*)syncManager
{
    __weak SPGSyncManager* weakSelf = syncManager;
    
    [self addGenericObserverForSyncRoomFeaturesWithBlock:^(JLTaskStatus taskStatus, NSDictionary *info, NSError *error) {
        switch (taskStatus) {
            case JLTaskStatus_Success:
            {
                NSDate* syncDate = [NSDate date];
                weakSelf.lastSyncDate = syncDate;
                [[NSUserDefaults standardUserDefaults] setObject:syncDate forKey:kSPGSyncManagerSyncDateKey];
                break;
            }
            case JLTaskStatus_Finished:
            {
                break;
            }
            default:
                break;
        }
    }];
    
    // add observer for get room features, for setting the initial sync date
    [self addGenericObserverForGetRoomFeaturesWithBlock:^(JLTaskStatus taskStatus, NSDictionary *info, NSError *error) {
        switch (taskStatus) {
            case JLTaskStatus_Success:
            {
                if(!weakSelf.lastSyncDate)
                {
                    NSDate* syncDate = [NSDate date];
                    weakSelf.lastSyncDate = syncDate;
                    [[NSUserDefaults standardUserDefaults] setObject:syncDate forKey:kSPGSyncManagerSyncDateKey];
                }
                break;
            }
            case JLTaskStatus_Finished:
            {
                break;
            }
            default:
                break;
        }
    }];
}

#pragma mark - Public

+ (void)sync
{
    if(self.isSyncing == NO)
    {
#if _USE_SYNC_TIMER_
        SPGSyncManager* syncManager = [self sharedInstance];
        //call timer to initiate it
        [syncManager timer];
#endif
        
        [self syncRoomFeaturesForPropertyWithID:nil block:nil];
    }
}


+ (void)syncRoomFeaturesForPropertyWithID:(NSString*)propertyId block:(JLTaskObserverBlock)block
{
    SPGSyncManager* syncManager = [SPGSyncManager sharedInstance];
    if(syncManager.spgAppDelegate.reachability.reachable)
    {
        [SPGAlertView dismissAllAlertsWithTitle:syncManager.offlineErrorTitle message:syncManager.offlineErrorMsg];
        syncManager.alertShown = NO;
        
        NSString* tokenStr = [SPGAuthenticationManager sharedInstance].token;
        NSDictionary* taskInfo = @{@"envUrl" : [SPGSyncManager environmentURL],
                                   @"propertyId" : propertyId.copy ?: @"",
                                   @"apiKey" : [SPGSyncManager environmentAPIKey],
                                   @"secretKey" : [SPGSyncManager environmentSecretKey],
                                   @"userToken" : tokenStr ?: @""};
        
        [self runTaskWithName:@"syncRoomFeatures" taskInfo:taskInfo observerBlock:block];
    }
    else if(!syncManager.alertShown)
    {
        syncManager.alertShown = YES;
        [SPGAlertView showAlertWithTitle:syncManager.offlineErrorTitle message:syncManager.offlineErrorMsg completionBlock:^(NSInteger buttonIndex, JLAlertView *alertView) {
            
        } cancelButtonTitle:@"OK" otherButtonTitles: nil];
    }
}

+ (BOOL)isSyncing
{
    NSURL* taskUrl = [self taskUrlWithName:@"syncRoomFeatures"];
    BOOL isSyncRunning = NO;
    if([self isTaskInQueueWithURL:taskUrl taskInfo:nil])
    {
        isSyncRunning = YES;
    }
    return isSyncRunning;
}

+ (JLTaskObserver*)addGenericObserverForSyncRoomFeaturesWithBlock:(JLTaskObserverBlock)block
{
    NSString* taskName = @"syncRoomFeatures";
    return [self addGenericObserverForTaskWithName:taskName observerBlock:block];
}

+ (JLTaskObserver*)addGenericObserverForGetRoomFeaturesWithBlock:(JLTaskObserverBlock)block
{
    NSString* taskName = @"getRoomFeatures";
    return [SPGPropertyManager addGenericObserverForTaskWithName:taskName observerBlock:block];
}

+ (NSDate *)lastSyncDate
{
    return [[self sharedInstance] lastSyncDate];
}

+ (NSDate *)syncExpiryDate
{
    NSDate *syncDate = [SPGSyncManager lastSyncDate];
    NSTimeInterval day = ((60 * 60) * 24); // 24 hours
    NSDate *expireDate = [syncDate dateByAddingTimeInterval:day];
    return expireDate;
}

+ (BOOL)syncDurationExpired
{
    NSDate *syncExpires = [SPGSyncManager syncExpiryDate];
    if (!syncExpires)
    {
        syncExpires = [NSDate distantFuture]; // Never been synced;
    }
    NSDate *now = [NSDate date];
    NSComparisonResult compare = [syncExpires compare:now];
    BOOL expired = NO;
    if (compare == NSOrderedAscending)
    {
        expired = YES;
    }
    return expired;
}

@end
