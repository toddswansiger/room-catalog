//
//  SPGChangedAttributesCache.h
//  StarwoodSPG
//
//  Created by Nick Lee on 7/3/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPGChangedAttributesCache : NSObject

+ (instancetype)sharedCache;

- (id<NSCopying>)keyForCategoryCode:(NSString *)categoryCode roomNumber:(NSString *)roomNumber buildingID:(NSString *)buildingID propertyID:(NSString *)propertyID floorNumber:(NSString *)floorNumber;

- (BOOL)isCategoryCodeDirty:(NSString *)categoryCode roomNumber:(NSString *)roomNumber buildingID:(NSString *)buildingID propertyID:(NSString *)propertyID floorNumber:(NSString *)floorNumber;

- (void)markCategoryCodeDirty:(NSString *)categoryCode roomNumber:(NSString *)roomNumber buildingID:(NSString *)buildingID propertyID:(NSString *)propertyID floorNumber:(NSString *)floorNumber;

- (void)clearEverything;

- (void)persistSet;

@end
