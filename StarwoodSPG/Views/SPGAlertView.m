//
//  SPGAlertView.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/10/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGAlertView.h"

@implementation SPGAlertView
+ (void)showAlertWithError:(SPGError*)error completionBlock:(JLAlertViewCompletionBlock)block
{
    if (error)
    {
        if ([[[error userInfo] valueForKey:@"SPGErrorCode"] isEqualToString:@"OTA283"])
        {
            NSString* message = error.spgErrorMessage;
            NSString* code = error.spgErrorCode;
            NSString* errorMsg = [NSString stringWithFormat:@"Error %@ : %@", code, message];
            NSLog(@"%@. Presenting failed authentication alert.", errorMsg);
            NSString* errorTitle = @"Incorrect Login";
            
            errorMsg = [NSString stringWithFormat:@"Please check your Starwood ONE username and password and retry. If you continue to have issues please click the forgotten password link below."];

            [self showAlertWithTitle:errorTitle message:errorMsg completionBlock:block cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            
        }
        else if ([[[error userInfo] valueForKey:@"SPGErrorCode"] isEqualToString:@"-1"])
        {
            NSString* message = error.spgErrorMessage;
            NSString* code = error.spgErrorCode;
            NSString* errorMsg = [NSString stringWithFormat:@"Error %@ : %@", code, message];
            NSLog(@"%@. Presenting sync failred alert.", errorMsg);
            NSString* errorTitle = @"Sync Error";
            
            errorMsg = [NSString stringWithFormat:@"An error occurred when trying to sync. Please make sure you have a strong signal, check your connection and try again."];
            
            [self showAlertWithTitle:errorTitle message:errorMsg completionBlock:block cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            
        }
        else
        {
                NSString* message = error.spgErrorMessage;
                NSString* code = error.spgErrorCode;
                NSString* errorMsg = [NSString stringWithFormat:@"Error %@ : %@", code, message];
                NSLog(@"%@. Presenting sync failed alert.", errorMsg);
                NSString* errorTitle = @"Oops!";
                
                errorMsg = [NSString stringWithFormat:@"There was an error with syncing your data. Please try again shortly and if the issues persists please contact your Rooms Director for assistance."];
                
                if([code isEqual:@"-1009"])
                {
                    errorTitle = @"You are offline";
                    errorMsg = @"Your changes will not sync with Valhalla Portal until you reconnect to wifi or data service. To ensure work is not lost, we recommend you do this at the end of your shift.";
                }
                
                [self showAlertWithTitle:errorTitle message:errorMsg completionBlock:block cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        }
}
}
@end
