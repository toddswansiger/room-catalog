//
//  SPGRoomFeatureCell.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/29/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGRoomFeatureCell.h"
#import "UIImage+Tint.h"
#import "SPGStyleManager.h"

@implementation SPGRoomFeatureCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UIImage *infoImage = [[self infoButton] imageForState:UIControlStateNormal];
    infoImage = [infoImage imageTintedWithColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorStarwoodBlue]];
    [[self infoButton] setImage:infoImage forState:UIControlStateNormal];
    [self setFeatureStatus:SPGRoomFeatureStatusIncomplete];

    [[self titleLabel] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorStarwoodBlue]];
    [[self titleLabel] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontSubheadlineTwo]];
}

- (void)setFeatureStatus:(SPGRoomFeatureStatus)featureStatus
{
    UIImageView *statusIndicator = [self statusIndicatorView];
    CALayer *statusLayer = [statusIndicator layer];
    [statusLayer setCornerRadius:6.0f];
    
    switch (featureStatus) {
       
        case SPGRoomFeatureStatusComplete:
            
            statusLayer.borderWidth = 0.0f;
            statusIndicator.backgroundColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorGreen];
            statusIndicator.image = nil;
            break;
            
        case SPGRoomFeatureStatusIncomplete:
            
            statusLayer.borderWidth = 1.0f;
            statusLayer.borderColor = [[[SPGStyleManager sharedManager] colorForElement:SPGElementColorPowder] CGColor];
            statusIndicator.backgroundColor = [UIColor clearColor];
            statusIndicator.image = nil;
            break;
            
        case SPGRoomFeatureStatusLocked:
            
            statusIndicator.backgroundColor = [UIColor clearColor];
            statusLayer.borderWidth = 0.0f;
            statusLayer.cornerRadius = 0.0f;
            [statusIndicator setImage:[[UIImage imageNamed:@"Lock_Icon"] imageTintedWithColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorGrey]]];
            break;
    }
    
    _featureStatus = featureStatus;
}

@end
