//
//  SPGCountdownLabel.h
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 6/27/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPGCountdownLabel : UILabel

@property (strong, nonatomic) NSDate *targetDate;
@property (assign, nonatomic) BOOL animate;
@property (strong, nonatomic) NSString *countdownPrefixString;

- (void)updateCountdownClock;

@end
