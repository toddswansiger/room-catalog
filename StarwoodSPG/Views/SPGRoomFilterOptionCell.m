//
//  SPGRoomFilterOptionCell.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/11/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGRoomFilterOptionCell.h"
#import "SPGStyleManager.h"

@implementation SPGRoomFilterOptionCell

- (void)awakeFromNib
{
    [[self titleLabel] setTintColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorStarwoodBlue]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    if (selected)
    {
        [[self titleLabel] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorStarwoodBlue]];
    }
    else
    {
        [[self titleLabel] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal]];
    }
}

@end
