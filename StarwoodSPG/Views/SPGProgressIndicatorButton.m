//
//  SPGProgressIndicatorButton.m
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 5/27/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGProgressIndicatorButton.h"
#import "SPGStyleManager.h"

@interface SPGProgressIndicatorButton ()

- (IBAction)didPressDownloadButton:(id)sender;
- (IBAction)didPressCancelButton:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *downloadingView;
@property (strong, nonatomic) IBOutlet UIView *inactiveView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *downloadStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *progressStatusLabel; // Preceeding two labels appear as "one" label, where one is visibly masked by the progressing indicator view.
@property (weak, nonatomic) IBOutlet UIView *progressIndicatorView;

@end

@implementation SPGProgressIndicatorButton

// Dev purposes
- (void)sampleDownload
{
    CADisplayLink *link = [CADisplayLink displayLinkWithTarget:self selector:@selector(incrementProgress)];
    [link addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
}
- (void)incrementProgress
{
    CGFloat progress = _progress;
    progress = (progress + 0.01f);
    [self setProgress:progress];
}

- (void)awakeFromNib
{
    [self styleInterface];
    [self setButtonState:SPGProgressButtonStateReadyToDownload];
    [[self downloadingView] removeFromSuperview];
}

- (void)styleInterface
{
    [[UILabel appearanceWhenContainedIn:[self class], nil] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontSubheadlineOne]];
    
    [[self statusLabel] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorWhite]];
    [[self inactiveView] setBackgroundColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorStarwoodBlue]];
    
    [[self downloadingView] setBackgroundColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorPowder]];
    [[self progressIndicatorView] setBackgroundColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorGreen]];
    [[self progressStatusLabel] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorWhite]];
    [[self downloadStatusLabel] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorGrey]];
    
    CGRect statusLabelRect = self.statusLabel.frame;
    statusLabelRect = [[self progressIndicatorView] convertRect:statusLabelRect fromView:self];
    [[self progressStatusLabel] setFrame:statusLabelRect]; // Mirror the label that appears in the inactive view
    statusLabelRect = [[self downloadingView] convertRect:statusLabelRect fromView:[self progressIndicatorView]];
    [[self downloadStatusLabel] setFrame:statusLabelRect];
}

- (void)setProgress:(CGFloat)progress
{
    CGFloat width = (self.downloadingView.bounds.size.width * progress);
    width = MIN(width, self.bounds.size.width);
    CGRect progressFrame = self.progressIndicatorView.frame;
    progressFrame.size.width = width;
    [UIView animateWithDuration:.2 animations:^{
       [[self progressIndicatorView] setFrame:progressFrame];
    }];
        
    _progress = progress;
}

- (void)didPressDownloadButton:(id)sender
{
    if (self.buttonState != SPGProgressButtonStateDownloadComplete)
    {
        [[self delegate] progressButtonDidPressBeginDownload:self];
        [self setButtonState:SPGProgressButtonStateDownloading];
    }
}

- (void)didPressCancelButton:(id)sender
{
    [[self delegate] progressButtonDidPressCancelDownload:self];
    [self setButtonState:SPGProgressButtonStateReadyToDownload];
}

- (void)showDownloadingView
{
    [UIView transitionFromView:self.inactiveView
                        toView:self.downloadingView
                      duration:.5
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    completion:nil];
}

- (void)showInactiveView
{
    [UIView transitionFromView:self.downloadingView
                        toView:self.inactiveView
                      duration:.5
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    completion:nil];
}

- (NSString *)stringForButtonState:(SPGProgressButtonState)buttonState
{
    NSString *string;
    switch (buttonState)
    {
        case SPGProgressButtonStateReadyToDownload:
            string = @"DOWNLOAD";
            break;
        case SPGProgressButtonStateDownloading:
            string = @"DOWNLOADING...";
            break;
        case SPGProgressButtonStateDownloadComplete:
            string = @"DONE!";
            break;
        default:
            break;
    }
    
    return string;
}

- (void)setButtonState:(SPGProgressButtonState)buttonState
{
    switch (buttonState)
    {
        case SPGProgressButtonStateReadyToDownload:
            [self showInactiveView];
            [[self cancelButton] setHidden:YES];
            break;
            
        case SPGProgressButtonStateDownloading:
            
            [self showDownloadingView];
            [[self cancelButton] setHidden:NO];
            break;
    
        case SPGProgressButtonStateDownloadComplete:
            
            [[self cancelButton] setHidden:YES];
            [self showInactiveView];
            break;
        
        default:
            break;
    }
    
    [self addSubview:[self cancelButton]];
    [[self statusLabel] setText:[self stringForButtonState:buttonState]];
    [[self progressStatusLabel] setText:[self stringForButtonState:buttonState]];
    [[self downloadStatusLabel] setText:[self stringForButtonState:buttonState]];
    
    _buttonState = buttonState;
}

@end
