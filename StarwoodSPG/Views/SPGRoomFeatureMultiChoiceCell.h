//
//  SPGRoomFeatureMultiChoiceCell.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/28/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGRoomFeatureCell.h"

typedef NS_ENUM(NSInteger, SPGMultiChoiceCellState) {
    
    SPGMultiChoiceCellStateDefault,
    SPGMultiChoiceCellStateSelected,
    SPGMultiChoiceCellStateLocked
    
};

@interface SPGRoomFeatureMultiChoiceCell : SPGRoomFeatureCell

@property (nonatomic, weak) IBOutlet UILabel *disclsoureIndicatorLabel;
@property (nonatomic, assign) SPGMultiChoiceCellState cellState;

@end
