//
//  SPGRoomFeatureMultiChoiceCell.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/28/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGRoomFeatureMultiChoiceCell.h"
#import "SPGStyleManager.h"

@implementation SPGRoomFeatureMultiChoiceCell

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size
{
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [[self chooseButton] setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 6.0f, 0.0f, 0.0f)];
    [[[self chooseButton] titleLabel] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontBody]];
    
    CALayer *chooseLayer = [[self chooseButton] layer];
    [chooseLayer setCornerRadius:22.0f];
    [chooseLayer setMasksToBounds:YES];
    
}

- (void)setCellState:(SPGMultiChoiceCellState)cellState
{
    UIColor *backgroundColor = [UIColor whiteColor];
    CGFloat borderWidth = 1.4f;
    UIColor *borderColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorPowder alpha:.3];
    UIColor *textColor = [UIColor blackColor];
    BOOL showDisclosureIndicator = YES;
    
    switch (cellState)
    {
        case SPGMultiChoiceCellStateDefault:
            
            backgroundColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorPowder alpha:.3];
            borderWidth = 0.0f;
            textColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorGrey alpha:.4];
            break;
            
        case SPGMultiChoiceCellStateSelected:
            
            break;

        case SPGMultiChoiceCellStateLocked:
            
            showDisclosureIndicator = NO;
            borderColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorGrey alpha:.4];
            break;
            
    }
    
    
    CALayer *layer = [[self chooseButton] layer];
    [layer setBorderWidth:borderWidth];
    [layer setBorderColor:[borderColor CGColor]];
    [[self chooseButton] setBackgroundColor:backgroundColor];
    [[[self chooseButton] titleLabel] setTextColor:textColor];
    
    [[self disclsoureIndicatorLabel] setHidden:!showDisclosureIndicator];
    
    _cellState = cellState;
}

@end
