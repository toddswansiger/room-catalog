//
//  SPGRoomDetailHeaderView.h
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 6/23/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPGRoomDetailHeaderView : UIView

+ (SPGRoomDetailHeaderView *)roomDetailHeaderView;

@property (weak, nonatomic) UILabel *roomTypeLabel;
@property (weak, nonatomic) UILabel *descriptionLabel;

@end
