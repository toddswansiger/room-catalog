//
//  SPGRoomFeatureYesNoCell.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/28/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGRoomFeatureCell.h"

@class SPGToggleSwitch;

@interface SPGRoomFeatureYesNoCell : SPGRoomFeatureCell

@property (weak, nonatomic) IBOutlet SPGToggleSwitch *toggleSwitch;

@end
