//
//  SPGRoomCell.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/30/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPGRoomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *completionIndicatorView;
@property (weak, nonatomic) IBOutlet UILabel *roomNumberLabel;
@property (weak, nonatomic) IBOutlet UIImageView *flagImageView;
@property (weak, nonatomic) IBOutlet UIView *flagSolidColorView;
@property (assign, nonatomic) BOOL isComplete;
@property (assign, nonatomic) BOOL showFlagIndicator;

@end
