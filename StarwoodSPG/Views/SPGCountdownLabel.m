//
//  SPGCountdownLabel.m
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 6/27/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGCountdownLabel.h"
#import "SPGStyleManager.h"

@interface SPGCountdownLabel ()

@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) NSTimer *countdownTimer;

@end

@implementation SPGCountdownLabel

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    _dateFormatter = dateFormatter;

    return self;
}

- (void)setAnimate:(BOOL)animate
{
    if (animate)
    {
        if (![self countdownTimer])
        {
            NSTimer *countdownTimer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(updateCountdownClock) userInfo:[NSDictionary dictionary] repeats:YES];
            [[NSRunLoop mainRunLoop] addTimer:countdownTimer forMode:NSRunLoopCommonModes];
        }
        
        [self updateCountdownClock];
    }
    else
    {
        [[self countdownTimer] invalidate];
        [self setCountdownTimer:nil];
    }

    _animate = animate;
}

- (void)updateCountdownClock
{
    if ([self targetDate])
    {
        NSDate *currentDate = [NSDate date];
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [calendar components:NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit
                                                   fromDate:currentDate
                                                     toDate:[self targetDate]
                                                    options:0];
        
        NSInteger hour = [components hour];
        NSInteger minutes = [components minute];
        NSInteger seconds = [components second];
        
        if (![self countdownPrefixString])
        {
            [self setCountdownPrefixString:@""];
        }
        
        UIFont *plainFont = [[SPGStyleManager sharedManager] fontForName:lightFontName size:10];
        UIFont *boldFont = [[SPGStyleManager sharedManager] fontForName:boldCondensedFontName size:10];
        NSDictionary *plainAttributes = @{NSFontAttributeName: plainFont};
        NSDictionary *boldAttributes = @{NSFontAttributeName: boldFont};
        
        NSString *timeString = [NSString stringWithFormat:@"%ld HRS | %ld MINS | %ld SECS", (long)hour, (long)minutes, (long)seconds];
        NSString *expireString = [NSString stringWithFormat:@"%@ %@", [self countdownPrefixString], timeString];
        
        NSMutableAttributedString *expireAttributed = [[NSMutableAttributedString alloc] initWithString:expireString attributes:plainAttributes];
        [expireAttributed addAttributes:boldAttributes range:[expireString rangeOfString:timeString]];
        
        [self setAttributedText:expireAttributed];
        
    }
    else
    {
        [self setFont:[[SPGStyleManager sharedManager] fontForName:lightFontName size:10]];
        [self setText:@"Updating..."];
    }

}

- (void)dealloc
{
    [[self countdownTimer] invalidate];
}


@end
