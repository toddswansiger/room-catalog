//
//  SPGAlertView.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/10/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "JLAlertView.h"
#import "SPGError.h"

@class SPGError;

@interface SPGAlertView : JLAlertView

+ (void)showAlertWithError:(SPGError*)error completionBlock:(JLAlertViewCompletionBlock)block;
@end
