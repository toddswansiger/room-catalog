//
//  SPGOverviewBuildingCell.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/23/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DACircularProgressView;

@interface SPGOverviewBuildingCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *viewRoomsButton;
@property (weak, nonatomic) IBOutlet UIButton *largeViewRoomsButton;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet DACircularProgressView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *roomsCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *catalogedLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalRoomsLabel;
@property (weak, nonatomic) IBOutlet UIView *horizontalDividerView;

@end
