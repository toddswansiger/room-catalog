//
//  SPGToggleSwitch.m
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 6/1/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGToggleSwitch.h"
#import "SPGStyleManager.h"
#import "UIView+TDViewAdditions.h"

static CGFloat switchWidth = 70.0f;
static CGFloat cornerRadius = 22.0f;
static CGFloat labelWidth = 60.0f;
static NSString *yesString = @"Yes";
static NSString *noString = @"No";

@interface SPGToggleSwitch ()

@property (assign, nonatomic) CGPoint swipeYOrigin;
@property (strong, nonatomic) UIPanGestureRecognizer *panRecognizer;
@property (weak, nonatomic) UIView *draggableView;
@property (weak, nonatomic) UILabel *leftLabel;
@property (weak, nonatomic) UILabel *rightLabel;
@property (weak, nonatomic) UILabel *switchLabel;
@property (strong, nonatomic) NSArray *overlappingRectValues;
@property (strong, nonatomic) NSArray *labels;

@end

@implementation SPGToggleSwitch

- (void)awakeFromNib
{
    [self setupSwitch];
    [self setState:SPGToggleSwitchStateDefault];
}

- (void)setupSwitch
{
    UIColor *darkGreyColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorGrey alpha:.4];
    UIFont *labelFont = [[SPGStyleManager sharedManager] fontForElement:SPGElementFontBody];
    [[self layer] setBorderColor:darkGreyColor.CGColor];
    [[self layer] setBorderWidth:1.4f];
    
    CGFloat xOrigin = ((self.bounds.size.width / 2.0f) - (switchWidth / 2.0f));
    CGRect switchRect = CGRectMake(xOrigin, 1.0f, switchWidth, (self.bounds.size.height - 2.0f));
    UIView *switchView = [[UIView alloc] initWithFrame:switchRect];
    [switchView setUserInteractionEnabled:YES];
    [[switchView layer] setCornerRadius:cornerRadius];
    [[switchView layer] setShadowOpacity:0.2f];
    [[switchView layer] setShadowRadius:2.0f];
    [[switchView layer] setShadowOffset:CGSizeMake(1.0f, 3.0f)];
    [switchView setBackgroundColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorWhite]];
    [[self layer] setCornerRadius:cornerRadius];
    [self setDraggableView:switchView];
    
    CGRect switchLabelRect = CGRectMake(0.0f, 0.0f, switchView.bounds.size.width, switchView.bounds.size.height);
    UILabel *switchLabel = [[UILabel alloc] initWithFrame:switchLabelRect];
    [switchLabel setFont:labelFont];
    [switchLabel setTextAlignment:NSTextAlignmentCenter];
    [switchLabel setBackgroundColor:[UIColor clearColor]];
    [switchLabel setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorWhite]];
    [switchView addSubview:switchLabel];
    [switchView setCenter:[switchView center]];
    [self setSwitchLabel:switchLabel];
    
    CGRect leftLabelRect = CGRectMake(8.0f, 2.0f, labelWidth, switchView.bounds.size.height);
    UILabel *leftLabel = [[UILabel alloc] initWithFrame:leftLabelRect];
    [leftLabel setFont:labelFont];
    [leftLabel setTextAlignment:NSTextAlignmentCenter];
    [leftLabel setTextColor:darkGreyColor];
    [leftLabel setBackgroundColor:[UIColor clearColor]];
    [leftLabel setText:yesString];
    UITapGestureRecognizer *leftTapRecognzer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapLeftSide:)];
    [leftLabel addGestureRecognizer:leftTapRecognzer];
    [leftLabel setUserInteractionEnabled:YES];
    
    [self addSubview:leftLabel];
    
    CGFloat rightLabelXOrigin = (self.bounds.size.width - labelWidth - 8.0f);
    CGRect rightLabelRect = CGRectMake(rightLabelXOrigin, 2.0f, labelWidth, switchView.bounds.size.height);
    UILabel *rightLabel = [[UILabel alloc] initWithFrame:rightLabelRect];
    [rightLabel setBackgroundColor:[UIColor clearColor]];
    [rightLabel setFont:labelFont];
    [rightLabel setTextAlignment:NSTextAlignmentCenter];
    [rightLabel setTextColor:darkGreyColor];
    [rightLabel setText:noString];
    
    UITapGestureRecognizer *rightTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapRightSide:)];
    [rightLabel addGestureRecognizer:rightTapRecognizer];
    [rightLabel setUserInteractionEnabled:YES];
    [self addSubview:rightLabel];
    
    [self addSubview:switchView];
    [self setUserInteractionEnabled:YES];
    [self setLabels:@[switchLabel, rightLabel, leftLabel]];

    CGFloat lineWidth = 18.0f;
    CGFloat lineXOrigin = CGRectGetMidX(self.bounds) - (lineWidth / 2.0f);
    UIView *centerContainer = [[UIView alloc] initWithFrame:CGRectMake(lineXOrigin, 0.0f, lineWidth, self.bounds.size.height)];
    [centerContainer setOpaque:NO];
    [centerContainer setBackgroundColor:[UIColor clearColor]];
    CGFloat lineYOrigin = CGRectGetMidY(self.bounds);
    CGRect lineRect = CGRectMake(0, lineYOrigin, lineWidth, 1.0f);
    
    UIView *horizontalLine = [[UIView alloc] initWithFrame:lineRect];
    [horizontalLine setBackgroundColor:darkGreyColor];
    [centerContainer addSubview:horizontalLine];
    
    UITapGestureRecognizer *centerRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapCenter:)];
    [centerContainer addGestureRecognizer:centerRecognizer];
    [self insertSubview:centerContainer belowSubview:switchView];

    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(handleSwipeRecognizer:)];
    [switchView addGestureRecognizer:panRecognizer];
}

- (void)setState:(SPGToggleSwitchState)state animated:(BOOL)animate
{
    UIColor *switchColor;
    UIColor *backgroundColor;
    CGFloat xCenter = 0.0f;
    BOOL interactionEnabled = YES;
    NSString *labelText = yesString;
    
    switch (state)
    {
        case SPGToggleSwitchStateReadOnlyNo:
            switchColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorGrey];
            backgroundColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorGrey alpha:.2];
            xCenter = (self.bounds.size.width - (switchWidth / 2.0f));
            interactionEnabled = NO;
            labelText = noString;
            break;
        case SPGToggleSwitchStateReadOnlyYes:
            switchColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorGrey];
            backgroundColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorGrey alpha:.2];
            xCenter = (switchWidth / 2.0f);
            interactionEnabled = NO;
            break;
        case SPGToggleSwitchStateDefault:
            switchColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorWhite];
            backgroundColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorPowder alpha:.6];
            xCenter = CGRectGetMidX(self.bounds);
            labelText = nil;
            break;
        case SPGToggleSwitchStateNo:
            switchColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorRed];
            backgroundColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorPowder alpha:.6];
            xCenter = (self.bounds.size.width - (switchWidth / 2.0f));
            labelText = noString;
            break;
        case SPGToggleSwitchStateYes:
            switchColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorGreen];
            backgroundColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorPowder alpha:.6];
            xCenter = (switchWidth / 2.0f);
            break;
    }
    
    [[self panRecognizer] setEnabled:interactionEnabled];
    
    CGPoint dragCenter = self.draggableView.center;
    dragCenter.x = xCenter;
    
    void (^updateBlock)() = ^(){
        
        [[self draggableView] setBackgroundColor:switchColor];
        [self setBackgroundColor:backgroundColor];
        [[self draggableView] setCenter:dragCenter];
        [[self switchLabel] setText:labelText];
    };
    
    if (animate)
    {
        [UIView animateWithDuration:.3
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             updateBlock();
                             
                         } completion:^(BOOL finished) {
                             
                             _state = state;
                         }];
    }
    else
    {
        updateBlock();
        _state = state;
    }
}

- (void)setState:(SPGToggleSwitchState)state
{
    [self setState:state animated:NO];
}

- (SPGToggleSwitchState)stateForSwitchCenter:(CGPoint)switchCenter
{
    SPGToggleSwitchState state = SPGToggleSwitchStateDefault;
    
    if (!_overlappingRectValues)
    {
        CGFloat columnWidth = roundf(self.bounds.size.width / 3.0f);
        CGFloat height = self.bounds.size.height;
        
        CGRect firstColumn = CGRectMake(0.0f, 0.0f, columnWidth, height);
        CGRect secondColumn = CGRectMake(columnWidth, 0.0f, columnWidth, height);
        CGRect thirdColumn = CGRectMake((columnWidth * 2.0f), 0.0f, columnWidth, height);
        
        _overlappingRectValues = @[[NSValue valueWithCGRect:firstColumn], [NSValue valueWithCGRect:secondColumn], [NSValue valueWithCGRect:thirdColumn]];
    }
    
    NSInteger columnIndex = [UIView indexOfRectContainingPoint:switchCenter evaluateRects:[self overlappingRectValues]];
    if (columnIndex == 0)
    {
        state = SPGToggleSwitchStateYes;
    }
    else if (columnIndex == 1)
    {
        state = SPGToggleSwitchStateDefault;
    }
    else if (columnIndex == 2)
    {
        state = SPGToggleSwitchStateNo;
    }
    
    return state;
}

- (void)handleSwipeRecognizer:(id)sender
{
    if(self.state == SPGToggleSwitchStateReadOnlyYes || self.state == SPGToggleSwitchStateReadOnlyNo)
        return;
    
    UIPanGestureRecognizer *recognizer = (UIPanGestureRecognizer *)sender;
    UIView *dragView = recognizer.view;
    CGPoint translation = [recognizer translationInView:self];
    CGPoint destination = CGPointMake(dragView.center.x + translation.x,
                                      dragView.center.y);
    
    CGFloat minimumX = (0.0f + CGRectGetMidX(dragView.bounds));
    CGFloat maximumX = (self.bounds.size.width -  CGRectGetMidX(dragView.bounds));
    destination.x = MAX(minimumX, destination.x);
    destination.x = MIN(destination.x, maximumX);
    [dragView setCenter:destination];
    

    if ([recognizer state] == UIGestureRecognizerStateEnded)
    {
        SPGToggleSwitchState destinationState = [self stateForSwitchCenter:destination];
        BOOL shouldUpdateObservers = (self.state != destinationState);
        [self setState:destinationState animated:YES];
        
        if(shouldUpdateObservers) {
            [self updateObserversWithSwitchValueChange:destinationState];
        }
    }
    else if ([recognizer state] == UIGestureRecognizerStateCancelled)
    {
        [self setState:SPGToggleSwitchStateDefault animated:YES];
    }
    
    
    [recognizer setTranslation:CGPointMake(0, 0) inView:self];
}

- (void)didTapLeftSide:(id)sender
{
    if(self.state == SPGToggleSwitchStateReadOnlyYes || self.state == SPGToggleSwitchStateReadOnlyNo)
        return;
    
    [self setState:SPGToggleSwitchStateYes animated:YES];
   
    BOOL shouldUpdateObservers = (self.state != SPGToggleSwitchStateYes);
    if(shouldUpdateObservers)
    {
        [self updateObserversWithSwitchValueChange:SPGToggleSwitchStateYes];
    }
}

- (void)didTapCenter:(id)sender
{
    if(self.state == SPGToggleSwitchStateReadOnlyYes || self.state == SPGToggleSwitchStateReadOnlyNo)
        return;
    
    [self setState:SPGToggleSwitchStateDefault animated:YES];
    
    BOOL shouldUpdateObservers = (self.state != SPGToggleSwitchStateDefault);
    if (shouldUpdateObservers)
    {
        [self updateObserversWithSwitchValueChange:SPGToggleSwitchStateDefault];
    }
}

- (void)didTapRightSide:(id)sender
{
    if(self.state == SPGToggleSwitchStateReadOnlyYes || self.state == SPGToggleSwitchStateReadOnlyNo)
        return;
    
    [self setState:SPGToggleSwitchStateNo animated:YES];
    
    BOOL shouldUpdateObservers = (self.state != SPGToggleSwitchStateNo);
    if(shouldUpdateObservers)
    {
        [self updateObserversWithSwitchValueChange:SPGToggleSwitchStateNo];
    }
}

- (void)updateObserversWithSwitchValueChange:(SPGToggleSwitchState)state
{
    switch (state)
    {
        case SPGToggleSwitchStateDefault:
            [[self delegate] toggleSwitchDidReset:self];
            break;
            
        case SPGToggleSwitchStateNo:
            [[self delegate] toggleSwitchDidSelectNo:self];
            break;
            
        case SPGToggleSwitchStateYes:
            [[self delegate] toggleSwitchDidSelectYes:self];
            break;
            
            default:
            break;
    }
}

@end
