//
//  SPGNavigationBar.h
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 6/10/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPGNavigationBar : UINavigationBar

@end
