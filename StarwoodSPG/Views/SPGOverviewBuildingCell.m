//
//  SPGOverviewBuildingCell.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/23/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGOverviewBuildingCell.h"
#import "SPGStyleManager.h"
#import "DACircularProgressView.h"

@implementation SPGOverviewBuildingCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [self applyStyle];
}

- (void)applyStyle
{
    self.backgroundColor = [UIColor clearColor];
    
    CGRect leftDividerFrame = CGRectMake(-10.0f, 6.0f, 0.5f, self.bounds.size.height - 12.0f);
    UIView *leftDivider = [[UIView alloc] initWithFrame:leftDividerFrame];
    [leftDivider setBackgroundColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal alpha:0.4]];
    [[self contentView] addSubview:leftDivider];
    
    [self.viewRoomsButton setBackgroundColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorStarwoodBlue alpha:1]];
    [self.viewRoomsButton setTintColor:[UIColor whiteColor]];
    
    [[[self viewRoomsButton] titleLabel] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontSubheadlineTwo]];
    [[self titleLabel] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontSubheadlineOne]];
    [[self titleLabel] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal alpha:.9]];
  
    [[self roomsCountLabel] setFont:[[SPGStyleManager sharedManager] fontForName:condensedFontName size:48.0f]];
    [[self roomsCountLabel] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal]];
    
    [[self catalogedLabel] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal]];
    [[self catalogedLabel] setFont:[[SPGStyleManager sharedManager] fontForName:boldCondensedFontName size:16]];
    
    UIView *divider = self.horizontalDividerView;
    CGRect dividerRect = divider.frame;
    dividerRect.size.height = 0.5f;
    [divider setFrame:dividerRect];
    [divider setBackgroundColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorPowder]];
    [[self totalRoomsLabel] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal alpha:0.9]];
    [[self totalRoomsLabel] setFont:[[SPGStyleManager sharedManager] fontForName:boldCondensedFontName size:16]];

    DACircularProgressView *circularProgressViewAppearance = [DACircularProgressView appearance];
    [circularProgressViewAppearance setTrackTintColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal alpha:.09]];
    [circularProgressViewAppearance setProgressTintColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorIndigo]];
    [circularProgressViewAppearance setBackgroundColor:[UIColor clearColor]];
    [circularProgressViewAppearance setThicknessRatio:0.12f];
    [circularProgressViewAppearance setRoundedCorners:YES];
    
}

- (void)dealloc
{
    self.viewRoomsButton = nil;
}

@end
