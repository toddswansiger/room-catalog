//
//  SPGNavigationBar.m
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 6/10/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGNavigationBar.h"

@implementation SPGNavigationBar

- (void)awakeFromNib
{
    [self setBarTintColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorStarwoodBlue]];
    [self setBarStyle:UIBarStyleBlack];
    [self setTranslucent:NO];
    [self setOpaque:YES];
    
    NSDictionary *textAttributes = @{NSFontAttributeName: [[SPGStyleManager sharedManager] fontForElement:SPGElementFontSubheadlineOne]};
    [self setTitleTextAttributes:textAttributes];
}

@end
