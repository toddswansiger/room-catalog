//
//  SPGRoomDetailHeaderView.m
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 6/23/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGRoomDetailHeaderView.h"
#import "SPGStyleManager.h"

@implementation SPGRoomDetailHeaderView

+ (SPGRoomDetailHeaderView *)roomDetailHeaderView
{
    SPGRoomDetailHeaderView *view = [[SPGRoomDetailHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 50.0f)];
    return view;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setAutoresizingMask:UIViewAutoresizingNone];
        [self setAutoresizesSubviews:NO];
        
        UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(9.0f, 6.0f, 128.0f, 21.0f)];
        _descriptionLabel = descriptionLabel;
        [self addSubview:descriptionLabel];
        [descriptionLabel setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontSubheadlineOne]];
        [descriptionLabel setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorStarwoodBlue]];
    
        UILabel *roomTypeLabel = [[UILabel alloc] initWithFrame:CGRectMake(9.0f, 28.0f, 291.0f, 21.0f)];
        _roomTypeLabel = roomTypeLabel;
        [self addSubview:roomTypeLabel];
        [roomTypeLabel setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontBody]];
        [roomTypeLabel setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal]];
    }
    
    return self;
}

@end
