//
//  SPGProgressIndicatorButton.h
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 5/27/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SPGProgressButtonState) {
    
    SPGProgressButtonStateReadyToDownload,
    SPGProgressButtonStateDownloading,
    SPGProgressButtonStateDownloadComplete
    
};

@class SPGProgressIndicatorButton;

@protocol SPGProgressIndicatorButtonDelegate <NSObject>

- (void)progressButtonDidPressBeginDownload:(SPGProgressIndicatorButton *)button;
- (void)progressButtonDidPressCancelDownload:(SPGProgressIndicatorButton *)button;

@end

@interface SPGProgressIndicatorButton : UIView

@property (nonatomic, assign) IBOutlet id<SPGProgressIndicatorButtonDelegate> delegate;
@property (nonatomic, assign) SPGProgressButtonState buttonState;
@property (nonatomic, assign) CGFloat progress;

@end
