//
//  SPGRoomAttributeSectionHeader.m
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 7/1/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGRoomAttributeSectionHeader.h"
#import "SPGStyleManager.h"

@implementation SPGRoomAttributeSectionHeader

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 44.0f, 30.0f)];
        [titleLabel setFont:[[SPGStyleManager sharedManager] fontForName:boldCondensedFontName size:14]];
        [titleLabel setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal]];
        _titleLabel = titleLabel;
        [self addSubview:titleLabel];
        
        _showsDisclosureButton = YES;
        
        UIButton *hideButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 44.0f, 30.0f)];
        [[hideButton titleLabel] setFont:[[SPGStyleManager sharedManager] fontForName:lightFontName size:14]];
        [hideButton setTitleColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorStarwoodBlue] forState:UIControlStateNormal];
        [hideButton setTitleColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorPowder] forState:UIControlStateHighlighted];
        [self addSubview:hideButton];
        
        _disclosureButton = hideButton;
                
        [self setUserInteractionEnabled:YES];
    
    }
    return self;
}

- (void)layoutSubviews
{
    CGRect frame = self.frame;
    CGSize labelSize = CGSizeMake(120.0f, 20.0f);
    CGRect labelRect = CGRectMake(12.0f, (CGRectGetMidY(self.bounds) - ((labelSize.height / 2.0f) - 2)), labelSize.width, labelSize.height);
    [[self titleLabel] setFrame:labelRect];
    
    CGSize buttonSize = CGSizeMake(80.0f, 44.0f);
    CGFloat yOrigin = (CGRectGetMidY(self.bounds) - ((buttonSize.height / 2.0f) - 2.0f));
    CGFloat xOrigin = frame.size.width - (buttonSize.width + 12.0f);
    
    CGRect hideButtonRect = CGRectMake(xOrigin, yOrigin, buttonSize.width, buttonSize.height);
    [[self disclosureButton] setFrame:hideButtonRect];
}

- (void)setShowsDisclosureButton:(BOOL)showsDisclosureButton
{
     _showsDisclosureButton = showsDisclosureButton;
    [[self disclosureButton] setHidden:!showsDisclosureButton];
   
}

@end
