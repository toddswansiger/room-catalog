//
//  SPGRoomFeatureCell.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/29/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SPGRoomFeatureStatus)
{
    SPGRoomFeatureStatusIncomplete,
    SPGRoomFeatureStatusComplete,
    SPGRoomFeatureStatusLocked
};

@interface SPGRoomFeatureCell : UITableViewCell

@property (assign, nonatomic) SPGRoomFeatureStatus featureStatus;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *chooseButton;
@property (strong, nonatomic) IBOutlet UIButton *infoButton;
@property (strong, nonatomic) IBOutlet UIButton *largeInfoButton;
@property (strong, nonatomic) IBOutlet UIImageView *statusIndicatorView;

@end
