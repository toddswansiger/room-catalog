//
//  SPGRoomAttributeSectionHeader.h
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 7/1/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPGRoomAttributeSectionHeader : UITableViewHeaderFooterView

@property (nonatomic, assign) BOOL showsDisclosureButton;
@property (nonatomic, weak) UILabel *titleLabel;
@property (nonatomic, weak) UIButton *disclosureButton;

@end
