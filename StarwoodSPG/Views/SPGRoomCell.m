//
//  SPGRoomCell.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/30/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGRoomCell.h"
#import "SPGStyleManager.h"
#import "UIImage+Tint.h"
#import "SPGStyleManager.h"

@implementation SPGRoomCell

- (void)awakeFromNib
{
    self.completionIndicatorView.layer.cornerRadius = 5.0f;
    [[self roomNumberLabel] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontSubheadlineOne]];
    
    UIImage *flagImage = [[[self flagImageView] image] imageTintedWithColor:[UIColor whiteColor]];
    [[self flagImageView] setImage:flagImage];
    [[self flagSolidColorView] setBackgroundColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorRed]];
}

- (void)setIsComplete:(BOOL)isComplete
{
    if (isComplete)
    {
        self.completionIndicatorView.backgroundColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorGreen];
        self.completionIndicatorView.layer.borderWidth = 0.0f;
    }
    else
    {
        self.completionIndicatorView.backgroundColor = [UIColor whiteColor];
        self.completionIndicatorView.layer.borderWidth = 0.5f;
        UIColor *borderColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorGrey];
        self.completionIndicatorView.layer.borderColor = borderColor.CGColor;
    }
}

- (void)setShowFlagIndicator:(BOOL)showFlagIndicator
{
    [[self flagSolidColorView] setHidden:!showFlagIndicator];
    _showFlagIndicator = showFlagIndicator;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
