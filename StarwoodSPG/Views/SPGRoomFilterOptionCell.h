//
//  SPGRoomFilterOptionCell.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/11/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPGRoomFilterOptionCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *checkmarkImageView;

@end
