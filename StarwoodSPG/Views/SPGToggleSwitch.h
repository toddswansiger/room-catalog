//
//  SPGToggleSwitch.h
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 6/1/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SPGToggleSwitchState)
{
    SPGToggleSwitchStateReadOnlyYes,
    SPGToggleSwitchStateReadOnlyNo,
    SPGToggleSwitchStateDefault,
    SPGToggleSwitchStateYes,
    SPGToggleSwitchStateNo,
};

@class SPGToggleSwitch;

@protocol SPGToggleSwitchDelegate <NSObject>

- (void)toggleSwitchDidSelectYes:(SPGToggleSwitch *)toggleSwitch;
- (void)toggleSwitchDidSelectNo:(SPGToggleSwitch *)toggleSwitch;
- (void)toggleSwitchDidReset:(SPGToggleSwitch *)toggleSwitch;

@end

@interface SPGToggleSwitch : UIView

@property (weak, nonatomic) id<SPGToggleSwitchDelegate> delegate;
@property (assign, nonatomic) SPGToggleSwitchState state;
- (void)setState:(SPGToggleSwitchState)state animated:(BOOL)animate;

@end
