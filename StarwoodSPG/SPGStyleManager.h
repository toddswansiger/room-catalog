//
//  SPGStyleManager.h
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 5/23/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>

#define RGBAColor(r,g,b,a) [UIColor colorWithRed:(r/255.0f) green:(g/255.0f) blue:(b/255.0f) alpha:(a)]

typedef NS_ENUM(NSUInteger, SPGElementColor)
{
    SPGElementColorStarwoodBlue,
    SPGElementColorAqua,
    SPGElementColorIndigo,
    SPGElementColorWhite,
    SPGElementColorCharcoal,
    SPGElementColorGrey,
    SPGElementColorPowder,
    SPGElementColorGreen,
    SPGElementColorRed
};

typedef NS_ENUM(NSUInteger, SPGElementFont)
{
    SPGElementFontHeadline,
    SPGElementFontSubheadlineOne,
    SPGElementFontSubheadlineTwo,
    SPGElementFontBody
};

extern NSString * const condensedFontName;
extern NSString * const boldCondensedFontName;
extern NSString * const lightFontName;

@interface SPGStyleManager : NSObject

+ (SPGStyleManager *)sharedManager;
- (UIColor *)colorWithRedValue:(CGFloat)red greenValue:(CGFloat)green blueValue:(CGFloat)blue;
- (UIFont *)fontForElement:(SPGElementFont)element;
- (UIFont *)fontForName:(NSString *)name size:(CGFloat)size;
- (UIColor *)colorForElement:(SPGElementColor)element;
- (UIColor *)colorForElement:(SPGElementColor)element alpha:(float)alpha;

@end
