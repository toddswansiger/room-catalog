//
//  SPGContstants.h
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 6/10/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#ifndef StarwoodSPG_SPGContstants_h
#define StarwoodSPG_SPGContstants_h

static NSString *SPGLogoutNotification  = @"spg.com.logoutNotification";
static NSString *SPGLoginNotification = @"spg.com.loginNotification";
static NSString *SPGAPIKeyDeprecatedNotification = @"spg.com.apiKeyDeprecated";

#endif
