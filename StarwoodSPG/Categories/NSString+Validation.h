//
//  NSString+Validation.h
//
//  Created by Nick Lee on 6/20/13.
//  Copyright (c) 2013 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef struct LengthConstraint {
	NSUInteger inclusiveMin;
	NSUInteger inclusiveMax;
} LengthConstraint;

extern LengthConstraint LengthConstraintMake(NSUInteger inclusiveMin, NSUInteger inclusiveMax);
extern BOOL Validate_b(NSString* s);
extern NSNumber* Validate_n(NSString* s);

@interface NSString (Validation)

-(NSString*)isEmail;
-(NSString*)matchesCharacterSet:(NSCharacterSet*)characterSet;
-(NSString*)isAlphanumeric;
-(NSString*)isAlphabetical;
-(NSString*)isNumeric;
-(NSString*)fitsLengthConstraints:(LengthConstraint)constraint;
-(NSString*)isEmptyShouldTrimWhitespace:(BOOL)trim;
-(NSString*)isNotEmptyShouldTrimWhitespace:(BOOL)trim;
-(NSString*)isURL;

@end
