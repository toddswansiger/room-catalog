//
//  UINavigationController+MenuNavigation.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/21/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "UINavigationController+MenuNavigation.h"

@implementation UINavigationController (MenuNavigation)

- (UIBarButtonItem*)menuButton
{
    static UIBarButtonItem* _menuButton = nil;
    
    if(!_menuButton)
    {
        _menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"User_Icon"] style:UIBarButtonItemStyleBordered target:self action:@selector(menuButtonSelected:)];
        [_menuButton setTintColor:[UIColor whiteColor]];
    }
    return _menuButton;
}

- (void)addMenuButton
{
    [self addRightBarButtonItem:self.menuButton];
}

- (void)removeMenuButton
{
    [self removeRightBarButtonItem:self.menuButton];
}

#pragma mark - Generic methods for adding removing UIBarButtonItems

- (UIViewController*)viewControllerWithRightBarButton:(UIBarButtonItem*)barButton
{
    UIViewController* aViewController = nil;
    // Must use reverse enumerator because of stack hierarchy
    // plus there CAN be mutliple view controllers with the same barButton
    // and we want to remove the 1st one
    for(UIViewController* viewController in self.viewControllers.reverseObjectEnumerator)
    {
        if([viewController.navigationItem.rightBarButtonItems containsObject:barButton])
        {
            aViewController = viewController;
            break;
        }
    }
    return aViewController;
}

- (void)removeRightBarButtonItem:(UIBarButtonItem*)item
{
    UIViewController* aViewController = [self viewControllerWithRightBarButton:item];
    if(aViewController)
    {
        NSMutableArray* rightBarButtons = [NSMutableArray arrayWithArray:aViewController.navigationItem.rightBarButtonItems];
        [rightBarButtons removeObject:item];
        
        aViewController.navigationItem.rightBarButtonItems = rightBarButtons;
    }
}

- (void)addRightBarButtonItem:(UIBarButtonItem*)item
{
    if(![self.topViewController.navigationItem.rightBarButtonItems containsObject:item])
    {
        NSMutableArray* rightBarButtons = [NSMutableArray arrayWithArray:self.topViewController.navigationItem.rightBarButtonItems];
        NSInteger index = 0;
        [rightBarButtons insertObject:item atIndex:index];
        self.topViewController.navigationItem.rightBarButtonItems = rightBarButtons;
    }
}

- (void)menuButtonSelected:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kMenuNavigationControllerMenuButtonSelectedNotification object:self];
}

@end
