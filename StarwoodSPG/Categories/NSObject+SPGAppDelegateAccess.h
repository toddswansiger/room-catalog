//
//  NSObject+SPGAppDelegateAccess.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 7/9/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SPGAppDelegate.h"

@interface NSObject (SPGAppDelegateAccess)
- (SPGAppDelegate*)spgAppDelegate;
@end
