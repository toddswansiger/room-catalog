//
//  NSString+Validation.m
//
//  Created by Nick Lee on 6/20/13.
//  Copyright (c) 2013 Tendigi. All rights reserved.
//

#import "NSString+Validation.h"

LengthConstraint LengthConstraintMake(NSUInteger inclusiveMin, NSUInteger inclusiveMax)
{
	return (LengthConstraint){inclusiveMin, inclusiveMax};
}

BOOL Validate_b(NSString* s)
{
	return s != nil;
}

NSNumber* Validate_n(NSString* s)
{
	return @(Validate_b(s));
}

@implementation NSString (Validation)

-(NSString*)isEmptyShouldTrimWhitespace:(BOOL)trim;
{
	if (trim) {
		return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0 ? self : nil;
	}
	return self.length == 0 ? self : nil;
}

-(NSString*)isNotEmptyShouldTrimWhitespace:(BOOL)trim;
{
	return [self isEmptyShouldTrimWhitespace:trim] ? nil : self;
}

-(NSString*)isURL
{
	NSURL *candidateURL = [NSURL URLWithString:self];
	// WARNING > "test" is an URL according to RFCs, being just a path
	// so you still should check scheme and all other NSURL attributes you need
	if (candidateURL && candidateURL.scheme && candidateURL.host) {
		return self;
	}
	return nil;
}

-(NSString*)isEmail;
{
	NSString* checkString = self;
	NSString* emailRegex = @"^([\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4})?$";
	NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
	return [emailTest evaluateWithObject:checkString] ? self : nil;
}

-(NSString*)matchesCharacterSet:(NSCharacterSet*)characterSet;
{
	NSCharacterSet* otherSet = [characterSet invertedSet];
	return [self rangeOfCharacterFromSet:otherSet].location == NSNotFound ? self : nil;
}

-(NSString*)isAlphanumeric;
{
	return [self matchesCharacterSet:[NSCharacterSet alphanumericCharacterSet]];
}

-(NSString*)isAlphabetical;
{
	return [self matchesCharacterSet:[NSCharacterSet letterCharacterSet]];
}

-(NSString*)isNumeric;
{
	return [self matchesCharacterSet:[NSCharacterSet decimalDigitCharacterSet]];
}

-(NSString*)fitsLengthConstraints:(LengthConstraint)constraint;
{
	return (self.length >= constraint.inclusiveMin && self.length <= constraint.inclusiveMax) ? self : nil;
}

@end
