//
//  NSDictionary+Merge.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/26/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Merge)
+ (NSDictionary *)dictionaryByMerging:(NSDictionary *)dict1 with:(NSDictionary *)dict2;
- (NSDictionary *)dictionaryByMergingWith:(NSDictionary *)dict;
@end
