//
//  NSString+StringContents.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/30/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (StringContents)
- (BOOL)containsString:(NSString*)substring;
- (BOOL)containsString:(NSString*)substring ignoreCase:(BOOL)ignoreCase;
- (BOOL)equalToString:(NSString*)substring ignoreCase:(BOOL)ignoreCase;
@end
