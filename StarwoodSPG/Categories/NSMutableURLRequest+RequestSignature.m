//
//  NSMutableURLRequest+RequestSignature.m
//  SPG
//
//  Created by Bobby Schuchert on 10/7/15.
//  Copyright © 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "NSMutableURLRequest+RequestSignature.h"
#import <CommonCrypto/CommonCrypto.h>

@implementation NSMutableURLRequest (RequestSignature)

-(void)signRequestUsingAccessKey:(NSString *)accessKey secretKey:(NSString *)secretKey {

    if (accessKey && secretKey) {
        
        NSString *url = [[self URL] relativePath]; //just use the relative path of the URL for signature
        NSString *method = [self HTTPMethod];
        NSString *timestamp = [self currentEpochTime];
        
        // build our signature
        NSString *signature = [self signatureWithUrl:url requestMethod:method accessKey:accessKey secretKey:secretKey timestamp:timestamp];
        
        [self setValue:accessKey forHTTPHeaderField:@"accessKey"];
        [self setValue:timestamp forHTTPHeaderField:@"timestamp"];
        [self setValue:signature forHTTPHeaderField:@"signature"];
    }
}


#pragma mark - Signature Methods

-(NSString *)signatureWithUrl:(NSString *)url requestMethod:(NSString *)method accessKey:(NSString *)accessKey
                    secretKey:(NSString *)secretKey timestamp:(NSString *)timestamp
{
    
    NSString *stringToHash = [NSString stringWithFormat:@"url=%@|requestMethod=%@|accessKey=%@|secretKey=%@|timestamp=%@", url, method, accessKey, secretKey, timestamp];
    NSString *signature = [self hmacsha1:stringToHash secret:secretKey];
    
    return signature;
}



#pragma mark - Utility Methods

-(NSString *)currentEpochTime {
    
    NSDate *now = [NSDate date];
    double epoch = [now timeIntervalSince1970];
    long long epochMilliseconds = (epoch * 1000);
    NSString *string = [NSString stringWithFormat:@"%lli", epochMilliseconds];
    
    return string;
}

- (NSString *)hmacsha1:(NSString *)data secret:(NSString *)key {
    
    const char *cKey  = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [data cStringUsingEncoding:NSASCIIStringEncoding];
    
    unsigned char cHMAC[CC_SHA1_DIGEST_LENGTH];
    
    CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    
    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    
    NSString *hash = [HMAC base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    return hash;
}


@end
