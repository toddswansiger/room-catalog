//
//  UIViewController+MainViewController.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/21/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPGMainViewController.h"

@class SPGAppDelegate;

@interface UIViewController (SPGAdditions)

+ (SPGMainViewController*)mainViewController;
+ (SPGAppDelegate *)appDelegate;
- (CGFloat)navigationBarHeight;
- (void)addCustomBackButton;


@end
