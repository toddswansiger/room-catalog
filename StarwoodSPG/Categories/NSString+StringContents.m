//
//  NSString+StringContents.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/30/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "NSString+StringContents.h"

@implementation NSString (StringContents)
- (BOOL)containsString:(NSString*)substring
{
	return ([self rangeOfString:substring].length != 0);
}

- (BOOL)containsString:(NSString*)substring ignoreCase:(BOOL)ignoreCase
{
	if( ignoreCase )
		return ([self rangeOfString:substring options:NSCaseInsensitiveSearch].length != 0);
	else
		return ([self rangeOfString:substring].length != 0);
}

- (BOOL)equalToString:(NSString*)substring ignoreCase:(BOOL)ignoreCase
{
	if( ignoreCase )
		return ([self rangeOfString:substring options:NSCaseInsensitiveSearch].length == substring.length && self.length == substring.length);
	else
		return ([self rangeOfString:substring].length == substring.length && self.length == substring.length);
}
@end
