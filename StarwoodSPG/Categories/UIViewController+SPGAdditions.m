//
//  UIViewController+MainViewController.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/21/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

@import CoreText;

#import "UIViewController+SPGAdditions.h"
#include <objc/runtime.h>
#import "NTAttributedStringBuilder.h"
#import "SPGStyleManager.h"

@implementation UIViewController (SPGAdditions)

static UIView *visibleOverlayView = nil;

+ (SPGMainViewController*)mainViewController
{
    SPGMainViewController* mainViewController = nil;
    
    if([[[[[UIApplication sharedApplication] delegate] window] rootViewController] isKindOfClass:[SPGMainViewController class]])
    {
        mainViewController = (SPGMainViewController*)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    }
    
    return mainViewController;
}

+ (SPGAppDelegate *)appDelegate
{
    return (SPGAppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (CGFloat)navigationBarHeight
{
    CGFloat height = 0.0f;
    if ([[self navigationController] navigationBar])
    {
        height = self.navigationController.navigationBar.bounds.size.height;
    }
    
    return height;
}

- (void)addCustomBackButton
{
    [[self navigationItem] setHidesBackButton:YES];
    UIImage *backImage = [UIImage imageNamed:@"Left-Arrow_Icon"];
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, backImage.size.width, backImage.size.height)];
    [backButton setImage:backImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didPressBackButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [[self navigationItem] setLeftBarButtonItem:backItem];
}

- (void)didPressBackButton:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}

@end
