//
//  UINavigationController+MenuNavigation.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/21/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kMenuNavigationControllerMenuButtonSelectedNotification @"MenuNavigationControllerMenuButtonSelectedNotification"

@interface UINavigationController (MenuNavigation)

- (void)addMenuButton;
- (void)addRightBarButtonItem:(UIBarButtonItem*)item;

@end
