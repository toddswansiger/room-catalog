//
//  UIView+ZSViewAdditions.h
//  Zuse
//
//  Created by Isaac Schmidt on 12/10/12.
//
//

#import <UIKit/UIKit.h>

@interface UIView (TDViewAdditions)

+ (UIView *)viewContainingPoint:(CGPoint)point evaluateViews:(NSArray *)views;
+ (UIView *)viewContainingRect:(CGRect)rect evaluateViews:(NSArray *)views;
+ (NSUInteger)indexOfRectContainingPoint:(CGPoint)point evaluateRects:(NSArray *)evaluate;
+ (NSUInteger)indexOfRectContainingRect:(CGRect)rect evaluateRects:(NSArray *)evaluate;
- (UIImage *)renderedImage;
+ (NSArray *)frameValuesForViews:(NSArray *)views;
- (NSArray *)frameValuesForSubviews;
- (NSArray *)subviewsContainedInRect:(CGRect)rect;
- (NSArray *)subviewsIntersectingRect:(CGRect)rect;
- (NSArray *)frameValuesForSubviewsInRect:(CGRect)rect;

@end
