//
//  NSObject+SPGAppDelegateAccess.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 7/9/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "NSObject+SPGAppDelegateAccess.h"

@implementation NSObject (SPGAppDelegateAccess)
- (SPGAppDelegate*)spgAppDelegate
{
    SPGAppDelegate* appDelegate = (SPGAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if(![appDelegate isKindOfClass:[SPGAppDelegate class]])
    {
        appDelegate = nil;
    }
    return appDelegate;
}

@end
