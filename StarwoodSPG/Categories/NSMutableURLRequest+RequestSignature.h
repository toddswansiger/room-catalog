//
//  NSMutableURLRequest+RequestSignature.h
//  SPG
//
//  Created by Bobby Schuchert on 10/7/15.
//  Copyright © 2015 Bottle Rocket, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableURLRequest (RequestSignature)

-(void)signRequestUsingAccessKey:(NSString *)accessKey secretKey:(NSString *)secretKey;

@end
