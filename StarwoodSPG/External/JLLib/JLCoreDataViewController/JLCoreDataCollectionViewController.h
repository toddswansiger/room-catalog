//
//  JLCoreDataCollectionViewController.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "JLCoreDataViewController.h"

@interface JLCoreDataCollectionViewController : JLCoreDataViewController <UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource>

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UICollectionViewFlowLayout *collectionViewFlowLayout;
@property (strong, nonatomic) NSString *cellIdentifier;
@property (assign, nonatomic) BOOL showIndex;
@property (assign, nonatomic) BOOL canRefresh;
@property (assign, nonatomic) BOOL isRefreshRunning;
- (void)fetchedResultsController:(NSFetchedResultsController *)controller configureCell:(UICollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

@end
