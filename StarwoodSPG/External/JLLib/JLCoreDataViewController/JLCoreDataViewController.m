//
//  JLCoreDataViewController.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "JLCoreDataViewController.h"

@interface JLCoreDataViewController ()
@property (nonatomic, readwrite) BOOL isFetchRunning;

@end

@implementation JLCoreDataViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self initVars];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	if (self = [super initWithCoder:aDecoder]) {
        [self initVars];
    }
	return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.fetchedResultsController.delegate = nil;
	self.fetchedResultsController = nil;
    self.fetchRequest = nil;
}

- (void) initVars{
    self.entityName = nil;
    self.searchField = nil;
    self.sectionKeyPath = nil;
    self.filter = nil;
    self.cacheName = @"Root";
    self.prefetchRelationship = nil;
    self.isFetchRunning = NO;
}

#pragma mark - UIViewController LifeCycle
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if(self.isFetchRunning == NO)
    {
        NSInteger count = [self.fetchedResultsController.fetchedObjects count];
        if(count == 0){
            //wait to ensure animations are done.
            [self fetchData];
        }
    }
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        
	}
}

- (void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.fetchedResultsController.delegate = nil;
	self.fetchedResultsController = nil;
    self.fetchRequest = nil;
    [super viewDidUnload];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

#pragma mark - Methods
- (BOOL)isVisible
{
    BOOL isVisible = (self.isViewLoaded && self.view.window);
    isVisible = isVisible && (self.parentViewController.navigationController ? self.parentViewController.navigationController.visibleViewController == self.parentViewController : self.navigationController.visibleViewController == self);
    
    return isVisible;
}

- (BOOL)isLandscape
{
    return UIInterfaceOrientationIsLandscape(self.interfaceOrientation);
}

- (UIInterfaceOrientation)interfaceOrientation
{
    return [[UIApplication sharedApplication] statusBarOrientation];
}

- (void)resetFetchResultsControllerWithPredicate:(NSPredicate *)predicate{
    //clear the cache of FRC
    [NSFetchedResultsController deleteCacheWithName:self.cacheName];
    
    //change the predicate of the fetch request
    self.filter = predicate;
    
    //reset the delegate and
    self.fetchedResultsController.delegate = nil;
    // Properly release old FetchedResultsController
    self.fetchedResultsController = nil;
    
    NSError *error = nil;
    // create a new object and perform fetch
    
	if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"error in the performfetch");
	}
    
    [self reloadData];
}

- (NSInteger)numberOfObjectsForSection:(NSInteger)section{
    if([self.fetchedResultsController sections].count > 0){
        id<NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    }else {
        return 0;
    }
}

#pragma mark - Header Methods
- (void)dataUpdateComplete{}

#pragma mark - Service Methods
- (void)fetchData
{
    // Overridden by subclasses
}

- (void)fetchMoreData
{
    // Overridden by subclasses
}

- (void)dataFetchDidStart{
    self.isFetchRunning = YES;
}

- (void)dataFetchDidFinish{
    self.isFetchRunning = NO;
    [self dataUpdateComplete];
}

- (void)dataFetchDidSucceed
{
    self.isFetchRunning = NO;
    [SPGPropertyManager setHasDownloadedPropertyData];
}

- (void)dataFetchDidError:(NSError*)error
{
    self.isFetchRunning = NO;
}

//core data
#pragma mark -
#pragma mark Setters
- (NSFetchedResultsController *)fetchedResultsController
{
	if (_fetchedResultsController)
		return _fetchedResultsController;
    
	_fetchedResultsController = [self createFetchedResultsControllerWithPredicate:self.filter];
	return _fetchedResultsController;
}

- (NSFetchedResultsController *)searchFetchedResultsController
{
	if (_searchFetchedResultsController)
		return _searchFetchedResultsController;
    
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%@ contains[cd] %@", self.searchField, self.searchDisplayController.searchBar.text];
	_searchFetchedResultsController = [self createFetchedResultsControllerWithPredicate:predicate];
	return _searchFetchedResultsController;
}

#pragma mark -
#pragma mark NSFetchedResultsController
- (NSFetchedResultsController *)createFetchedResultsControllerWithPredicate:(NSPredicate *)predicate
{
    NSFetchedResultsController *aFetchedResultsController = nil;
    id<JLCoreDataManagedObjectDataSource> appDelegate = (id<JLCoreDataManagedObjectDataSource>)[[UIApplication sharedApplication] delegate];

    if([appDelegate conformsToProtocol:@protocol(JLCoreDataManagedObjectDataSource)])
    {
        NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
        self.fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:self.entityName inManagedObjectContext:managedObjectContext];
        if(entity) {
            self.fetchRequest.entity = entity;
            if(self.sortDescriptors.count)
            {
                [self.fetchRequest setSortDescriptors:self.sortDescriptors];
            }
            
            if(!self.batchSize)
                self.batchSize = 20;
            
            [self.fetchRequest setFetchBatchSize:self.batchSize];
            [self.fetchRequest setPredicate:predicate];
            [self.fetchRequest setReturnsDistinctResults:self.showDistinct];
            if(self.prefetchRelationship != nil)
                [self.fetchRequest setRelationshipKeyPathsForPrefetching:self.prefetchRelationship];
            
            aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:self.fetchRequest
                                                                            managedObjectContext:managedObjectContext
                                                                              sectionNameKeyPath:self.sectionKeyPath
                                                                                       cacheName:self.cacheName];
            aFetchedResultsController.delegate = self;
            [NSFetchedResultsController deleteCacheWithName:self.cacheName];
            
        }
    }
	return aFetchedResultsController;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
	[self reloadDataWithController:controller];
}
- (void)reloadData{}
- (void)reloadDataWithController:(NSFetchedResultsController*)controller{}

#pragma mark -
#pragma mark Search Delegate

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
	[self filterContentForSearchString:searchString];
	return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
	return YES;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willUnloadSearchResultsTableView:(UITableView *)tableView
{
	self.searchFetchedResultsController.delegate = nil;
	self.searchFetchedResultsController = nil;
}

- (void)filterContentForSearchString:(NSString *)search
{
	if (search && search.length) {
		[NSFetchedResultsController deleteCacheWithName:self.cacheName];
		NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", self.searchField, search];
		[self.searchFetchedResultsController.fetchRequest setPredicate:predicate];
	}
    
	NSError *error = nil;
	if (![self.searchFetchedResultsController performFetch:&error]) {
		JLDebugLog(@"Unresolved error %@, %@", error, [error userInfo]);
	}
}



@end
