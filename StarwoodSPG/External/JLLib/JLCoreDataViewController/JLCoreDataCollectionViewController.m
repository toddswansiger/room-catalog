//
//  JLCoreDataCollectionViewController.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "JLCoreDataCollectionViewController.h"

@interface JLCoreDataCollectionViewController ()
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end

@implementation JLCoreDataCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self initVars];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	if (self = [super initWithCoder:aDecoder]) {
        [self initVars];
    }
	return self;
}

- (void)initVars
{
    [super initVars];
    self.cellIdentifier = nil;
    self.showIndex = YES;
    self.canRefresh = NO;
}

#pragma mark - UIViewController LifeCycle
-(void)viewDidLoad
{
    //add tableview background image
    self.collectionView.backgroundColor = [UIColor clearColor];
	[self.collectionView setDelegate:self];
	[self.collectionView setDataSource:self];
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(self.canRefresh && !self.refreshControl)
    {
        self.refreshControl = [[UIRefreshControl alloc] init];
        [self.refreshControl addTarget:self
                                action:@selector(didPullDownToRefresh)
                      forControlEvents:UIControlEventValueChanged];
        [self.collectionView addSubview:self.refreshControl];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)dealloc
{
    self.collectionViewFlowLayout = nil;
    self.collectionView.delegate = nil;
    self.collectionView.dataSource = nil;
    self.collectionView = nil;
    [self.refreshControl removeFromSuperview];
    [self.refreshControl removeTarget:self action:@selector(didPullDownToRefresh) forControlEvents:UIControlEventAllEvents];
    self.refreshControl = nil;
}

#pragma mark - Pull To Refresh

- (IBAction)didPullDownToRefresh
{
    self.isRefreshRunning = YES;
    [self fetchData];
}

- (void)dataUpdateComplete
{
    self.isRefreshRunning = NO;
    [self.refreshControl performSelector:@selector(endRefreshing) withObject:nil afterDelay:0.0];
    [self reloadData];
}

#pragma mark - Methods

- (void)reloadData
{
    [self.collectionView reloadData];
    [super reloadData];
}

- (void)reloadDataWithController:(NSFetchedResultsController *)controller
{
	[self reloadData];
}

#pragma mark -
#pragma mark Helpers

- (NSFetchedResultsController *)fetchedResultsControllerForCollectionView:(UICollectionView *)collectionView
{
	return self.fetchedResultsController;
}

#pragma mark-
#pragma mark- FETCHRESULTCONTROLLER DELEGATE

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    //[self.collectionView reloadData];
    [super controllerDidChangeContent:controller];
}

#pragma mark -
#pragma mark UICollectionView Delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [[[self fetchedResultsControllerForCollectionView:collectionView] sections] count];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    id<NSFetchedResultsSectionInfo> sectionInfo = [[[self fetchedResultsControllerForCollectionView:collectionView] sections] objectAtIndex:section];
	return [sectionInfo numberOfObjects];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:self.cellIdentifier forIndexPath:indexPath];
    [self fetchedResultsController:[self fetchedResultsControllerForCollectionView:collectionView] configureCell:cell atIndexPath:indexPath];
	return cell;
}

// The view that is returned must be retrieved from a call to -dequeueReusableSupplementaryViewOfKind:withReuseIdentifier:forIndexPath:
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

- (void)fetchedResultsController:(NSFetchedResultsController *)controller configureCell:(UICollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
}


@end
