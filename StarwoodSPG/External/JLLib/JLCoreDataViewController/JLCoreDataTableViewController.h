//
//  JLCoreDataTableViewController.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "JLCoreDataViewController.h"

@interface JLCoreDataTableViewController : JLCoreDataViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSString *cellIdentifier;
@property (assign, nonatomic) BOOL showIndex;
@property (assign, nonatomic) BOOL canRefresh;
@property (assign, nonatomic) BOOL isRefreshRunning;

// customSortingUsed - use this property to disable auto sorting/updating in fetchedResultsController
@property (nonatomic) BOOL customSortingUsed;
- (NSFetchedResultsController *)fetchedResultsControllerForTableView:(UITableView *)tableView;
- (UITableView *)tableViewForFetchedResultsController:(NSFetchedResultsController *)controller;
- (void)fetchedResultsController:(NSFetchedResultsController *)controller configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

@end
