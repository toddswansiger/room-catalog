//
//  JLCoreDataViewController.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@protocol JLCoreDataManagedObjectDataSource <NSObject, UIApplicationDelegate>

- (NSManagedObjectContext*)managedObjectContext;

@end

@interface JLCoreDataViewController : UIViewController <NSFetchedResultsControllerDelegate>

//Properties used for configuring FetchedResultsController
@property (strong, nonatomic) NSString *entityName;
@property (strong, nonatomic) NSArray *sortDescriptors;
@property (strong, nonatomic) NSString *searchField;
@property (strong, nonatomic) NSString *sectionKeyPath;
@property (strong, nonatomic) NSPredicate *filter;
@property (strong, nonatomic) NSString *cacheName;
@property (assign, nonatomic) BOOL showDistinct;
@property (strong, nonatomic) NSArray *prefetchRelationship;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSFetchedResultsController *searchFetchedResultsController;
@property (strong, nonatomic) NSFetchRequest *fetchRequest;
@property (assign, nonatomic) BOOL sortUsingDescending;
@property (assign, nonatomic) NSInteger batchSize;
@property (nonatomic, readonly) BOOL isFetchRunning;

- (void)initVars;
- (BOOL)isVisible;
- (BOOL)isLandscape;
- (UIInterfaceOrientation)interfaceOrientation;
- (void)filterContentForSearchString:(NSString *)search;
- (void)resetFetchResultsControllerWithPredicate:(NSPredicate*)predicate;
- (void)reloadData;
- (void)reloadDataWithController:(NSFetchedResultsController*)controller;
- (NSInteger) numberOfObjectsForSection:(NSInteger)section;
//database call methods
- (void)dataUpdateComplete;

//service call methods
- (void) fetchData;
- (void) fetchMoreData;
- (void) dataFetchDidFinish;
- (void) dataFetchDidStart;
- (void) dataFetchDidSucceed;
- (void) dataFetchDidError:(NSError*)error;
@end
