//
//  JLUtility.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "JLUtility.h"

@implementation JLUtility

+ (void)showHideNetworkIndicator:(BOOL)doShow
{
    static int counter = 0;
    if(doShow)
    {
        counter++;
    }
    else
    {
        counter--;
        if(counter < 0)
        {
            counter = 0;
        }
    }
    
    if(counter > 0)
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    }
    else
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }
}

@end
