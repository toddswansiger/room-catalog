//
//  NSURL+Query.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (Query)
- (NSString*)getQueryParameter:(NSString*)parameter;
- (NSDictionary*)getQueryParameters;
@end
