//
//  NSURL+Query.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "NSURL+Query.h"

@implementation NSURL (Query)
- (NSString*)getQueryParameter:(NSString*)parameter
{
    NSString *value = nil;
    NSDictionary *dict = [self getQueryParameters];
    value = [dict objectForKey:parameter];
    return value;
}

- (NSDictionary*)getQueryParameters
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSArray *pairs = [[self query] componentsSeparatedByString:@"&"];
    for (NSString *pair in pairs) {
        NSArray *elements = [pair componentsSeparatedByString:@"="];
        if(elements.count > 1){
			NSString *key = [[elements objectAtIndex:0] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
			NSString *val = [[elements objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
			[dict setObject:val forKey:key];
		}
    }
    return dict;
}
@end
