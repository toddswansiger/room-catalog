//
//  JLDataManager.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JLUtility.h"

typedef void (^JLDataManagerFetchedResultsBlock) (NSArray *fetchedObjects, NSError *error);


@interface JLDataManager : NSObject

+ (void)setupDataManagerWithStoreName:(NSString*)storeName;
+ (NSManagedObjectContext*)managedObjectContext;
/**
 * Creates a New NSManagementObjectContent with the persistentStoreCoordinator.  Since
 * this returns a NEW object, this will need to be released when finished using.
 * This should be used to create new Contexts for working in a background thread.
 */
+ (NSManagedObjectContext*)newManagedObjectContext;

+(void)savetheDatatoStoreOnParentContextInBackground:(void (^)(NSError *))errorCallback completionhandler:(void (^)(void))completion;

/**
 * entityName parameter accepts any object that responds to NSObject's 'description' method
 * and whose description value is an NSManagedObject class name
 */
+ (id)uniqueEntityForName:(id)entityName
                withValue:(id)value
                   forKey:(NSString *)key
inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;

+ (NSArray*)entitiesForName:(id)entityName
              withPredicate:(NSPredicate*)predicate
     inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;

+ (NSArray*)entitiesForName:(id)entityName
              withPredicate:(NSPredicate*)predicate
            sortDescriptors:(NSArray*)sortDescriptors
prefetchReleationshipKeyPaths:(NSArray*)prefetchReleationshipKeyPaths
     inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;

+ (NSArray*)distinctEntitiesForName:(id)entityName
                      withPredicate:(NSPredicate*)predicate
                  propertiesToFetch:(NSArray*)propertiesToFetch
             inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;


+ (NSManagedObject*)firstEntityForName:(id)entityName
                         withPredicate:(NSPredicate*)predicate
                inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;

+ (NSInteger)entitiesCountForName:(id)entityName
                    withPredicate:(NSPredicate*)predicate
           inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;

+ (void)entitiesForName:(id)entityName
          withPredicate:(NSPredicate*)predicate
      withCallbackBlock:(JLDataManagerFetchedResultsBlock)block;

+ (void)entitiesForName:(id)entityName
          withPredicate:(NSPredicate*)predicate
        sortDescriptors:(NSArray*)sortDescriptors
prefetchReleationshipKeyPaths:(NSArray*)prefetchReleationshipKeyPaths
      withCallbackBlock:(JLDataManagerFetchedResultsBlock)block;

+ (void)asynchronousFetchRequest:(NSFetchRequest *)fetchRequest
               withCallbackBlock:(JLDataManagerFetchedResultsBlock)block;

+ (void)asynchronousFetchRequest:(NSFetchRequest *)fetchRequest
               withCallbackQueue:(dispatch_queue_t)callbackQueue
                           block:(JLDataManagerFetchedResultsBlock)callbackBlock;

+ (BOOL)deleteAllObjectsForEntity:(NSString*)entityName
                    withPredicate:(NSPredicate*)predicate
                       andContext:(NSManagedObjectContext *)managedObjectContext;

@end
