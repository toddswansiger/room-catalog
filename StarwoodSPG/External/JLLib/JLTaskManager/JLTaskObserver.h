//
//  JLTaskObserver.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JLTaskObserversCollection.h"
#import "JLTask.h"

typedef void(^JLTaskObserverBlock)(JLTaskStatus taskStatus, NSDictionary* info, NSError* error);

@class  JLTaskObserversCollection;

@interface JLTaskObserver : NSObject
@property (nonatomic, copy) JLTaskObserverBlock callback;
@property (nonatomic, copy) NSURL* taskUrl;
@property (nonatomic) JLTaskStatus taskStatus;
@property (nonatomic, strong) JLTaskObserversCollection* collection;
// taskObserverInfo - variable to be used to store anything useful for the caller
// (i.e. unique ID, other details, etc.)
@property (nonatomic, strong) id taskObserverInfo;
// isGenericTaskObserver - flag used to determine whether task listener is generic for listening
// to all tasks that match taskUrl
@property (nonatomic) BOOL isGenericTaskObserver;

- (void)updateTaskStatus:(JLTaskStatus)taskStatus withState:(NSDictionary*)state;

+ (JLTaskObserver*)taskObserverWithUrl:(NSURL*)url observerBlock:(JLTaskObserverBlock)callback;
+ (JLTaskObserver*)taskObserverWithGenericTaskUrl:(NSURL*)url observerBlock:(JLTaskObserverBlock)callback;

- (id)initWithUrl:(NSURL*)url observerBlock:(JLTaskObserverBlock)callback;
- (id)initWithGenericTaskUrl:(NSURL*)url observerBlock:(JLTaskObserverBlock)callback;

- (BOOL)taskUrlMatchesUrl:(NSURL*)url;

// Will stop observing will stop observing task state changes.
// Applies to generic task listener only
- (void)stopObserving;

- (BOOL)isTaskRunning;
- (NSString*)taskName;
@end
