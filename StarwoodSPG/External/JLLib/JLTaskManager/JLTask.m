//
//  JLTask.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/17/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "JLTask.h"
#import "JLDataManager.h"
#import "JLTaskUnit.h"
#import "JLUtility.h"

@interface JLTask ()
@property (nonatomic,strong) NSManagedObjectContext *backgroundManagedObjectContext;
@property (nonatomic) BOOL requiresCoreData;
@end

@implementation JLTask

- (id) initWithUrl:(NSURL*)url
{
    self = [super init];
    if(self)
    {
        _url = url;
        _requiresCoreData = YES;
        _state = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)main
{
    NSMutableArray *unitArray = [[NSMutableArray alloc] init];
    
    NSArray *components = [self.url.path pathComponents];
    
    for (int i = 0; i < [components count] - 1; i++) {
        Class unitClass = NSClassFromString(components[i]);
        if(unitClass)
        {
            id unit = [[unitClass alloc] init];
            if(unit)
            {
                [unitArray addObject:unit];
            }
        }
    }
    
    if(unitArray.count)
    {
        self.units = unitArray;
    }
    
    [self execute];
}

- (void)dealloc
{
    self.url = nil;
    self.units = nil;
    self.state = nil;
}

- (BOOL)isEqual:(JLTask*)object
{
    BOOL result = NO;
    if([object isKindOfClass:[JLTask class]] && [object.url isEqual:self.url])
    {
        result = YES;
    }
    return result;
}

#pragma mark - Execution Methods

- (void)saveData
{
    if(self.shouldInterrupt == NO)
    {
        [JLDataManager savetheDatatoStoreOnParentContextInBackground:nil completionhandler:nil];
    }
}

- (void) setShouldInterrupt:(BOOL)shouldInterrupt
{
    if(_shouldInterrupt != shouldInterrupt)
    {
        _shouldInterrupt = shouldInterrupt;
        
        if (_shouldInterrupt)
        {
            self.backgroundManagedObjectContext = nil;
            for (JLTaskUnit *unit in self.units)
            {
                if(self.requiresCoreData)
                {
                    unit.managedObjectContext = nil;
                }
            }
            
            self.status = JLTaskStatus_Interrupted;
        }
    }
}

- (void)execute
{
    //create the error object
    NSError *error = nil;
    
    //create a managed object context here
    self.backgroundManagedObjectContext = [JLDataManager newManagedObjectContext];
    
    self.status = JLTaskStatus_Running;
    
    @try {
        
        NSString* urlStr = (self.url.absoluteString.length ? self.url.absoluteString : @"");
        [self.state setObject:urlStr forKey:JLTaskUnitURLKey];
        
        for (JLTaskUnit *unit in self.units)
        {
            if(self.shouldInterrupt || error) {
                break;
            } else {
                if(self.requiresCoreData)
                {
                    unit.managedObjectContext = self.backgroundManagedObjectContext;
                }
                
                //logging for unit start
                self.currentUnitName = [[unit class] description];
                
                // synchronize units of same class
                @synchronized([unit class]) {
                    unit.url = self.url;
                    unit.state = self.state;
                    unit.task = self;
                    
                    if([unit shouldDoWorkWithError:&error] && !error)
                    {
                        JLSHOWNETWORKINDICATOR(YES);
                        [unit doWorkWithError:&error];
                        JLSHOWNETWORKINDICATOR(NO);
                        [self setProgress:1.0 forUnit:unit];
                    }
                }
            }
        }
        
        if(self.shouldInterrupt)
        {
            
            [self performSelectorOnMainThread:@selector(saveData) withObject:nil waitUntilDone:YES];
            
            self.status = JLTaskStatus_Finished;
        }
        else
        {
            if(error)
            {
                [self processError:error];
            }
            else
            {

                [self performSelectorOnMainThread:@selector(saveData) withObject:nil waitUntilDone:YES];
                
                self.status = JLTaskStatus_Success;
            }
            
            self.status = JLTaskStatus_Finished;
        }
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception Occurred in Task %@ - Unit %@ - Exception = %@", self.url.lastPathComponent, self.currentUnitName, exception);
        JLSHOWNETWORKINDICATOR(NO);

        [self performSelectorOnMainThread:@selector(saveData) withObject:nil waitUntilDone:NO];
        
        error = [[exception userInfo] objectForKey:JLTaskExceptionErrorKey];
        if(error)
        {
            [self processError:error];
        }
        else
        {
            error = [NSError errorWithDomain:JLTaskErrorDomain code:JLTaskErrorUnhandledCode userInfo:@{NSLocalizedFailureReasonErrorKey : [exception reason], JLTaskFailedUnitKey : self.currentUnitName ?: @""}];
            [self processError:error];
        }
        
    }@finally {
        //do nothing
    }
}

- (void)processError:(NSError*)error
{
    [self.state setObject:error forKey:JLTaskErrorKey];
    self.status = JLTaskStatus_Error;
    self.status = JLTaskStatus_Finished;
}

- (void)setProgress:(double)unitProgress forUnit:(JLTaskUnit*)unit
{
    if(self.units.count && unit)
    {
        // progress = (u1 + u2 + .. + uk-1 + 1.0*uk) / N
        // 0.5 +
        double totalProgress = (double)[self.units indexOfObject:unit];
        totalProgress += unitProgress;
        totalProgress /= (double)self.units.count;
        
        self.state[@"progress"] = @(totalProgress);
        self.status = JLTaskStatus_Progress;                                        
    }
}
@end
