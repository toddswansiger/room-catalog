//
//  JLTaskManager.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/17/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "JLTaskManager.h"
#import "JLTaskObserversCollection.h"
#import "NSObject+JLObjectObserver.h"

@interface JLTaskManager ()
@property (nonatomic, strong) NSOperationQueue* operationQueue;
@property (strong) JLTaskObserversCollection* collection;
@property (nonatomic, strong) NSDictionary* tasksList;
@end

@implementation JLTaskManager

#pragma mark - Singleton Methods
// Dictionary that holds all instances of JLTaskManager subclasses
static NSMutableDictionary *_sharedInstances = nil;

+ (void)initialize
{
	if (_sharedInstances == nil) {
		_sharedInstances = [NSMutableDictionary dictionary];
	}
}

+ (id)allocWithZone:(NSZone *)zone
{
	// Not allow allocating memory in a different zone
	return [self sharedInstance];
}

+ (id)copyWithZone:(NSZone *)zone
{
	// Not allow copying to a different zone
	return [self sharedInstance];
}

+ (instancetype)sharedInstance
{
	id sharedInstance = nil;
    
	@synchronized(self) {
		NSString *instanceClass = NSStringFromClass(self);
        
		// Looking for existing instance
		sharedInstance = [_sharedInstances objectForKey:instanceClass];
        
		// If there's no instance – create one and add it to the dictionary
		if (sharedInstance == nil) {
			sharedInstance = [[super allocWithZone:nil] init];
			[_sharedInstances setObject:sharedInstance forKey:instanceClass];
		}
	}
    
	return sharedInstance;
}

+ (void)destroyInstance
{
	[_sharedInstances removeObjectForKey:NSStringFromClass(self)];
}

#pragma mark - Task & Observer Methods

+ (void)addObserver:(JLTaskObserver*)observer
{
    @synchronized(self)
    {
        [[[self sharedInstance] collection] addTaskObserver:observer];
    }
}

+ (void)removeObserver:(JLTaskObserver*)observer
{
    @synchronized(self)
    {
        [[[self sharedInstance] collection] removeTaskObserver:observer];
    }
}

+ (JLTaskObserver*)addGenericObserverForTaskWithName:(NSString*)taskName observerBlock:(JLTaskObserverBlock)block
{
    JLTaskObserver* taskObserver = nil;
    @synchronized(self)
    {
        NSURL* taskUrl = [self taskUrlWithName:taskName];
        
        if(block)
        {
            taskObserver = [[JLTaskObserver alloc] initWithGenericTaskUrl:taskUrl observerBlock:block];
            [[[self sharedInstance] collection] addTaskObserver:taskObserver];
        }
    }
    return taskObserver;
}

+ (BOOL)isTaskInQueueWithURL:(NSURL*)taskUrl taskInfo:(id)taskInfo
{
    BOOL result = NO;
    
    JLTask* matchingTask = [self taskWithUrl:taskUrl];
    if(taskUrl && matchingTask)
    {
        if(taskInfo && [matchingTask.taskInfo isEqual:taskInfo])
        {
            result = YES;
        }
        else if(!taskInfo)
        {
            result = YES;
        }
    }
    return result;
}

+ (BOOL)isTaskInQueue:(JLTask*)task
{
    BOOL result = NO;
    NSArray* allTasks = [[[self sharedInstance] operationQueue] operations];

    if([allTasks containsObject:task])
    {
        result = YES;
    }
    return result;
}

+ (id)taskWithUrl:(NSURL*)url
{
    NSArray* allTasks = [[[self sharedInstance] operationQueue] operations];
    JLTask* task = nil;
    
    if(url)
    {
        NSArray* myTasks = [allTasks filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"url == %@", url]];

        if(myTasks.count)
        {
            task = myTasks[0];
        }
    }
    return task;
}

+ (id)taskWithUrl:(NSURL*)url taskInfo:(id)taskInfo observerBlock:(JLTaskObserverBlock)block;
{
    JLTask* task = [[JLTask alloc] initWithUrl:url];
    task.taskInfo = taskInfo;
    
    if(block)
    {
        JLTaskObserver* taskObserver = [[JLTaskObserver alloc] initWithUrl:url observerBlock:block];
        [[[self sharedInstance] collection] addTaskObserver:taskObserver];
    }
    return task;
}

+ (id)runTaskWithName:(NSString*)taskName taskInfo:(id)taskInfo observerBlock:(JLTaskObserverBlock)block
{
    JLTask* task = nil;
    NSURL* taskUrl = [self taskUrlWithName:taskName];

    if(taskUrl)
    {
        task = [self taskWithUrl:taskUrl taskInfo:taskInfo observerBlock:block];
        task.taskInfo = taskInfo;
        [self runTask:task];
    }
    return task;
}

+ (void)interruptTaskWithName:(NSString*)taskName
{
    JLTask* task = nil;
    NSURL* taskUrl = [self taskUrlWithName:taskName];
    if(taskUrl)
    {
        task = [self taskWithUrl:taskUrl];
        task.shouldInterrupt = YES;
    }
}

+ (NSURL*)taskUrlWithName:(NSString*)taskName
{
    NSURL* taskUrl = nil;
    if(taskName.length && [[[self sharedInstance] tasksList] count])
    {
        NSString* taskUrlStr = [[self sharedInstance] tasksList][taskName];
        taskUrl = [NSURL URLWithString:taskUrlStr];
    }
    return taskUrl;
}

+ (BOOL)runTask:(JLTask*)task
{
    BOOL result = NO;
    @synchronized(self)
    {
        if(![self isTaskInQueue:task])
        {
            //add a observer
            [[self sharedInstance] observeTask:task];
            [[[self sharedInstance] operationQueue] addOperation:task];
            result = YES;
        }
    }
    return result;
}

+ (NSUInteger)taskCount
{
    return [[[self sharedInstance] operationQueue] operationCount];
}


- (id)init
{
    self = [super init];
    if(self)
    {
        _collection = [JLTaskObserversCollection collection];
        _operationQueue = [[NSOperationQueue alloc] init];
        [_operationQueue setMaxConcurrentOperationCount:4];
        
        NSString* path = nil;
        
        if(self.taskListPath.length)
        {
            path = self.taskListPath;
        }
        else
        {
            if([[NSBundle mainBundle] pathForResource:@"Tasks" ofType:@"plist"])
            {
                path = [[NSBundle mainBundle] pathForResource:@"Tasks" ofType:@"plist"];
                _taskListPath = path;
            }
        }
        
        if([[NSFileManager defaultManager] fileExistsAtPath:path])
        {
            _tasksList = [NSDictionary dictionaryWithContentsOfFile:path];
        }
    }
    return self;
}

- (void)setTaskListPath:(NSString *)taskListPath
{
    if(_taskListPath != taskListPath)
    {
        _taskListPath = taskListPath;
        
        if([[NSFileManager defaultManager] fileExistsAtPath:_taskListPath])
        {
            _tasksList = [NSDictionary dictionaryWithContentsOfFile:_taskListPath];
        }
    }
}

- (void)observeTask:(JLTask*)task
{
    __weak JLTaskManager* weakSelf = self;
    [task addObserverWithKeyPaths:@[@"status"] block:^(JLObjectObserver *observer, NSString *keyPath, JLTask* aTask, NSDictionary *change, void *context) {
        
        if([keyPath isEqualToString:@"status"])
        {
            JLTaskStatus taskStatus = [[change valueForKey:NSKeyValueChangeNewKey] intValue];
            
            NSArray* taskObservers = [weakSelf.collection taskObservers];
            
            NSArray* filteredTaskObservers = [taskObservers filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"taskUrl == %@", aTask.url]];
            
            if(filteredTaskObservers.count)
            {
                for(JLTaskObserver* observer in filteredTaskObservers)
                {
                    [observer updateTaskStatus:taskStatus withState:aTask.state.copy];
                }
            }
            
            if(taskStatus == JLTaskStatus_Error)
            {
                id<JLTaskManagerErrorHandlingDelegate> appDelegate = (id<JLTaskManagerErrorHandlingDelegate>)[[UIApplication sharedApplication] delegate];
                NSError* error = nil;
                if(taskStatus == JLTaskStatus_Error)
                {
                    error = aTask.state[JLTaskErrorKey];
                }
                
                if([appDelegate respondsToSelector:@selector(handleTaskManagerError:forTaskWithURL:)])
                {
                    [[NSOperationQueue mainQueue] addOperations:@[[NSBlockOperation blockOperationWithBlock:^{

                        [appDelegate handleTaskManagerError:error forTaskWithURL:aTask.url];
                    }]] waitUntilFinished:YES];
                }
            }
            
            if(taskStatus == JLTaskStatus_Finished)
            {
                [aTask removeObserverWithKeyPaths:@[@"status"]];
            }
        }
    }];
}
@end
