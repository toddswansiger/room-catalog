//
//  JLDataManager.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "JLDataManager.h"
#import "CoreData+MagicalRecord.h"


@implementation JLDataManager

+ (void)setupDataManagerWithStoreName:(NSString*)storeName
{
    // initial setup
    [MagicalRecord setLoggingMask:MagicalRecordLogMaskOff];
    [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:storeName];
}

+ (NSManagedObjectContext*)managedObjectContext
{
    static NSManagedObjectContext* managedObjectContext = nil;
    static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		managedObjectContext = [NSManagedObjectContext MR_defaultContext];
	});

    return managedObjectContext;
}

+ (NSManagedObjectContext*)newManagedObjectContext
{
    NSManagedObjectContext* managedObjectContext = [NSManagedObjectContext MR_contextWithParent:[self managedObjectContext]];
    NSMergePolicy* mergePolicy = [[NSMergePolicy alloc] initWithMergeType:NSMergeByPropertyObjectTrumpMergePolicyType];
    [managedObjectContext setMergePolicy:mergePolicy];
	[managedObjectContext setUndoManager:nil];
    
    return managedObjectContext;
}


+ (void) ensureManagedObjectContext:(NSManagedObjectContext*)managedObjectContext
{
    if([NSThread isMainThread] == NO && [managedObjectContext isEqual:[JLDataManager managedObjectContext]]){
        JLDebugLog(@"Accessing main thread context on background thread");
    }
}

+(void)savetheDatatoStoreOnParentContextInBackground:(void (^)(NSError *))errorCallback completionhandler:(void (^)(void))completion
{
    NSManagedObjectContext * parentContext =  [JLDataManager managedObjectContext];
    
    [parentContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (success) {
            if (completion) {
                completion();
            }
        } else {
            if (errorCallback) {
                errorCallback(error);
            }
        }
    }];
}

+ (id)uniqueEntityForName:(id)entityName
                withValue:(id)value
                   forKey:(NSString *)key
   inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    
    [self ensureManagedObjectContext:managedObjectContext];
    
    entityName = [entityName description];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:entityName
                                 inManagedObjectContext:managedObjectContext];
    
    NSExpression *expKey = [NSExpression expressionForKeyPath:key];
    NSExpression *expValue = [NSExpression expressionForConstantValue:value];
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:expKey
                                                                rightExpression:expValue
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType
                                                                        options:0];
    
    request.predicate = predicate;
    NSArray *result = [managedObjectContext executeFetchRequest:request error:nil];
    
    id entity = [result lastObject];
    if (entity == nil) {
        entity = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:managedObjectContext];
        [entity setValue:value forKey:key];
    }
    return entity;
}

+ (NSArray*)entitiesForName:(id)entityName
              withPredicate:(NSPredicate*)predicate
     inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    
    [self ensureManagedObjectContext:managedObjectContext];
    
    entityName = [entityName description];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:entityName
                                 inManagedObjectContext:managedObjectContext];
    
    request.predicate = predicate;
    NSArray *result = [managedObjectContext executeFetchRequest:request error:nil];
    return result;
}


+ (NSArray*)entitiesForName:(id)entityName
              withPredicate:(NSPredicate*)predicate
            sortDescriptors:(NSArray*)sortDescriptors
prefetchReleationshipKeyPaths:(NSArray*)prefetchReleationshipKeyPaths
     inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    
    [self ensureManagedObjectContext:managedObjectContext];
    
    entityName = [entityName description];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:entityName
                                 inManagedObjectContext:managedObjectContext];
    request.sortDescriptors = sortDescriptors;
    request.predicate = predicate;
    request.relationshipKeyPathsForPrefetching = prefetchReleationshipKeyPaths;
    request.fetchBatchSize = 20;
    
    NSArray *result = [managedObjectContext executeFetchRequest:request error:nil];
    return result;
}

+ (NSArray*)distinctEntitiesForName:(id)entityName
                      withPredicate:(NSPredicate*)predicate
                  propertiesToFetch:(NSArray*)propertiesToFetch
             inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    
    [self ensureManagedObjectContext:managedObjectContext];
    
    entityName = [entityName description];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:entityName
                                 inManagedObjectContext:managedObjectContext];
    
    request.returnsDistinctResults = YES;
    request.propertiesToFetch = propertiesToFetch;
    request.predicate = predicate;
    NSArray *result = [managedObjectContext executeFetchRequest:request error:nil];
    return result;
}

+ (NSManagedObject*)firstEntityForName:(id)entityName
                         withPredicate:(NSPredicate*)predicate
                inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    
    [self ensureManagedObjectContext:managedObjectContext];
    
    NSManagedObject *managedObject = nil;
    
    NSArray *fetchedObjects = [JLDataManager entitiesForName:entityName
                                                withPredicate:predicate
                                       inManagedObjectContext:managedObjectContext];
    
    if([fetchedObjects count] > 0){
        managedObject =[fetchedObjects objectAtIndex:0];
    }
    return managedObject;
}

+ (NSInteger)entitiesCountForName:(id)entityName
                    withPredicate:(NSPredicate*)predicate
           inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    
    [self ensureManagedObjectContext:managedObjectContext];
    
    NSInteger count = 0;
    entityName = [entityName description];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:entityName
                                   inManagedObjectContext:managedObjectContext]];
    [request setPredicate:predicate];
    
    [request setIncludesSubentities:NO];
    NSError *err;
    count = [managedObjectContext countForFetchRequest:request error:&err];
    return count;
}

#pragma mark - ASYNC FetchRequests

+ (void)entitiesForName:(id)entityName
          withPredicate:(NSPredicate*)predicate
      withCallbackBlock:(JLDataManagerFetchedResultsBlock)block
{
    
    [JLDataManager entitiesForName:entityName
                      withPredicate:predicate
                    sortDescriptors:nil
      prefetchReleationshipKeyPaths:nil
                  withCallbackBlock:block];
}

+ (void)entitiesForName:(id)entityName
          withPredicate:(NSPredicate*)predicate
        sortDescriptors:(NSArray*)sortDescriptors
prefetchReleationshipKeyPaths:(NSArray*)prefetchReleationshipKeyPaths
      withCallbackBlock:(JLDataManagerFetchedResultsBlock)block
{
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityName = [entityName description];
    request.entity = [NSEntityDescription entityForName:entityName
                                 inManagedObjectContext:[JLDataManager managedObjectContext]];
    request.sortDescriptors = sortDescriptors;
    request.predicate = predicate;
    request.relationshipKeyPathsForPrefetching = prefetchReleationshipKeyPaths;
    request.fetchBatchSize = 20;
    
    [JLDataManager asynchronousFetchRequest:request
                                            withCallbackBlock:block];
}

+ (void)asynchronousFetchRequest:(NSFetchRequest *)fetchRequest
               withCallbackBlock:(JLDataManagerFetchedResultsBlock)block
{
	
    if (![NSThread isMainThread])
    {

    }
	
	[self asynchronousFetchRequest:fetchRequest
                 withCallbackQueue:dispatch_get_main_queue()
                             block:block];
}

+ (void)asynchronousFetchRequest:(NSFetchRequest *)fetchRequest
               withCallbackQueue:(dispatch_queue_t)callbackQueue
                           block:(JLDataManagerFetchedResultsBlock)callbackBlock
{
    
	NSManagedObjectContext *threadedContext = [self newManagedObjectContext];
	
    [threadedContext performBlock:^{
        NSError *error = nil;
		NSArray *array = [threadedContext executeFetchRequest:fetchRequest error:&error];
		
		NSMutableArray *objectIDs = [NSMutableArray arrayWithCapacity:[array count]];
		
		for (NSManagedObject *mo in array)
			[objectIDs addObject:[mo objectID]];
        
		dispatch_async(callbackQueue, ^{
			
			NSMutableArray *returnedObjects = [NSMutableArray arrayWithCapacity:[objectIDs count]];
            NSManagedObjectContext *mainThreadContext = [JLDataManager managedObjectContext];
            
			for (NSManagedObjectID *objectID in objectIDs)
				[returnedObjects addObject:[mainThreadContext objectWithID:objectID]];
			
			callbackBlock([NSArray arrayWithArray:returnedObjects], error);
		});
    }];
}

#pragma mark -
#pragma mark Delete Objects

// Delete all objects for a given entity
+ (BOOL)deleteAllObjectsForEntity:(id)entityName
                   withPredicate:(NSPredicate*)predicate
                      andContext:(NSManagedObjectContext *)managedObjectContext
{
    
    [self ensureManagedObjectContext:managedObjectContext];
    
	// Create fetch request
    entityName = [entityName description];
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext];
	[request setEntity:entity];
    
	// Ignore property values for maximum performance
	[request setIncludesPropertyValues:NO];
    
    if (predicate) {
        request.predicate = predicate;
    }
    
	// Execute the count request
	NSError *error = nil;
	NSArray *fetchResults = [managedObjectContext executeFetchRequest:request error:&error];
    
	// Delete the objects returned if the results weren't nil
	if (fetchResults != nil) {
		for (NSManagedObject *manObj in fetchResults) {
			[managedObjectContext deleteObject:manObj];
		}
	} else {
		return NO;
	}
    
	return YES;	
}


@end
