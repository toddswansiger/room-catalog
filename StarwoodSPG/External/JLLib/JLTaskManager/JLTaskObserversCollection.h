//
//  JLTaskObserversCollection.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>

@class JLTaskObserver;

@interface JLTaskObserversCollection : NSObject
@property (nonatomic, strong) NSMutableArray* taskObservers;

+ (instancetype)sharedInstance;
+ (JLTaskObserversCollection*)collection;
- (void)addTaskObserver:(JLTaskObserver*)taskObserver;
- (void)removeTaskObserver:(JLTaskObserver*)taskObserver;
- (void)removeAllTaskObservers;
@end
