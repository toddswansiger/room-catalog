//
//  JLTaskManager.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/17/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JLTask.h"
#import "JLTaskObserver.h"
#import "JLTaskObserversCollection.h"
#import "JLDataManager.h"

@protocol JLTaskManagerErrorHandlingDelegate <UIApplicationDelegate>
- (void)handleTaskManagerError:(id)error forTaskWithURL:(NSURL*)taskURL;
@end

@interface JLTaskManager : NSObject

// Defaults to a file called Tasks.plist in the bundle directory
// Override the getter to provide a custom Tasks List location
// Or call the setter with a new path to choose a different tasks list location
@property (nonatomic, strong) NSString* taskListPath;

+ (instancetype)sharedInstance;

// Creates a new instance of a task
+ (JLTask*)taskWithUrl:(NSURL*)url taskInfo:(id)taskInfo observerBlock:(JLTaskObserverBlock)block;

// Creates and runs a new instance of a task
+ (id)runTaskWithName:(NSString*)taskName taskInfo:(id)taskInfo observerBlock:(JLTaskObserverBlock)block;

// Adds a generic observer that will listen for any task matching task name
+ (JLTaskObserver*)addGenericObserverForTaskWithName:(NSString*)taskName observerBlock:(JLTaskObserverBlock)block;

// Notifies task that it should be interrupted
+ (void)interruptTaskWithName:(NSString*)taskName;

+ (NSURL*)taskUrlWithName:(NSString*)taskName;
+ (BOOL)runTask:(JLTask*)task;
+ (void)addObserver:(JLTaskObserver*)observer;
+ (void)removeObserver:(JLTaskObserver*)observer;
+ (BOOL)isTaskInQueueWithURL:(NSURL*)taskUrl taskInfo:(id)taskInfo;
+ (NSUInteger)taskCount;


@end
