//
//  JLTaskUnit.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/17/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>

@class  JLTask;
@interface JLTaskUnit : NSObject

@property (nonatomic, weak) JLTask* task;
@property(nonatomic, strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic, copy) NSURL *url;
@property (nonatomic, strong) NSMutableDictionary *state;
- (BOOL)shouldDoWorkWithError:(NSError**)error;
- (void)doWorkWithError:(NSError**)error;

#pragma mark - Core Data

- (id)newEntityWithName:(NSString*)entityName;

- (NSArray*)entitiesForName:(id)entityName
              withPredicate:(NSPredicate*)predicate
            sortDescriptors:(NSArray*)sortDescriptors
prefetchReleationshipKeyPaths:(NSArray*)prefetchReleationshipKeyPaths;

- (NSArray*)entitiesForName:(id)entityName
              withPredicate:(NSPredicate*)predicate;

- (NSManagedObject*)firstEntityForName:(id)entityName
                         withPredicate:(NSPredicate*)predicate;

- (NSArray*)distinctEntitiesForName:(id)entityName
                      withPredicate:(NSPredicate*)predicate
                  propertiesToFetch:(NSArray*)propertiesToFetch;

- (NSInteger)entitiesCountForName:(id)entityName
                    withPredicate:(NSPredicate*)predicate;

- (BOOL)deleteAllObjectsForEntity:(id)entityName
                   withPredicate:(NSPredicate*)predicate;

- (BOOL)deleteAllObjectsForEntity:(id)entityName;

- (void)save;

@end
