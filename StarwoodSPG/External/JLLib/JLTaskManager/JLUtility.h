//
//  JLUtility.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>

#define JLSHOWNETWORKINDICATOR(x) [JLUtility showHideNetworkIndicator:x]

#ifdef DEBUG
#define JLDebugLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#define JLDebugLog(fmt, ...)
#endif

@interface JLUtility : NSObject
+ (void)showHideNetworkIndicator:(BOOL)doShow;

@end
