//
//  JLTaskObserver.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "JLTaskObserver.h"
#import "JLTaskManager.h"
#import "JLUtility.h"

@interface JLTaskObserver ()
@property (nonatomic, weak) NSOperationQueue* currentQueue;

@end
@implementation JLTaskObserver

#pragma mark Init/Dealloc

+ (id)taskObserverWithUrl:(NSURL*)url observerBlock:(JLTaskObserverBlock)callback
{
    return [[JLTaskObserver alloc] initWithUrl:url observerBlock:callback];
}

+ (id)taskObserverWithGenericTaskUrl:(NSURL*)url observerBlock:(JLTaskObserverBlock)callback
{
    return [[JLTaskObserver alloc] initWithGenericTaskUrl:url observerBlock:callback];
}

- (id)initWithUrl:(NSURL*)url observerBlock:(JLTaskObserverBlock)callback
{
    self = [super init];
    if(self)
    {
        self.callback = callback;
        self.taskUrl = url;
        self.taskStatus = JLTaskStatus_Unknown;
        self.currentQueue = [NSOperationQueue currentQueue];
        
    }
    return self;
}

- (id)initWithGenericTaskUrl:(NSURL*)url observerBlock:(JLTaskObserverBlock)callback
{
    self = [super init];
    if(self)
    {
        self.callback = callback;
        self.taskUrl = url;
        self.taskStatus = JLTaskStatus_Unknown;
        self.isGenericTaskObserver = YES;
        self.currentQueue = [NSOperationQueue currentQueue];

        //immediately add observer for generic url
        //[JLTaskManager addObserver:self];
    }
    return self;
}

- (void)stopObserving
{
    //[JLTaskManager removeObserver:self];
    self.callback = nil;
}

- (BOOL)isTaskRunning
{
    return (self.taskStatus != JLTaskStatus_Unknown && self.taskStatus != JLTaskStatus_Finished ? YES : NO);
}

- (NSString*)taskName
{
    return [NSString stringWithFormat:@"%@", self.taskUrl.path.lastPathComponent];
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"<%@: %p> taskUrl = %@, taskStatus = %d", NSStringFromClass([self class]), self, self.taskUrl, self.taskStatus];
}

- (void)dealloc
{
    self.taskUrl = nil;
    [self.collection removeTaskObserver:self];
}

- (BOOL)taskUrlMatchesUrl:(NSURL*)url
{
    return ([url isEqual:self.taskUrl] || (self.isGenericTaskObserver && [self.taskUrl.absoluteString.lastPathComponent isEqualToString:url.absoluteString.lastPathComponent]));
}

- (BOOL)isEqualToTaskObserver:(JLTaskObserver*)otherObject
{
    BOOL result = NO;
    if([self taskUrlMatchesUrl:otherObject.taskUrl])
    {
        result = YES;
    }
    return result;
}


- (void)updateTaskStatus:(JLTaskStatus)taskStatus withState:(NSDictionary*)state
{
    self.taskStatus = taskStatus;
    NSError* error = nil;
    
    if(taskStatus == JLTaskStatus_Error)
    {
        error = state[JLTaskErrorKey];
    }
    
    // if running asynchronously, then send status on queue the start was triggered on
    if(self.currentQueue)
    {
        // must wait so that state is consistent after operation completes, and to prevent
        // connection from getting autoreleased before it sent finished state
        [self.currentQueue addOperations:@[[NSBlockOperation blockOperationWithBlock:^{
            if(self.callback)
            {
                self.callback(self.taskStatus, state, error);
            }
        }]] waitUntilFinished:YES];
    }
    else
    {
        if(self.callback)
        {
            self.callback(self.taskStatus, state, error);
        }
    }
    
    if(taskStatus == JLTaskStatus_Cancelled)
    {
        if(!self.isGenericTaskObserver)
        {
            [self.collection removeTaskObserver:self];
        }
    }
    else if(taskStatus == JLTaskStatus_Interrupted)
    {
        if(!self.isGenericTaskObserver)
        {
            [self.collection removeTaskObserver:self];
        }
    }
    else if(taskStatus == JLTaskStatus_Finished)
    {
        if(!self.isGenericTaskObserver)
        {
            self.callback = nil;
            [self.collection removeTaskObserver:self];
        }
    }
}

@end
