//
//  JLTask.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/17/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>

#define JLTaskUnitURLKey @"JLTaskUnitURLKey"
#define JLTaskErrorKey @"JLTaskErrorKey"
#define JLTaskExceptionErrorKey @"JLTaskExceptionErrorKey"
#define JLTaskErrorDomain @"JLTaskErrorDomain"
#define JLTaskFailedUnitKey @"JLTaskFailedUnitKey"

#define JLTaskErrorUnhandledCode 999999

typedef enum {
    JLTaskStatus_Unknown,
    JLTaskStatus_Started,
    JLTaskStatus_Running,
    JLTaskStatus_Solving,
    JLTaskStatus_Progress,
    JLTaskStatus_Success,
    JLTaskStatus_Cancelled,
    JLTaskStatus_Interrupted,
    JLTaskStatus_Error,
    JLTaskStatus_Finished
}JLTaskStatus;

@class JLTaskUnit;

@interface JLTask : NSOperation
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, assign) BOOL shouldInterrupt;
@property (nonatomic, strong) NSArray *units;
@property (nonatomic, strong) NSMutableDictionary *state;
@property (nonatomic, strong) NSString *currentUnitName;
@property (nonatomic, strong) NSError* errorStatus;
@property (nonatomic) JLTaskStatus status;
// taskInfo - variable to be used to store anything useful for the caller
// (i.e. unique ID, other details, etc.)
@property (nonatomic, strong) id taskInfo;

- (id)initWithUrl:(NSURL*)url;

- (BOOL)isEqual:(JLTask*)object;

- (void)setProgress:(double)unitProgress forUnit:(JLTaskUnit*)unit;

@end
