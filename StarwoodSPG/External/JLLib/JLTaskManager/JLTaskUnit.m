//
//  JLTaskUnit.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/17/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "JLTaskUnit.h"

@implementation JLTaskUnit

- (BOOL)shouldDoWorkWithError:(NSError**)error
{
    return YES;
}

- (void)doWorkWithError:(NSError**)error
{
}


#pragma mark - Core Data

- (id)newEntityWithName:(NSString*)entityName
{
    if(!self.managedObjectContext)
        return nil;
    
    return [NSEntityDescription insertNewObjectForEntityForName:entityName
                                         inManagedObjectContext:self.managedObjectContext];
}


- (NSArray*)entitiesForName:(id)entityName
              withPredicate:(NSPredicate*)predicate
            sortDescriptors:(NSArray*)sortDescriptors
prefetchReleationshipKeyPaths:(NSArray*)prefetchReleationshipKeyPaths{
    
    if(!self.managedObjectContext)
        return nil;
    
    return [JLDataManager entitiesForName:entityName
                             withPredicate:predicate
                           sortDescriptors:sortDescriptors
             prefetchReleationshipKeyPaths:prefetchReleationshipKeyPaths
                    inManagedObjectContext:self.managedObjectContext];
}

- (NSManagedObject*)firstEntityForName:(id)entityName
                         withPredicate:(NSPredicate*)predicate
{
    if(!self.managedObjectContext)
        return nil;
    
    return [JLDataManager firstEntityForName:entityName
                            withPredicate:predicate
                   inManagedObjectContext:self.managedObjectContext];
}

- (NSArray*)entitiesForName:(id)entityName
              withPredicate:(NSPredicate*)predicate{
    
    if(!self.managedObjectContext)
        return nil;
    
    return [JLDataManager entitiesForName:entityName
                            withPredicate:predicate
                   inManagedObjectContext:self.managedObjectContext];
}

- (NSArray*)distinctEntitiesForName:(id)entityName
                      withPredicate:(NSPredicate*)predicate
                  propertiesToFetch:(NSArray*)propertiesToFetch
{
    
    if(!self.managedObjectContext)
        return nil;
    
    return [JLDataManager distinctEntitiesForName:entityName
                                    withPredicate:predicate
                                propertiesToFetch:propertiesToFetch
                           inManagedObjectContext:self.managedObjectContext];
}

- (NSInteger)entitiesCountForName:(id)entityName
                    withPredicate:(NSPredicate*)predicate{
    
    if(!self.managedObjectContext)
        return 0;
    
    return [JLDataManager entitiesCountForName:entityName
                                 withPredicate:predicate
                        inManagedObjectContext:self.managedObjectContext];
}

-(BOOL)deleteAllObjectsForEntity:(id)entityName
                   withPredicate:(NSPredicate*)predicate{
    
    if(!self.managedObjectContext)
        return NO;
    
    return [JLDataManager deleteAllObjectsForEntity:entityName
                                      withPredicate:predicate
                                         andContext:self.managedObjectContext];
}

-(BOOL)deleteAllObjectsForEntity:(NSString*)entityName{
    
    if(!self.managedObjectContext)
        return NO;
    
    return [JLDataManager deleteAllObjectsForEntity:entityName
                                      withPredicate:nil
                                         andContext:self.managedObjectContext];
    
}


- (void)save
{
    if (self.task.shouldInterrupt) {
        return;
    }
    // We'll occasionally write our imports to the store and clear out the ctx and the pool
    // to improve the memory footprint. We would likely do this in larger batches in a real app.
    NSError *error = nil;
    [self.managedObjectContext save:&error];
    if (error) {
        NSLog(@"Error Occured: %@",[error description]);
    }
}

@end
