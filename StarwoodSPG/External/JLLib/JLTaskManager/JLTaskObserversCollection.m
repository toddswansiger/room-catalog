//
//  JLTaskObserversCollection.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "JLTaskObserversCollection.h"
#import "JLTaskObserver.h"

@implementation JLTaskObserversCollection

#pragma mark - Singleton Methods
// Dictionary that holds all instances of JLTaskManager subclasses
static NSMutableDictionary *_sharedInstances = nil;

+ (void)initialize
{
	if (_sharedInstances == nil) {
		_sharedInstances = [NSMutableDictionary dictionary];
	}
}

+ (instancetype)sharedInstance
{
	id sharedInstance = nil;
    
	@synchronized(self) {
		NSString *instanceClass = NSStringFromClass(self);
        
		// Looking for existing instance
		sharedInstance = [_sharedInstances objectForKey:instanceClass];
        
		// If there's no instance – create one and add it to the dictionary
		if (sharedInstance == nil) {
			sharedInstance = [[[self class] alloc] init];
			[_sharedInstances setObject:sharedInstance forKey:instanceClass];
		}
	}
    
	return sharedInstance;
}

+ (void)destroyInstance
{
	[_sharedInstances removeObjectForKey:NSStringFromClass(self)];
}

+ (JLTaskObserversCollection*)collection
{
    return [[JLTaskObserversCollection alloc] init];
}

+ (JLTaskObserversCollection*)genericTaskObserversForAllTasksWithUrls:(NSArray*)taskUrlArray Callback:(JLTaskObserverBlock)callback
{
    JLTaskObserversCollection* collection = [JLTaskObserversCollection collection];
    JLTaskObserver* taskObserver = nil;
    
    for(NSURL* url in taskUrlArray)
    {
        taskObserver = [JLTaskObserver taskObserverWithGenericTaskUrl:url observerBlock:callback];
        [collection addTaskObserver:taskObserver];
    }
    return collection;
}


- (id)init {
    
    self = [super init];
    if(self)
    {
        _taskObservers = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)addTaskObserver:(JLTaskObserver*)taskObserver
{
    @synchronized(self)
    {
        if(taskObserver)
        {
            taskObserver.collection = self;
            [self.taskObservers addObject:taskObserver];
        }
    }
}

- (void)removeTaskObserver:(JLTaskObserver*)taskObserver
{
    @synchronized(self)
    {
        if(taskObserver && [self.taskObservers containsObject:taskObserver])
        {
            taskObserver.collection = nil;
            [self.taskObservers removeObject:taskObserver];
        }
    }
}
- (void)removeAllTaskObservers
{
    @synchronized(self)
    {
        [self.taskObservers removeAllObjects];
    }
}
@end
