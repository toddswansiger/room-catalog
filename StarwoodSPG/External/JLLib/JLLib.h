//
//  JLLib.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/19/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "JLCoreDataTableViewController.h"
#import "JLCoreDataCollectionViewController.h"
#import "JLTaskManager.h"
#import "JLURLConnection.h"
#import "JLAlertView.h"
#import "JLDataManager.h"