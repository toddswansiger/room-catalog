//
//  JLAlertView.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/9/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JLAlertView;

typedef void(^JLAlertViewCompletionBlock)(NSInteger buttonIndex, id alertView);

@interface JLAlertView : UIAlertView
@property (nonatomic, weak) id owner;

#pragma mark Completion Block Versions
// Auto-Retained
+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
           completionBlock:(JLAlertViewCompletionBlock)block
         cancelButtonTitle:(NSString *)cancelButtonTitle
         otherButtonTitles:(NSString *)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;

// Auto-Retained
+ (void)showOkCancelAlertWithTitle:(NSString *)title message:(NSString *)message completionBlock:(JLAlertViewCompletionBlock)completionBlock;

- (id)initWithTitle:(NSString *)title message:(NSString *)message completionBlock:(JLAlertViewCompletionBlock)completionBlock cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;

#pragma mark - Owner Versions
// Auto-Retained
+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
           completionBlock:(JLAlertViewCompletionBlock)completionBlock
                     owner:(id)owner
         cancelButtonTitle:(NSString *)cancelButtonTitle
         otherButtonTitles:(NSString *)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;

// Auto-Retained
+ (void)showOkCancelAlertWithTitle:(NSString *)title message:(NSString *)message completionBlock:(JLAlertViewCompletionBlock)completionBlock owner:(id)owner;


- (void)addButtonWithTitle:(NSString*)title;


#pragma mark - Delegate Versions

// Auto-Retained
+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
                  delegate:(id)delegate
         cancelButtonTitle:(NSString *)cancelButtonTitle
         otherButtonTitles:(NSString *)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;

+ (void)dismissAllAlertsWithDelegate:(id)delegate;


#pragma Dismiss

- (void)dismiss;

#pragma mark - Dismiss by Title And Message

+ (JLAlertView*)alertWithTitle:(NSString*)title message:(NSString*)message;

+ (void)dismissAllAlertsWithTitle:(NSString*)title message:(NSString*)message;

#pragma mark - Dismiss by Owner

+ (void)dismissAllAlertsWithOwner:(id)owner;

@end

