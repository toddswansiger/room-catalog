//
//  JLAlertView.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/9/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "JLAlertView.h"

@interface JLAlertView () <UIAlertViewDelegate>
{
}
@property (nonatomic, copy) JLAlertViewCompletionBlock completionBlock;

@end

@implementation JLAlertView
@synthesize completionBlock = _completionBlock;

#pragma mark - Alert Collection Management

+ (NSMutableArray*)alertCollection
{
    static NSMutableArray* alertCollection = nil;
    @synchronized(alertCollection)
    {
        if(!alertCollection)
        {
            alertCollection = [[NSMutableArray alloc] init];
        }
    }
    
    return alertCollection;
}

+ (void)addAlertToCollection:(JLAlertView*)alertView
{
    @synchronized(self)
    {
        if(![[self alertCollection] containsObject:alertView])
            [[self alertCollection] addObject:alertView];
    }
}

+ (void)removeAlertFromCollection:(JLAlertView*)alertView
{
    @synchronized(self)
    {
        if([[self alertCollection] containsObject:alertView])
            [[self alertCollection] removeObject:alertView];
    }
}

#pragma mark - Dismiss by Title And Message

+ (JLAlertView*)alertWithTitle:(NSString*)title message:(NSString*)message
{
    JLAlertView* returnAlert = nil;
    
    @synchronized(self)
    {
        for(JLAlertView* alert in [self alertCollection])
        {
            if([alert.title isEqualToString:title] && [alert.message isEqualToString:message])
            {
                returnAlert = alert;
                break;
            }
        }
    }
    return returnAlert;
}

+ (void)dismissAllAlertsWithTitle:(NSString*)title message:(NSString*)message
{
    @synchronized(self)
    {
        JLAlertView* alert = [self alertWithTitle:title message:message];
        [alert dismiss];
        [self removeAlertFromCollection:alert];
    }
}

#pragma mark Completion Block Versions
+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
           completionBlock:(JLAlertViewCompletionBlock)block
         cancelButtonTitle:(NSString *)cancelButtonTitle
         otherButtonTitles:(NSString *)otherButtonTitles, ...
{
    if([self alertWithTitle:title message:message].visible)
        return;
    
    JLAlertView *alert = [[JLAlertView alloc] initWithTitle:title
                                                    message:message
                                            completionBlock:block
                                          cancelButtonTitle:cancelButtonTitle
                                          otherButtonTitles:nil];
    if (otherButtonTitles != nil) {
        [alert addButtonWithTitle:otherButtonTitles];
        va_list args;
        va_start(args, otherButtonTitles);
        NSString * title = nil;
        while((title = va_arg(args, NSString*))) {
            [alert addButtonWithTitle:title];
        }
        va_end(args);
    }
    
    [alert show];
    
    [self addAlertToCollection:alert];
}

+ (void)showOkCancelAlertWithTitle:(NSString *)title message:(NSString *)message completionBlock:(JLAlertViewCompletionBlock)completionBlock
{
    [self showAlertWithTitle:title message:message completionBlock:completionBlock cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
}

#pragma mark - Owner Versions

+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
           completionBlock:(JLAlertViewCompletionBlock)completionBlock
                     owner:(id)owner
         cancelButtonTitle:(NSString *)cancelButtonTitle
         otherButtonTitles:(NSString *)otherButtonTitles, ...
{
    if([self alertWithTitle:title message:message].visible)
        return;
    
    JLAlertView *alert = [[JLAlertView alloc] initWithTitle:title
                                                    message:message
                                            completionBlock:completionBlock
                                          cancelButtonTitle:cancelButtonTitle
                                          otherButtonTitles:nil];
    if (otherButtonTitles != nil) {
        [alert addButtonWithTitle:otherButtonTitles];
        va_list args;
        va_start(args, otherButtonTitles);
        NSString * title = nil;
        while((title = va_arg(args, NSString*))) {
            [alert addButtonWithTitle:title];
        }
        va_end(args);
    }
    alert.owner = owner;
    
    [alert show];
    
    [self addAlertToCollection:alert];
}

+ (void)showOkCancelAlertWithTitle:(NSString *)title message:(NSString *)message completionBlock:(JLAlertViewCompletionBlock)completionBlock owner:(id)owner
{
    [self showAlertWithTitle:title message:message completionBlock:completionBlock owner:owner cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
}

+ (void)dismissAllAlertsWithOwner:(id)owner
{
    @synchronized(self)
    {
        if([[self alertCollection] count]) {
            NSArray* collection = [self alertCollection];
            NSMutableArray* deletedObjs = [NSMutableArray array];
            for(JLAlertView* alert in collection)
            {
                if(alert.owner != nil && alert.owner == owner)
                {
                    [deletedObjs addObject:alert];
                }
            }
            
            if([deletedObjs count])
            {
                [[self alertCollection] removeObjectsInArray:deletedObjs];
                [deletedObjs makeObjectsPerformSelector:@selector(dismiss)];
            }
        }
    }
}

- (void)dealloc
{
    [self dismiss];
    self.owner = nil;
    self.delegate = nil;
}

- (void)addButtonWithTitle:(NSString*)title
{
    [super addButtonWithTitle:title];
}


#pragma mark - Delegate Versions


+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
                  delegate:(id)delegate
         cancelButtonTitle:(NSString *)cancelButtonTitle
         otherButtonTitles:(NSString *)otherButtonTitles, ...
{
    
    if([self alertWithTitle:title message:message].visible)
        return;
    
    JLAlertView *alertView = [[JLAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:delegate
                                              cancelButtonTitle:cancelButtonTitle
                                              otherButtonTitles:nil];
    
    [alertView show];
    
    [self addAlertToCollection:alertView];
}

+ (void)dismissAllAlertsWithDelegate:(id)delegate
{
    @synchronized(self)
    {
        if([[self alertCollection] count]) {
            NSArray* collection = [self alertCollection];
            NSMutableArray* deletedObjs = [NSMutableArray array];
            for(JLAlertView* alert in collection)
            {
                if(alert.delegate == delegate)
                {
                    [deletedObjs addObject:alert];
                }
            }
            
            if([deletedObjs count])
            {
                [[self alertCollection] removeObjectsInArray:deletedObjs];
            }
        }
    }
}


- (id)initWithTitle:(NSString *)title message:(NSString *)message completionBlock:(JLAlertViewCompletionBlock)completionBlock cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...
{
    self = [super initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
    
    if(self)
    {
        if (cancelButtonTitle) {
            [self addButtonWithTitle:cancelButtonTitle];
            self.cancelButtonIndex = [self numberOfButtons] - 1;
        }

        if (otherButtonTitles != nil) {
            [self addButtonWithTitle:otherButtonTitles];
            va_list args;
            va_start(args, otherButtonTitles);
            NSString * title = nil;
            while((title = va_arg(args, NSString*))) {
                [self addButtonWithTitle:title];
            }
            va_end(args);
        }
        self.completionBlock = completionBlock;
    }
    return self;
}

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(self.delegate && self.delegate != self && [self.delegate respondsToSelector:@selector(alertView:clickedButtonAtIndex:)])
    {
        [self.delegate alertView:self clickedButtonAtIndex:buttonIndex];
    }
    
    if(self.completionBlock)
    {
        self.completionBlock(buttonIndex, self);
        // on click, remove completionBlock
        self.completionBlock = nil;
    }
    
    //clean up
    [JLAlertView removeAlertFromCollection:self];
}

- (void)dismiss
{
    [self dismissWithClickedButtonIndex:[self cancelButtonIndex] animated:NO];
}

@end
