//
//  NSString+JSON.m
//  Thebe
//
//  Created by Jonathan Lott on 5/17/14.
//  Copyright (c) 2014 Verizon. All rights reserved.
//

#import "NSString+JSON.h"

@implementation NSString (NumericCompare)
- (NSComparisonResult)numericCompare:(NSString *)string
{
    return [self compare:string options:NSNumericSearch];
}
@end

@implementation NSString (JSON)

- (NSData*)dataValue
{
    return [self dataUsingEncoding:NSUTF8StringEncoding];
}

- (NSMutableDictionary *)jsonDictionaryValue
{
    NSError *parseError = nil;
    NSMutableDictionary *outputDictionary =
    [NSJSONSerialization JSONObjectWithData:self.dataValue
                                    options:NSJSONReadingMutableContainers
                                      error:&parseError];
    
    return outputDictionary;
}

@end