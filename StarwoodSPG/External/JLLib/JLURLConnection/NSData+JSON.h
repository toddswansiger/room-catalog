//
//  NSData+JSON.h
//  Thebe
//
//  Created by Jonathan Lott on 5/17/14.
//  Copyright (c) 2014 Verizon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (JSON)
- (NSString*)stringValue;
- (NSMutableDictionary *)jsonDictionaryValue;
@end


