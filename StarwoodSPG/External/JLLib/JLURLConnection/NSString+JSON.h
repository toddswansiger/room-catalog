//
//  NSString+JSON.h
//  Thebe
//
//  Created by Jonathan Lott on 5/17/14.
//  Copyright (c) 2014 Verizon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NumericCompare)
- (NSComparisonResult)numericCompare:(NSString *)string;
@end

@interface NSString (JSON)
- (NSData*)dataValue;
- (NSMutableDictionary *)jsonDictionaryValue;

@end
