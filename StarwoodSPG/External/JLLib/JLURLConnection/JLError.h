//
//  JLError.h
//
//
//  Created by Jonathan Lott on 2/2/10.
//  Copyright (c) 2010 A Lott Of Ideas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JLError : NSError

@end
