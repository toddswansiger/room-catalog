//
//  NSData+JSON.m
//  Thebe
//
//  Created by Jonathan Lott on 5/17/14.
//  Copyright (c) 2014 Verizon. All rights reserved.
//

#import "NSData+JSON.h"

@implementation NSData (JSON)

- (NSString*)stringValue
{
    NSString *str = [[NSString alloc] initWithData:self encoding:NSUTF8StringEncoding];
    return str;
}

- (NSMutableDictionary *)jsonDictionaryValue
{
    NSError *parseError = nil;
    NSMutableDictionary *outputDictionary =
    [NSJSONSerialization JSONObjectWithData:self
                                    options:NSJSONReadingMutableContainers
                                      error:&parseError];
    if(parseError)
        NSLog(@"error parsing json from data = %@", parseError);
    
    return outputDictionary;
}

@end
