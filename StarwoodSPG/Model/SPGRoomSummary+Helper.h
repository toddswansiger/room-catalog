//
//  SPGRoomSummary+Helper.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGRoomSummary.h"

@class SPGAttributeMapping, SPGAttributeCategory;

@interface SPGRoomSummary (Helper)

- (NSString*)propertyId;
- (SPGAttributeMapping*)selectedMappingForAttribute:(SPGAttributeCategory*)attribute;
- (BOOL)isCompleted;
- (BOOL)hasAttributeMapping:(SPGAttributeMapping*)attributeMapping;
- (BOOL)hasAttributeCategory:(SPGAttributeCategory*)attributeCategory;
- (double)percentCompleted;
- (NSDictionary*)jsonValue;
- (NSString*)transactionId;
- (NSArray*)modifiedFeatureCodes;
- (BOOL)isModified;

+ (NSArray*)partiallyCatalogedRooms;

@end
