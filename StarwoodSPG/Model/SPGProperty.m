//
//  SPGProperty.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/28/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGProperty.h"
#import "SPGAttributeCategory.h"
#import "SPGBuilding.h"
#import "SPGUser.h"

@implementation SPGProperty

@dynamic propertyId;
@dynamic propertyName;
@dynamic attributeCategories;
@dynamic buildings;
@dynamic users;

@end
