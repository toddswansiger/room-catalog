//
//  SPGAttributeCategory+Helper.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/23/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGAttributeCategory+Helper.h"
#import "NSString+StringContents.h"

static __weak SPGRoomSummary* _currentRoom;

@implementation SPGAttributeCategory (Helper)

+ (void)setCurrentRoom:(SPGRoomSummary*)currentRoom
{
    _currentRoom = currentRoom;
}

+ (NSDictionary*)propertyToKeyPathMapping
{
    NSDictionary* propertyToKeyPathMapping = @{
                                  @"categoryCode" : @"code",
                                  @"name" : @"name",
                                  @"categoryDescription" : @"description",
                                  @"categoryType" : @"categoryType",
                                  @"minSelection" : @"minSelection",
                                  @"maxSelection" : @"maxSelection",
                                  @"attributeMapping" : @"attributeMappings.attributeCategoryMapping"
                                  };
    return propertyToKeyPathMapping;
}


+ (NSString*)uniqueIDPropertyName
{
    // return the Objects property name
    // whose value is unique for this object
    return @"categoryCode";
}

#pragma mark - Helper Methods

- (SPGAttributeType)attributeType
{
    SPGAttributeMapping* anyMapping = self.attributeMapping.allObjects.firstObject;
    SPGAttributeType attributeType = SPGAttributeType_Unknown;
    
    if(self.attributeMapping.count == 2 && ([anyMapping.displayText equalToString:@"yes" ignoreCase:YES] || [anyMapping.displayText equalToString:@"no" ignoreCase:YES]))
    {
        attributeType = SPGAttributeType_YesNo;
    }
    // some multiple choice mappings have only 2 choices but they are not yes/no choices
    else if(self.attributeMapping.count >= 1)
    {
        attributeType = SPGAttributeType_MultiChoice;
    }
    return attributeType;
}

- (SPGAttributeState)attributeStateForRoom:(SPGRoomSummary*)room
{
    
    
    
    SPGAttributeState state = SPGAttributeState_Incomplete;
#if 1
    NSArray* roomFeatureCodes = [room.roomFeature.featureCodes.allObjects valueForKeyPath:@"categoryCode"];
    if([roomFeatureCodes containsObject:self.categoryCode])
    {
        state = SPGAttributeState_Locked;
    }
    // Do not extend this logic any further in this fashion. This is a special case. If it turns out we need to handle any more individual types for locking, we need to introduce another solution.
    else if ([self.categoryCode isEqualToString:@"CONNECTINGROOM"])
    {
        state = SPGAttributeState_Locked;
    }
 
#else
    //alternative logic here uses attributeMapping
    NSArray* roomFeatureCodes = [room.roomFeature.featureCodes.allObjects valueForKeyPath:@"codeName"];
    NSArray* mappingCodes = [self.attributeMapping.allObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"mappingCode IN %@", roomFeatureCodes ?: @[]]];
    
    if(mappingCodes.count)
    {
        state = SPGAttributeState_Locked;
    }
#endif
    return state;
}

- (BOOL)isLocked
{
    BOOL isLocked = NO;
    SPGAttributeState state =  [self attributeStateForRoom:_currentRoom];
    isLocked = (SPGAttributeState_Locked == state);
    return isLocked;
}
@end
