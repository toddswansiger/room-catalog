//
//  SPGSyncTransaction.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/21/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SPGSyncTransaction : NSManagedObject

@property (nonatomic, retain) NSString * transactionId;
@property (nonatomic, retain) NSString * transactionInfo;
@property (nonatomic, retain) NSNumber * transactionState;
@property (nonatomic, retain) NSNumber * transactionTimeInterval;
@property (nonatomic, retain) NSNumber * transactionType;
@property (nonatomic, retain) NSString * objectId;
@property (nonatomic, retain) NSString * objectClassName;

@end
