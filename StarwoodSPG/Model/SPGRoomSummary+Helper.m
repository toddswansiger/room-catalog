//
//  SPGRoomSummary+Helper.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGRoomSummary+Helper.h"
#import "SPGBuilding+Helper.h"
#import "SPGChangedAttributesCache.h"

@implementation SPGRoomSummary (Helper)

+ (NSDictionary*)propertyToKeyPathMapping
{
    NSDictionary* propertyToKeyPathMapping = @{@"roomNum" : @"roomNum",
                                               @"floorNum" : @"floorNum",
                                               @"buildingName" : @"buildingName",
                                               @"roomDescription" : @"roomDescription",
                                               @"featureCodes" : @"featureCodes.featureCode"};
    return propertyToKeyPathMapping;
}

+ (NSString*)uniqueIDPropertyName
{
    // return the Objects property name
    // whose value is unique for this object
    return @"roomNum";
}

- (NSString*)propertyId
{
    NSString* propertyId = nil;
    
    if(self.roomFeature.roomFeatureSummary)
    {
        propertyId = self.roomFeature.roomFeatureSummary.propertyId;
    }
    return propertyId;
}



- (NSDictionary*)jsonValue
{
    NSMutableDictionary* jsonValue = [super jsonValue].mutableCopy;
    
    // remove building name if it is unknown
    if([jsonValue[@"buildingName"] isEqualToString:kBuildingNameUnknown])
    {
        [jsonValue removeObjectForKey:@"buildingName"];
    }
    
    NSArray* featuresArrayJSON = [self.featureCodes.allObjects valueForKey:@"jsonValue"];
    if(featuresArrayJSON.count)
    {
        NSDictionary* featureCodesJSON = @{@"featureCode" : featuresArrayJSON};
        [jsonValue setObject:featureCodesJSON forKey:@"featureCodesArray"];
    }
    return jsonValue;
}

- (NSString*)transactionId
{
    NSString* transactionId = nil;
    NSString* propID = self.propertyId;
    
    if(propID.length)
    {
        transactionId = [NSString stringWithFormat:@"%@-%@", propID, self.roomNum];
    }
    return transactionId;
}

#pragma mark - Helper Methods

- (SPGAttributeMapping*)selectedMappingForAttribute:(SPGAttributeCategory*)attribute
{
    SPGAttributeMapping* mapping = nil;
    
    if(attribute.categoryCode.length)
    {
        // get feature codes that match the attribute's category code
        NSArray* featureCodes = [self.featureCodes.allObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"categoryCode == %@", attribute.categoryCode]];
        
        //adding in feature codes from roomFeature level (ie. locked feature codes)
        NSArray* lockedFeatures = [self.roomFeature.featureCodes.allObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"categoryCode == %@", attribute.categoryCode]];
        
        featureCodes = [featureCodes arrayByAddingObjectsFromArray:lockedFeatures];
        
        // extract only the code names
        NSArray* codeNames = [featureCodes valueForKey:@"codeName"];
        if(featureCodes.count)
        {
            NSString* codeName = codeNames[0];
            NSArray* mappings = [attribute.attributeMapping.allObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"mappingCode == %@", codeName]];
            mapping = mappings.count ? mappings[0] : nil;
        }
    }
    return mapping;
}

- (BOOL)isCompletedForAttribute:(SPGAttributeCategory*)attribute
{
    return ([self selectedMappingForAttribute:attribute] != nil);
}

- (BOOL)isCompleted
{
    BOOL isCompleted = NO;
    NSArray* attributeCategories = [self.building.property.attributeCategories.array valueForKey:@"categoryCode"];
    
    NSArray* featureCodes = self.featureCodes.allObjects;
    NSArray* featureCodeCategories = [featureCodes valueForKey:@"categoryCode"];

    //adding in feature codes from roomFeature level (ie. locked feature codes)
    NSArray* lockedFeatures = self.roomFeature.featureCodes.allObjects;
    lockedFeatures = [lockedFeatures filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (categoryCode IN %@)", featureCodeCategories]];

    featureCodes = [featureCodes arrayByAddingObjectsFromArray:lockedFeatures];
    
    NSArray* allFeaturesWithAttributes = [featureCodes filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(categoryCode IN %@)", attributeCategories]];
    
    if(allFeaturesWithAttributes.count >= attributeCategories.count)
    {
        isCompleted = YES;
    }
    return isCompleted;
}

- (BOOL)hasAttributeMapping:(SPGAttributeMapping*)attributeMapping
{
    BOOL hasAttribute = NO;
    NSString* attributeCodeNameCategoryPair = attributeMapping.nameCategoryPair;
    NSArray* codeNameCategoryPairs = [self.featureCodes.allObjects valueForKeyPath:@"nameCategoryPair"];
    if(attributeCodeNameCategoryPair.length && [codeNameCategoryPairs containsObject:attributeCodeNameCategoryPair])
    {
        hasAttribute = YES;
    }
    return hasAttribute;
}

- (BOOL)hasAttributeCategory:(SPGAttributeCategory*)attributeCategory
{
    
    BOOL hasAttribute = NO;

    NSArray* attributesCodeNameCategoryPairs = [attributeCategory.attributeMapping.allObjects valueForKeyPath:@"nameCategoryPair"];
    NSArray* codeNameCategoryPairs = [self.featureCodes.allObjects valueForKeyPath:@"nameCategoryPair"];
    NSArray* unionArray = [codeNameCategoryPairs filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(self IN %@)", attributesCodeNameCategoryPairs]];
    if(unionArray.count)
    {
        hasAttribute = YES;
    }
    return hasAttribute;
}

- (double)percentCompleted
{
    double percent = 0.0;
    
    NSArray* attributeCategories = [self.building.property.attributeCategories.array valueForKey:@"categoryCode"];
    
    NSArray* featureCodes = self.featureCodes.allObjects;
    NSArray* featureCodeCategories = [featureCodes valueForKey:@"categoryCode"];
    //adding in feature codes from roomFeature level (ie. locked feature codes)
    //NOTE: There may be overlap between locked features and feature codes
    NSArray* lockedFeatures = self.roomFeature.featureCodes.allObjects;
    
    lockedFeatures = [lockedFeatures filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (categoryCode IN %@)", featureCodeCategories]];
    featureCodes = [featureCodes arrayByAddingObjectsFromArray:lockedFeatures];
    
    NSArray* allFeaturesWithAttributes = [featureCodes filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(categoryCode IN %@)", attributeCategories]];
    
    allFeaturesWithAttributes = [allFeaturesWithAttributes sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"categoryCode" ascending:YES]]];
    attributeCategories = [attributeCategories sortedArrayUsingSelector:@selector(localizedStandardCompare:)];
    
    if(attributeCategories.count > 0)
    {
        percent = (double)((double)allFeaturesWithAttributes.count / (double)attributeCategories.count);
    }
    return percent;
}

- (NSArray*)modifiedFeatureCodes
{
    NSMutableDictionary* roomJSON = self.jsonValue.mutableCopy;
    NSArray *featureCodes = [roomJSON valueForKeyPath:@"featureCodesArray.featureCode"];
    NSArray *modifiedFeatureCodes = [featureCodes filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSDictionary *evaluatedObject, NSDictionary *bindings) {
        
        NSString *categoryCode = [evaluatedObject objectForKey:@"categoryCode"];
        
        BOOL dirty = [[SPGChangedAttributesCache sharedCache] isCategoryCodeDirty:categoryCode
                                                                       roomNumber:self.roomNum
                                                                       buildingID:self.building.buildingName
                                                                       propertyID:self.building.property.propertyId
                                                                      floorNumber:self.floorNum];
        
        return dirty;
        
    }]];
    return modifiedFeatureCodes;
}

- (BOOL)isModified
{
    NSManagedObjectContext* moc = [JLDataManager newManagedObjectContext];
    NSArray* rooms = @[self];
    NSArray* pendingTransactions = [JLDataManager entitiesForName:[[SPGSyncTransaction class] description] withPredicate:nil inManagedObjectContext:moc];
    NSArray* pendingTransactionIDs = [pendingTransactions valueForKeyPath:@"transactionId"];
    NSArray* partiallyCatalogedRooms = [rooms filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(transactionId IN %@) && isCompleted == NO", pendingTransactionIDs]];
    BOOL isModified = partiallyCatalogedRooms.count ? YES : NO;
    return isModified;
}

+ (NSArray*)partiallyCatalogedRooms
{
    NSManagedObjectContext* moc = [JLDataManager newManagedObjectContext];
    NSArray* rooms = [JLDataManager entitiesForName:[[SPGRoomSummary class] description] withPredicate:nil inManagedObjectContext:moc];
    NSArray* pendingTransactions = [JLDataManager entitiesForName:[[SPGSyncTransaction class] description] withPredicate:nil inManagedObjectContext:moc];
    NSArray* pendingTransactionIDs = [pendingTransactions valueForKeyPath:@"transactionId"];
    NSArray* partiallyCatalogedRooms = [rooms filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(transactionId IN %@) && isCompleted == NO", pendingTransactionIDs]];
    
    return partiallyCatalogedRooms;
}

@end
