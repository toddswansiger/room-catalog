//
//  SPGFeatureCode+Helper.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGFeatureCode.h"

@interface SPGFeatureCode (Helper)
- (NSString*)nameCategoryPair;
@end
