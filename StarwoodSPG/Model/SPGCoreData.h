//
//  SPGCoreData.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/19/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGRoomSummary+Helper.h"
#import "SPGRoomFeatureSummary+Helper.h"
#import "SPGRoomFeature+Helper.h"
#import "SPGFeatureCode+Helper.h"
#import "SPGProperty+Helper.h"
#import "SPGBuilding+Helper.h"
#import "SPGAttributeCategory+Helper.h"
#import "SPGAttributeMapping+Helper.h"
#import "SPGSyncTransaction+Helper.h"
#import "SPGUser+Helper.h"
