//
//  SPGCoreDataManagedObject.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@protocol SPGCoreDataManagedObjectMappingProtocol <NSObject>
+ (NSDictionary*)propertyToKeyPathMapping;
- (void)mapModelWithData:(NSDictionary*)data;
+ (id)existingObjectForData:(NSDictionary*)data context:(NSManagedObjectContext*)context;
@optional
+ (NSString*)uniqueIDPropertyName;
- (NSDictionary*)jsonValue;
@end

@interface SPGCoreDataManagedObject : NSManagedObject <SPGCoreDataManagedObjectMappingProtocol>
+ (NSDictionary*)propertyToKeyPathMapping;
- (void)mapModelWithData:(NSDictionary*)data;
+ (id)existingObjectForData:(NSDictionary*)data context:(NSManagedObjectContext*)context;
- (NSDictionary*)jsonValue;

@end
