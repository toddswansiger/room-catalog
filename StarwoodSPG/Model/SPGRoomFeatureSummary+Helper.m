//
//  SPGRoomFeatureSummary+Helper.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGRoomFeatureSummary+Helper.h"

@implementation SPGRoomFeatureSummary (Helper)

+ (NSDictionary*)propertyToKeyPathMapping
{
    NSDictionary* propertyToKeyPathMapping = @{@"propertyId" : @"propertyId",
                                               @"roomFeatures" : @"roomTypeFeatures.roomTypeSummary"};
    return propertyToKeyPathMapping;
}

+ (NSString*)uniqueIDPropertyName
{
    // return the Objects property name
    // whose value is unique for this object
    return @"propertyId";
}

@end
