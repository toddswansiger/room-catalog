//
//  SPGRoomFeature+Helper.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGRoomFeature+Helper.h"

@implementation SPGRoomFeature (Helper)

#pragma mark - SPGCoreDataManagedObjectMappingProtocol

+ (NSDictionary*)propertyToKeyPathMapping
{
    NSDictionary* propertyToKeyPathMapping = @{@"roomTypeCode" : @"roomTypeCode",
                                               @"roomTypeName" : @"roomTypeName",
                                               @"roomTypeDescription" : @"roomTypeDescription",
                                               @"featureCodes" : @"featureCodes.featureCode",
                                               @"roomSummaries" : @"rooms.roomSummary",
                                               @"roomFeatureSummary" : @"roomFeatureSummary"};
    return propertyToKeyPathMapping;
}

+ (NSString*)uniqueIDPropertyName
{
    // return the Objects property name
    // whose value is unique for this object
    return @"roomTypeCode";
}

@end
