//
//  SPGRoomFeatureSummary.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/19/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGRoomFeatureSummary.h"
#import "SPGRoomFeature.h"


@implementation SPGRoomFeatureSummary

@dynamic propertyId;
@dynamic roomFeatures;

@end
