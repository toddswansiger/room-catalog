//
//  SPGProperty+Helper.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/21/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGProperty+Helper.h"

@implementation SPGProperty (Helper)


/*
 NSString * propertyId;
 NSString * propertyName;
 NSSet *attributeCategories;
 NSSet *buildings;
 NSSet *user;
 */
+ (NSDictionary*)propertyToKeyPathMapping
{
    NSDictionary* propertyToKeyPathMapping = @{@"propertyId" : @"propId",
                                               @"propertyName" : @"propName",
                                               @"buildings" : @"",
                                               @"attributeCategories" : @"attributeCategories.attributeCategory"};
    return propertyToKeyPathMapping;
}

+ (NSString*)uniqueIDPropertyName
{
    // return the Objects property name
    // whose value is unique for this object
    return @"propertyId";
}

@end
