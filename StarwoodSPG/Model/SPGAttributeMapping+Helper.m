//
//  SPGAttributeMapping+Helper.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/27/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGAttributeMapping+Helper.h"

@implementation SPGAttributeMapping (Helper)

/*
  NSString * mappingCode;
  NSString * displayText;
  NSString * mappingDescription;
  NSNumber * displaySequence;
  SPGAttributeCategory *attributeCategory;
 */
+ (NSDictionary*)propertyToKeyPathMapping
{
    NSDictionary* propertyToKeyPathMapping = @{
                                               @"mappingCode" : @"code",
                                               @"displayText" : @"displayText",
                                               @"mappingDescription" : @"description",
                                               @"displaySequence" : @"displaySequence",
                                               @"attributeCategory" : @""
                                               };
    return propertyToKeyPathMapping;
}


+ (NSString*)uniqueIDPropertyName
{
    // return the Objects property name
    // whose value is unique for this object
    return @"mappingCode";
}

- (NSString*)nameCategoryPair
{
    return [NSString stringWithFormat:@"%@%@",self.attributeCategory.categoryCode, self.mappingCode];
}
@end
