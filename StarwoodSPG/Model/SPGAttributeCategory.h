//
//  SPGAttributeCategory.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/27/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SPGCoreDataManagedObject.h"

@class SPGAttributeMapping, SPGProperty;

@interface SPGAttributeCategory : SPGCoreDataManagedObject

@property (nonatomic, retain) NSString * categoryDescription;
@property (nonatomic, retain) NSString * categoryType;
@property (nonatomic, retain) NSString * categoryCode;
@property (nonatomic, retain) NSNumber * maxSelection;
@property (nonatomic, retain) NSNumber * minSelection;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) SPGProperty *property;
@property (nonatomic, retain) NSSet *attributeMapping;
@end

@interface SPGAttributeCategory (CoreDataGeneratedAccessors)

- (void)addAttributeMappingObject:(SPGAttributeMapping *)value;
- (void)removeAttributeMappingObject:(SPGAttributeMapping *)value;
- (void)addAttributeMapping:(NSSet *)values;
- (void)removeAttributeMapping:(NSSet *)values;

@end
