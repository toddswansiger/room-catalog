//
//  SPGBuilding.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/21/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGBuilding.h"
#import "SPGRoomSummary.h"


@implementation SPGBuilding

@dynamic buildingName;
@dynamic rooms;
@dynamic property;
@dynamic percentComplete;

- (float)percentComplete
{
    NSArray *rooms = [[self rooms] allObjects];
    NSArray* roomsWithCodes = [rooms filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isCompleted == YES"]];
    float progress = 0.0;
    
    if(roomsWithCodes.count && rooms.count)
    {
        progress = (CGFloat)roomsWithCodes.count / (CGFloat)rooms.count;
    }
    
    return progress;
}

@end
