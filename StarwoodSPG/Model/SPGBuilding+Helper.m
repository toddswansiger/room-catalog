//
//  SPGBuilding+Helper.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/21/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGBuilding+Helper.h"

@implementation SPGBuilding (Helper)


// TODO: Need to input correct mapping once we get API
+ (NSDictionary*)propertyToKeyPathMapping
{
    NSDictionary* propertyToKeyPathMapping = @{@"buildingName" : @"buildingName",
                                               @"rooms" : @"rooms",
                                               @"property" : @"property"};
    return propertyToKeyPathMapping;
}

+ (NSString*)uniqueIDPropertyName
{
    // return the Objects property name
    // whose value is unique for this object
    return @"buildingName";
}

- (float)catalogProgress
{
    float progress = 0.0;
    
    NSArray* rooms = self.rooms.allObjects;
    NSArray* roomsWithCodes = [rooms filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isCompleted == YES"]];
    if(roomsWithCodes.count)
    {
        progress = (float)roomsWithCodes.count / (float)rooms.count;
    }
    
    return progress;
}

- (float)roomCount
{
    float roomCount = 0.0;
    
    NSArray* rooms = self.rooms.allObjects;
    roomCount = (float)rooms.count;
    return roomCount;
}

- (float)completedRoomCount
{
    float completedRoomCount = 0.0;
    
    NSArray* rooms = self.rooms.allObjects;
    NSArray* roomsWithCodes = [rooms filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isCompleted == YES"]];
    completedRoomCount = (float)roomsWithCodes.count;
    
    return completedRoomCount;
}
@end
