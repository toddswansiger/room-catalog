//
//  SPGAttributeCategory+Helper.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/23/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGAttributeCategory.h"

typedef enum
{
    SPGAttributeType_Unknown = 0,
    SPGAttributeType_YesNo,
    SPGAttributeType_MultiChoice
} SPGAttributeType;


typedef enum
{
    SPGAttributeState_Incomplete = 0,
    SPGAttributeState_Completed = 1,
    SPGAttributeState_Locked = 2
} SPGAttributeState;

@interface SPGAttributeCategory (Helper)

+ (void)setCurrentRoom:(SPGRoomSummary*)currentRoom;

+ (NSDictionary*)propertyToKeyPathMapping;
+ (NSString*)uniqueIDPropertyName;

- (SPGAttributeType)attributeType;
- (BOOL)isLocked;

@end
