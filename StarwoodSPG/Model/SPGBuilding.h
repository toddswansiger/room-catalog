//
//  SPGBuilding.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/21/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SPGCoreDataManagedObject.h"

@class SPGRoomSummary, SPGProperty;

@interface SPGBuilding : SPGCoreDataManagedObject

@property (nonatomic, retain) NSString * buildingName;
@property (nonatomic, retain) NSSet *rooms;
@property (nonatomic, retain) SPGProperty *property;
@property (nonatomic, assign, readonly) float percentComplete;

@end

@interface SPGBuilding (CoreDataGeneratedAccessors)

- (void)addRoomsObject:(SPGRoomSummary *)value;
- (void)removeRoomsObject:(SPGRoomSummary *)value;
- (void)addRooms:(NSSet *)values;
- (void)removeRooms:(NSSet *)values;

@end
