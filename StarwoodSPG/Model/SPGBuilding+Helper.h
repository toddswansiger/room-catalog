//
//  SPGBuilding+Helper.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/21/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGBuilding.h"

#define kBuildingNameUnknown @"UNKNOWN"
#define kBuildingDisplayNameDefault @"Main Building"

@interface SPGBuilding (Helper)
- (float)catalogProgress;
- (float)roomCount;
- (float)completedRoomCount;
@end
