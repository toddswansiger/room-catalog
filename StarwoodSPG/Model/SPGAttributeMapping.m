//
//  SPGAttributeMapping.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/27/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGAttributeMapping.h"
#import "SPGAttributeCategory.h"


@implementation SPGAttributeMapping

@dynamic mappingCode;
@dynamic displayText;
@dynamic mappingDescription;
@dynamic displaySequence;
@dynamic attributeCategory;

@end
