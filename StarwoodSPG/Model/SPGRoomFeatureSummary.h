//
//  SPGRoomFeatureSummary.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/19/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGCoreDataManagedObject.h"

@class SPGRoomFeature;

@interface SPGRoomFeatureSummary : SPGCoreDataManagedObject

@property (nonatomic, retain) NSString * propertyId;
@property (nonatomic, retain) NSSet *roomFeatures;
@end

@interface SPGRoomFeatureSummary (CoreDataGeneratedAccessors)

- (void)addRoomFeaturesObject:(SPGRoomFeature *)value;
- (void)removeRoomFeaturesObject:(SPGRoomFeature *)value;
- (void)addRoomFeatures:(NSSet *)values;
- (void)removeRoomFeatures:(NSSet *)values;

@end
