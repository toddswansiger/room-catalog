//
//  SPGRoomSummary.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/19/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGRoomSummary.h"
#import "SPGFeatureCode.h"
#import "SPGRoomFeature.h"


@implementation SPGRoomSummary

@dynamic roomNum;
@dynamic floorNum;
@dynamic buildingName;
@dynamic roomDescription;
@dynamic isFlagged;
@dynamic featureCodes;
@dynamic roomFeature;
@dynamic building;

- (void)awakeFromFetch
{
    [super awakeFromFetch];
}

@end
