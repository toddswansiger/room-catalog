//
//  SPGRoomSummary.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/19/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGCoreDataManagedObject.h"

@class SPGFeatureCode, SPGRoomFeature, SPGBuilding;

@interface SPGRoomSummary : SPGCoreDataManagedObject

@property (nonatomic, retain) NSString * roomNum;
@property (nonatomic, retain) NSString * floorNum;
@property (nonatomic, retain) NSString * buildingName;
@property (nonatomic, retain) NSString * roomDescription;
@property (nonatomic, retain) NSNumber * isFlagged;
@property (nonatomic, retain) NSSet *featureCodes;
@property (nonatomic, retain) SPGRoomFeature *roomFeature;
@property (nonatomic, retain) SPGBuilding *building;

@end

@interface SPGRoomSummary (CoreDataGeneratedAccessors)

- (void)addFeatureCodesObject:(SPGFeatureCode *)value;
- (void)removeFeatureCodesObject:(SPGFeatureCode *)value;
- (void)addFeatureCodes:(NSSet *)values;
- (void)removeFeatureCodes:(NSSet *)values;

@end
