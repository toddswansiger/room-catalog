//
//  SPGAttributeCategory.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/27/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGAttributeCategory.h"
#import "SPGAttributeMapping.h"
#import "SPGProperty.h"


@implementation SPGAttributeCategory

@dynamic categoryDescription;
@dynamic categoryType;
@dynamic categoryCode;
@dynamic maxSelection;
@dynamic minSelection;
@dynamic name;
@dynamic property;
@dynamic attributeMapping;

@end
