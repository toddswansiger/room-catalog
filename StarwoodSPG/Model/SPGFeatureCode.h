//
//  SPGFeatureCode.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/19/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGCoreDataManagedObject.h"

@class SPGRoomFeature, SPGRoomSummary;

@interface SPGFeatureCode : SPGCoreDataManagedObject

@property (nonatomic, retain) NSString * codeName;
@property (nonatomic, retain) NSString * categoryCode;
@property (nonatomic, retain) NSSet *roomFeatures;
@property (nonatomic, retain) NSSet *roomSummaries;
@end

@interface SPGFeatureCode (CoreDataGeneratedAccessors)

- (void)addRoomFeaturesObject:(SPGRoomFeature *)value;
- (void)removeRoomFeaturesObject:(SPGRoomFeature *)value;
- (void)addRoomFeatures:(NSSet *)values;
- (void)removeRoomFeatures:(NSSet *)values;

- (void)addRoomSummariesObject:(SPGRoomSummary *)value;
- (void)removeRoomSummariesObject:(SPGRoomSummary *)value;
- (void)addRoomSummaries:(NSSet *)values;
- (void)removeRoomSummaries:(NSSet *)values;

@end