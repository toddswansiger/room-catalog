//
//  SPGAttributeMapping.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/27/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SPGCoreDataManagedObject.h"

@class SPGAttributeCategory;

@interface SPGAttributeMapping : SPGCoreDataManagedObject

@property (nonatomic, retain) NSString * mappingCode;
@property (nonatomic, retain) NSString * displayText;
@property (nonatomic, retain) NSString * mappingDescription;
@property (nonatomic, retain) NSNumber * displaySequence;
@property (nonatomic, retain) SPGAttributeCategory *attributeCategory;

@end
