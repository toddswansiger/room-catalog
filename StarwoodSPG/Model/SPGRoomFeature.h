//
//  SPGRoomFeature.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/19/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGCoreDataManagedObject.h"

@class SPGFeatureCode, SPGRoomFeatureSummary, SPGRoomSummary;

@interface SPGRoomFeature : SPGCoreDataManagedObject

@property (nonatomic, retain) NSString * roomTypeCode;
@property (nonatomic, retain) NSString * roomTypeName;
@property (nonatomic, retain) NSString * roomTypeDescription;
@property (nonatomic, retain) NSSet *featureCodes;
@property (nonatomic, retain) NSSet *roomSummaries;
@property (nonatomic, retain) SPGRoomFeatureSummary *roomFeatureSummary;
@end

@interface SPGRoomFeature (CoreDataGeneratedAccessors)

- (void)addFeatureCodesObject:(SPGFeatureCode *)value;
- (void)removeFeatureCodesObject:(SPGFeatureCode *)value;
- (void)addFeatureCodes:(NSSet *)values;
- (void)removeFeatureCodes:(NSSet *)values;

- (void)addRoomSummariesObject:(SPGRoomSummary *)value;
- (void)removeRoomSummariesObject:(SPGRoomSummary *)value;
- (void)addRoomSummaries:(NSSet *)values;
- (void)removeRoomSummaries:(NSSet *)values;

@end
