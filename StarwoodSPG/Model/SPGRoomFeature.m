//
//  SPGRoomFeature.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/19/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGRoomFeature.h"
#import "SPGFeatureCode.h"
#import "SPGRoomFeatureSummary.h"
#import "SPGRoomSummary.h"


@implementation SPGRoomFeature

@dynamic roomTypeCode;
@dynamic roomTypeName;
@dynamic roomTypeDescription;
@dynamic featureCodes;
@dynamic roomSummaries;
@dynamic roomFeatureSummary;

@end
