//
//  SPGFeatureCode+Helper.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGFeatureCode+Helper.h"

@implementation SPGFeatureCode (Helper)

+ (NSDictionary*)propertyToKeyPathMapping
{
    NSDictionary* propertyToKeyPathMapping = @{@"codeName" : @"featureCode",
                                               @"categoryCode" : @"categoryCode",
                                               @"roomSummaries" : @"roomSummary",
                                               @"roomFeatures" : @"roomFeature"};
    return propertyToKeyPathMapping;
}

+ (NSString*)uniqueIDPropertyName
{
    // return the Objects property name
    // whose value is unique for this object
    return @"categoryCode";
}

- (NSString*)nameCategoryPair
{
    return [NSString stringWithFormat:@"%@%@",self.categoryCode, self.codeName];
}
@end
