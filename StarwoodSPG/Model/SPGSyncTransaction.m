//
//  SPGSyncTransaction.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/21/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGSyncTransaction.h"


@implementation SPGSyncTransaction

@dynamic transactionId;
@dynamic transactionInfo;
@dynamic transactionState;
@dynamic transactionTimeInterval;
@dynamic transactionType;
@dynamic objectId;
@dynamic objectClassName;

@end
