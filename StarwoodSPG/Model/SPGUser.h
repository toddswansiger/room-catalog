//
//  SPGUser.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/28/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SPGCoreDataManagedObject.h"

@class SPGProperty;

@interface SPGUser : SPGCoreDataManagedObject

@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * userID;
@property (nonatomic, retain) NSString * token;
@property (nonatomic, retain) NSData * password;
@property (nonatomic, retain) NSSet *properties;
@end

@interface SPGUser (CoreDataGeneratedAccessors)

- (void)addPropertiesObject:(SPGProperty *)value;
- (void)removePropertiesObject:(SPGProperty *)value;
- (void)addProperties:(NSSet *)values;
- (void)removeProperties:(NSSet *)values;

@end
