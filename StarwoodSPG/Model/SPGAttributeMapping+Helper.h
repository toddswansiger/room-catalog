//
//  SPGAttributeMapping+Helper.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/27/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGAttributeMapping.h"



@interface SPGAttributeMapping (Helper)
- (NSString*)nameCategoryPair;
@end
