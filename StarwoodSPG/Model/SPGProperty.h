//
//  SPGProperty.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/28/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SPGCoreDataManagedObject.h"

@class SPGAttributeCategory, SPGBuilding, SPGUser;

@interface SPGProperty : SPGCoreDataManagedObject

@property (nonatomic, retain) NSString * propertyId;
@property (nonatomic, retain) NSString * propertyName;
@property (nonatomic, retain) NSOrderedSet *attributeCategories;
@property (nonatomic, retain) NSSet *buildings;
@property (nonatomic, retain) NSSet *users;
@end

@interface SPGProperty (CoreDataGeneratedAccessors)

- (void)addAttributeCategoriesObject:(SPGAttributeCategory *)value;
- (void)removeAttributeCategoriesObject:(SPGAttributeCategory *)value;
- (void)addAttributeCategories:(NSSet *)values;
- (void)removeAttributeCategories:(NSSet *)values;

- (void)addBuildingsObject:(SPGBuilding *)value;
- (void)removeBuildingsObject:(SPGBuilding *)value;
- (void)addBuildings:(NSSet *)values;
- (void)removeBuildings:(NSSet *)values;

- (void)addUsersObject:(SPGUser *)value;
- (void)removeUsersObject:(SPGUser *)value;
- (void)addUsers:(NSSet *)values;
- (void)removeUsers:(NSSet *)values;

@end
