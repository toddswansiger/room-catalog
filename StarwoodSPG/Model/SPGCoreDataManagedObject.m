//
//  SPGCoreDataManagedObject.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/18/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGCoreDataManagedObject.h"

@implementation NSObject (HandleNulls)

- (id)stripNulls
{
    if([self isKindOfClass:[NSDictionary class]])
    {
        NSDictionary* myDict = (NSDictionary*)self;
        NSMutableDictionary* dict = [NSMutableDictionary dictionary];
        for(NSString* key in myDict.allKeys)
        {
            id object = myDict[key];
            if(![object isKindOfClass:[NSNull class]])
            {
                [dict setObject:[object stripNulls] forKey:key];
            }
        }
        return dict;
    }
    //JLo: nulls in NSArray cannot be stripped because it will mess up the
    // order, but we need to keep traversing to next NSArray or NSDictionary object
     else if([self isKindOfClass:[NSArray class]])
     {
         NSArray* myArray = (NSArray*)self;
         NSMutableArray* array = [NSMutableArray array];
         for(id object in myArray)
         {
             if([object isKindOfClass:[NSArray class]] || [object isKindOfClass:[NSDictionary class]])
             {
                 [array addObject:[object stripNulls]];
             }
             else
             {
                 [array addObject:object];
             }
         }
         return array;
     }
    return self;
}
@end

@implementation SPGCoreDataManagedObject

- (NSDictionary*)mapping
{
    NSDictionary* propertyToKeyPathMapping = self.class.propertyToKeyPathMapping;
    
    NSMutableDictionary* mappingDict = [NSMutableDictionary dictionary];
    NSDictionary* attributes = self.entity.attributesByName;
    NSDictionary* relationships = self.entity.relationshipsByName;
    
    for(id key in attributes.allKeys)
    {
        id obj = [attributes objectForKey:key];
        NSString* mappingKey = [propertyToKeyPathMapping objectForKey:key];
        if(mappingKey.length)
        {
            NSDictionary* mappingGroup = @{@"mapping" : key, @"attributes" : obj};
            mappingDict[mappingKey] = mappingGroup;
        }
    }
    
    for(id key in relationships.allKeys)
    {
        id obj = [relationships objectForKey:key];
        NSString* mappingKey = [propertyToKeyPathMapping objectForKey:key];
        if(mappingKey.length)
        {
            NSDictionary* mappingGroup = @{@"mapping" : key, @"relationships" : obj};
            mappingDict[mappingKey] = mappingGroup;
        }
    }
    
    return mappingDict;
}

#pragma mark - SPGCoreDataManagedObjectMappingProtocol

+ (NSDictionary*)propertyToKeyPathMapping
{
    NSDictionary* propertyToKeyPathMapping = @{};
    return propertyToKeyPathMapping;
}

+ (id)existingObjectForData:(NSDictionary*)data context:(NSManagedObjectContext*)context
{
    id existingObject = nil;
    @try {
        
        if([self respondsToSelector:@selector(uniqueIDPropertyName)] && [[self uniqueIDPropertyName] length])
        {
            NSString* propertyName = [self uniqueIDPropertyName];
            NSString* mappingKey = self.propertyToKeyPathMapping[propertyName];
            id dataMapValue = data[mappingKey];
            //convert nsnumber to string
            if([dataMapValue isKindOfClass:[NSNumber class]])
            {
                dataMapValue = [(NSNumber*)dataMapValue stringValue];
            }
            
            if([dataMapValue isKindOfClass:[NSString class]] && [(NSString*)dataMapValue length] && propertyName.length)
            {
                // must perform filter predicate on resulting array because I want to support
                // the possibility of propertyName NOT being a NSManagedObject property
                NSArray* matchingObjects = [JLDataManager entitiesForName:[self description] withPredicate:nil inManagedObjectContext:context];
                
                NSArray* filteredObjects = [matchingObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K == %@", propertyName, dataMapValue]];
                
                if(filteredObjects.count)
                {
                    existingObject = filteredObjects[0];
                }
            }
            else if(![dataMapValue isKindOfClass:[NSString class]])
            {
                NSLog(@"dataMapValue (%@) is not a string class, class = %@", dataMapValue, [[dataMapValue class] description]);
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"existingObjectForData failed with ERROR - %@", exception);
        @throw exception;
    }
    
    return existingObject;
}

- (void)mapModelWithData:(NSDictionary*)data
{
    
    @try {
    
        NSDictionary* mappingDict = [self mapping];
        
        if(mappingDict.count)
        {
            //handle nulls
            NSDictionary* strippedData = [data stripNulls];
            
            for(NSString* key in mappingDict.allKeys)
            {
                id obj = [strippedData valueForKeyPath:key];
                NSDictionary* mapping = mappingDict[key];
                
                // skip nil objects. These are objects that are not found in dictionary.
                if(mapping.count && obj != nil)
                {
                    NSAttributeDescription* attributes = mapping[@"attributes"];
                    NSRelationshipDescription* relationships = mapping[@"relationships"];
                    
                    NSString* mappingKey = mapping[@"mapping"];
                    if(mappingKey.length) {
                        
                        if(attributes)
                        {
                            switch (attributes.attributeType) {
                                case NSInteger16AttributeType:
                                case NSInteger32AttributeType:
                                case NSInteger64AttributeType:
                                    [self setValue:[NSNumber numberWithInteger:[(NSString*)obj integerValue]] forKey:mappingKey];
                                    break;
                                case NSDecimalAttributeType:
                                case NSDoubleAttributeType:
                                case NSFloatAttributeType:
                                    [self setValue:[NSNumber numberWithDouble:[(NSString*)obj doubleValue]] forKey:mappingKey];
                                    break;
                                case NSStringAttributeType:
                                    [self setValue:[NSString stringWithFormat:@"%@", obj] forKey:mappingKey];
                                    break;
                                case NSBooleanAttributeType:
                                    [self setValue:[NSNumber numberWithBool:[(NSString*)obj boolValue]] forKey:mappingKey];
                                    break;
                                case  NSDateAttributeType:
                                case NSBinaryDataAttributeType:
                                case NSUndefinedAttributeType:
                                default:
                                    NSLog(@"skipping key with attribute %@", attributes);
                                    break;
                            }
                        }
                        else if(relationships)
                        {
                            if(relationships.destinationEntity.managedObjectClassName.length)
                            {
                                NSString* entityName = relationships.destinationEntity.managedObjectClassName;
                                Class aClass = NSClassFromString(entityName);
                                
                                if([aClass instancesRespondToSelector:@selector(mapModelWithData:)])
                                {
                                    if([obj isKindOfClass:[NSArray class]])
                                    {
                                        NSArray* array = (NSArray*)obj;
                                        if(relationships.isToMany)
                                        {
                                            id relationshipCollection = nil;
                                            
                                            // find existing set of relationship objects
                                            id existingObjects = [self valueForKey:mappingKey];
                                            NSArray* existingObjectsArray = nil;
                                            
                                            if([existingObjects isKindOfClass:[NSSet class]] && [(NSSet*)existingObjects count])
                                            {
                                                //relationshipCollection = [(NSSet*)existingObjects mutableCopy];
                                                existingObjectsArray = [(NSSet*)existingObjects allObjects];
                                            }
                                            else if([existingObjects isKindOfClass:[NSOrderedSet class]] && [(NSOrderedSet*)existingObjects count])
                                            {
                                                //relationshipCollection = [(NSOrderedSet*)existingObjects mutableCopy];
                                                existingObjectsArray = [(NSOrderedSet*)existingObjects array];
                                            }
                                            
                                            if(!relationshipCollection)
                                            {
                                                relationshipCollection = relationships.isOrdered ? [NSMutableOrderedSet orderedSet] : [NSMutableSet set];
                                            }
                                            
                                            for(NSDictionary* dict in array)
                                            {
                                                id<SPGCoreDataManagedObjectMappingProtocol> newObject = nil;
                                                BOOL existingObjectFound = NO;

                                                if(existingObjectsArray.count)
                                                {
                                                    if([aClass respondsToSelector:@selector(propertyToKeyPathMapping)] && [aClass respondsToSelector:@selector(uniqueIDPropertyName)] && [[aClass uniqueIDPropertyName] length])
                                                    {
                                                        NSString* propertyName = [aClass uniqueIDPropertyName];
                                                        NSString* mappingKey = aClass.propertyToKeyPathMapping[propertyName];
                                                        id dataMapValue = dict[mappingKey];
                                                        //convert nsnumber to string
                                                        if([dataMapValue isKindOfClass:[NSNumber class]])
                                                        {
                                                            dataMapValue = [(NSNumber*)dataMapValue stringValue];
                                                        }
                                                        
                                                        if([dataMapValue isKindOfClass:[NSString class]] && [(NSString*)dataMapValue length] && propertyName.length)
                                                        {
                                                            NSArray* filteredObjects = [existingObjectsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K == %@", propertyName, dataMapValue]];
                                                            
                                                            if(filteredObjects.count)
                                                            {
                                                                newObject = filteredObjects[0];
                                                                existingObjectFound = YES;
                                                            }
                                                        
                                                        }
                                                    }
                                                }
                                                
                                                if(!newObject)
                                                {
                                                    newObject = [NSEntityDescription insertNewObjectForEntityForName:entityName
                                                                                              inManagedObjectContext:self.managedObjectContext];
                                                }
                                                
                                                
                                                [newObject mapModelWithData:dict];
                                                [relationshipCollection addObject:newObject];
                                            }
                                            
                                            @try {
                                                [self setValue:relationshipCollection forKey:mappingKey];
                                            } @catch (NSException *exception) {
                                                NSLog(@"ERROR - Format Exception - %@", exception);
                                                @throw exception;
                                            }
                                        }
                                        else
                                        {
                                            NSLog(@"array object found for entity (%@) that is not a to-Many entity", entityName);
                                        }
                                    }
                                    else if([obj isKindOfClass:[NSDictionary class]])
                                    {
                                        NSDictionary* dict = (NSDictionary*)obj;
                                        
                                        if(!relationships.isToMany)
                                        {
                                            id<SPGCoreDataManagedObjectMappingProtocol> newObject = nil;
                                            
                                            // find existing relationship object
                                            newObject = [self valueForKey:mappingKey];

                                            if(!newObject)
                                            {
                                                newObject = [NSEntityDescription insertNewObjectForEntityForName:entityName
                                                                                          inManagedObjectContext:self.managedObjectContext];
                                            }

                                            [newObject mapModelWithData:dict];
                                            @try {
                                                [self setValue:newObject forKey:mappingKey];
                                            } @catch (NSException *exception) {
                                                NSLog(@"ERROR - Format Exception - %@", exception);
                                                @throw exception;
                                            }
                                        }
                                        else
                                        {
                                            NSLog(@"dictionary object found for entity (%@) that is a to-Many entity - object = %@", entityName, dict);
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"mapModelWithData failed with ERROR - %@", exception);
        @throw exception;
    }
}

- (NSDictionary*)jsonValue
{
    NSMutableDictionary* jsonValue = [NSMutableDictionary dictionary];
    
    NSDictionary* propKeyMapping = [self.class propertyToKeyPathMapping];
    
    for(NSString* property in propKeyMapping.allKeys)
    {
        if(property.length)
        {
            NSString* jsonKeyPath = propKeyMapping[property];
            id obj = [self valueForKeyPath:property];
            
            //TODO: Need more obj validation to prevent unhandled types of objects
            if([obj isKindOfClass:[NSString class]] || [obj isKindOfClass:[NSNumber class]])
            {
                [jsonValue setObject:obj forKey:jsonKeyPath];
            }
            else
            {
#if 0 // this can cause recursive loop for inverse relationships
                if(jsonKeyPath.length && [obj isKindOfClass:[SPGCoreDataManagedObject class]])
                {
                    NSLog(@"[%@] obj is of class %@", jsonKeyPath, [obj class].description);
                    [jsonValue setObject:[(SPGCoreDataManagedObject*)obj jsonValue] forKey:jsonKeyPath];
                }
#endif
            }
        }
    }
    return jsonValue;
}

@end
