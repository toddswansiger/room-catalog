//
//  SPGUser+Helper.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/28/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGUser+Helper.h"

@implementation SPGUser (Helper)

/*
  NSString * firstName;
  NSString * lastName;
  NSString * userID;
  NSString * token;
  NSData * password;
  NSSet *properties;
 */
+ (NSDictionary*)propertyToKeyPathMapping
{
    NSDictionary* propertyToKeyPathMapping = @{
                                               @"firstName" : @"profileResponse.profile.firstName",
                                               @"lastName" : @"profileResponse.profile.lastName",
                                               @"userID" : @"",
                                               @"token" : @"",
                                               @"password" : @"",
                                               @"properties" : @"profileResponse.profile.properties",
                                               };
    return propertyToKeyPathMapping;
}


+ (NSString*)uniqueIDPropertyName
{
    // return the Objects property name
    // whose value is unique for this object
    return @"token";
}

@end
