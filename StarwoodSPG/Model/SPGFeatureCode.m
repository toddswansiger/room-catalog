//
//  SPGFeatureCode.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/19/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGFeatureCode.h"
#import "SPGRoomFeature.h"
#import "SPGRoomSummary.h"


@implementation SPGFeatureCode

@dynamic codeName;
@dynamic categoryCode;
@dynamic roomFeatures;
@dynamic roomSummaries;

@end
