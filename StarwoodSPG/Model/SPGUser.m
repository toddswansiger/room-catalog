//
//  SPGUser.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/28/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGUser.h"
#import "SPGProperty.h"


@implementation SPGUser

@dynamic firstName;
@dynamic lastName;
@dynamic userID;
@dynamic token;
@dynamic password;
@dynamic properties;

@end
