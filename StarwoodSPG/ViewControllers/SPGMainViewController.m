//
//  SPGMainViewController.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/15/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGMainViewController.h"
#import "SPGUserMenuViewController.h"
#import "UIViewController+SPGAdditions.h"
#import "SPGVersionAlertEmbededViewController.h"
#import "UIAlertView+Blocks.h"
#import "MBProgressHUD.h"
#import "SPGAppDelegate.h"

@interface SPGMainViewController ()

@property (nonatomic, weak) SPGUserMenuViewController* menuViewController;
@property (nonatomic, assign) BOOL didPerformInitialSegue;

@end

@implementation SPGMainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [self setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    self.drawerSlideGestureEnabled = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(revealToggle) name:kMenuNavigationControllerMenuButtonSelectedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLogout) name:SPGLogoutNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLogin) name:SPGLoginNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAPIKeyDeprecatedNotification) name:SPGAPIKeyDeprecatedNotification object:nil];
    [self setDidPerformInitialSegue:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (![self didPerformInitialSegue])
    {
        [self performSegueWithIdentifier:@"CenterViewController" sender:nil];
        [self performSegueWithIdentifier:@"RightViewController" sender:nil];
        [self setDidPerformInitialSegue:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"CenterViewController"])
    {
        // Might not be what we intend as this destination VC as this is the nav controller
        self.centerViewController = segue.destinationViewController;
        self.rootNavigationController = segue.destinationViewController;
    }
    if([segue.identifier isEqualToString:@"RightViewController"])
    {
        self.menuViewController = (SPGUserMenuViewController*)segue.destinationViewController;
        self.rightDrawerViewController = segue.destinationViewController;
    }
}

#pragma mark - Drawer Toggle

- (void)revealToggle
{
    if (MMDrawerSideLeft == self.openSide)
    {
        [self closeDrawerAnimated:YES
                       completion:nil];
    } else
    {
        [self openDrawerSide:MMDrawerSideRight
                    animated:YES
                  completion:nil];
        [[SPGAnalyticsManager sharedManager] logEvent:@"User_Profile_Icon_Clicked"];
    }
}

#pragma mark - App Updates

- (void)handleAPIKeyDeprecatedNotification
{
    RIButtonItem *cancelItem = [RIButtonItem itemWithLabel:@"OK"];
    RIButtonItem *updateItem = [RIButtonItem itemWithLabel:@"Update" action:^{
        
        [[UIViewController appDelegate] handleApplicationUpdateRequest];
        
    }];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Update App"
                                                    message:@"To continue using this application, you will need to download and install the latest version."
                                           cancelButtonItem:cancelItem
                                           otherButtonItems:updateItem, nil];
    [alert show];
    
}

#pragma mark - Logout

- (void)handleLogout
{
    [SPGAuthenticationManager logout:^(JLTaskStatus taskStatus, NSDictionary *info, NSError *error) {
        
        if(taskStatus == JLTaskStatus_Running)
        {
            UIView *topView = [[[[[UIApplication sharedApplication] delegate] window] rootViewController] view];
            if (topView)
            {
                NSLog(@"showing hud");
                [MBProgressHUD showHUDAddedTo:topView animated:YES];
            }
            
            [self closeDrawerAnimated:YES completion:^(BOOL finished) {
                [(UINavigationController *)[self centerViewController] popToRootViewControllerAnimated:YES];
                [self setDrawerSlideGestureEnabled:NO];
            }];
        }
        else if(taskStatus == JLTaskStatus_Finished)
        {
            UIView *topView = [[[[[UIApplication sharedApplication] delegate] window] rootViewController] view];

            NSLog(@"hiding hud");
            [MBProgressHUD hideAllHUDsForView:topView animated:YES];
        }
    }];
    
    
}

- (void)handleLogin
{
     [self setDrawerSlideGestureEnabled:YES];
}

#pragma mark - Property Accessor Overloads

- (void)setDrawerSlideGestureEnabled:(BOOL)drawerSlideGestureEnabled
{
    _drawerSlideGestureEnabled = drawerSlideGestureEnabled;
    
    if(_drawerSlideGestureEnabled)
    {
        self.openDrawerGestureModeMask = MMOpenDrawerGestureModeAll;
        //self.closeDrawerGestureModeMask = MMCloseDrawerGestureModeAll;
    }
    else
    {
        self.openDrawerGestureModeMask = MMOpenDrawerGestureModeNone;
        //self.closeDrawerGestureModeMask = MMCloseDrawerGestureModeNone;
    }
}
@end
