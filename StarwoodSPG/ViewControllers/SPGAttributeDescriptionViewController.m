//
//  SPGAttributeDescriptionViewController.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/29/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGAttributeDescriptionViewController.h"
#import "SPGStyleManager.h"

@interface SPGAttributeDescriptionViewController ()

@end

@implementation SPGAttributeDescriptionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self styleInterface];
}

- (void)styleInterface
{
    [[self titleLabel] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontHeadline]];
    [[self titleLabel] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal]];
    [[self descriptionLabel] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontBody]];
    [[self descriptionLabel] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(self.selectedAttribute)
    {
        self.title = self.selectedAttribute.name.uppercaseString;
        self.descriptionLabel.text = self.selectedAttribute.categoryDescription;
        CGRect descriptionFrame = self.descriptionLabel.frame;
        [[self descriptionLabel] sizeToFit];
        CGFloat betterSize = self.descriptionLabel.frame.size.height;
        descriptionFrame.size.height = betterSize;
        [[self descriptionLabel] setFrame:descriptionFrame];
        [[SPGAnalyticsManager sharedManager] logEvent:@"Room_Detail_Help_Icon_Clicked" withParameters:@{@"Attribute_Title" : self.title}];
    }
}

@end
