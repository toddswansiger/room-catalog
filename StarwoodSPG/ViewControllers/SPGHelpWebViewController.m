//
//  SPGHelpWebViewController.m
//  StarwoodSPG
//
//  Created by Jeff Soto on 7/11/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGHelpWebViewController.h"

@interface SPGHelpWebViewController ()

@end

@implementation SPGHelpWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.view.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    self.navigationItem.title = @"HELP";
    [self.helpWebView setScalesPageToFit:YES];
    [self.helpWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.starwoodhotels.com/Media/PDF/Mobile/RoomCatalogHelp/StarwoodRoomCatalogHelp.pdf"]]];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

    UIAlertView *helpAlertViewError = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Help is unavailable at this time" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [helpAlertViewError show];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
