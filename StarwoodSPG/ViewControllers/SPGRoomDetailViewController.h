//
//  SPGRoomDetailViewController.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/15/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPGRoomDetailViewController : JLCoreDataTableViewController

@property (nonatomic, strong) SPGRoomSummary* selectedRoom;

@end
