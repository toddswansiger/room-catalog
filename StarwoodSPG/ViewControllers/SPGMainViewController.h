//
//  SPGMainViewController.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/15/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMDrawerController.h"
#import "UINavigationController+MenuNavigation.h"

@interface SPGMainViewController : MMDrawerController

@property (nonatomic, assign) BOOL drawerSlideGestureEnabled;
@property (nonatomic, weak) UINavigationController *rootNavigationController;

@end
