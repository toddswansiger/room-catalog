//
//  SPGOverviewViewController.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/15/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGOverviewViewController.h"
#import "SPGOverviewBuildingCell.h"
#import "SPGRoomListingViewController.h"
#import "SPGStyleManager.h"
#import "DACircularProgressView.h"
#import "UIImage+Tint.h"
#import "SPGSyncStatusEmbededViewController.h"
#import "KSReachability.h"
#import "SPGAppDelegate.h"
#import "UIView+TDViewAdditions.h"
#import "SPGCountdownLabel.h"

@interface SPGOverviewViewController ()

@property (nonatomic, weak) SPGBuilding* selectedBuilding;
@property (nonatomic, assign) BOOL propertyIsDownloaded;
@property (nonatomic, assign) BOOL showPropertyDownloadInterface;
@property (nonatomic) BOOL isDownloadButtonPressed;
@property (nonatomic) CGFloat downloadProgress;
@property (strong, nonatomic) SPGSyncStatusEmbededViewController *syncStatusController;
@property (nonatomic, assign) CGFloat startingPercentageComplete;

@end

@implementation SPGOverviewViewController

- (void)viewDidLoad
{
    // configure core data before calling super
    self.entityName = [[SPGBuilding class] description];
    self.cacheName = @"SPGBuildings";
    self.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"buildingName" ascending:YES selector:@selector(localizedStandardCompare:)]];
    self.cellIdentifier = @"BuildingCell";
    self.filter = nil;
    self.canRefresh = YES;
    
    // setup menu drawer
    [[UIViewController mainViewController] setDrawerSlideGestureEnabled:NO];
    [self.navigationController addMenuButton];
    [self addCustomBackButton];
    [[[self navigationItem] rightBarButtonItem] setTintColor:[UIColor whiteColor]];
    
    [super viewDidLoad];
    
    [self determinePropertyDownloadStatus];
    if ([self propertyIsDownloaded]) {
        [[SPGAnalyticsManager sharedManager] setStartingPercentageComplete:[self determineCatalogProgressStatus] forPropertyID:self.selectedProperty.propertyId];
    }
    [self setShowPropertyDownloadInterface:!self.propertyIsDownloaded];

    [self styleInterface];
    self.pageControl.userInteractionEnabled = NO;
    
    self.title = @"OVERVIEW";
    
    self.view.clipsToBounds = YES;
    // Remove countdown timer on shorter screens hack
    if (self.view.bounds.size.height < 500.0f)
    {
        [[[self syncStatusController] countdownBackgroundView] removeFromSuperview];
        [[self syncStatusController] setCountdownBackgroundView:nil];
    }
}

- (void)styleInterface
{
    [[self propertyTitleLabel] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontHeadline]];
    [[self propertyTitleLabel] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal]];
    [[self propertyTitleView] setBackgroundColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal alpha:.07]];
    
    CALayer *statusLayer = [[self propertyCompletionStatusLabel] layer];
    [statusLayer setCornerRadius:14.0f];
    [statusLayer setMasksToBounds:YES];
    [[self propertyCompletionStatusLabel] setBackgroundColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorIndigo]];
    [[self propertyCompletionStatusLabel] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontSubheadlineTwo]];
    [[self propertyCompletionStatusLabel] setFont:[[SPGStyleManager sharedManager] fontForName:boldCondensedFontName size:14.0f]];
    [[self propertyCompletionStatusLabel] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorWhite]];
    
    [[self propertyDownloadDescriptionTextView] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal]];
    [[self propertyDownloadDescriptionTextView] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontBody]];
    
    UIImage *leftArrow = [[self leftButton] imageForState:UIControlStateNormal];
    leftArrow = [leftArrow imageTintedWithColor:[UIColor blackColor]];
    [[self leftButton] setImage:leftArrow forState:UIControlStateNormal];
    
    UIImage *rightArrow = [[self rightButton] imageForState:UIControlStateNormal];
    rightArrow = [rightArrow imageTintedWithColor:[UIColor blackColor]];
    [[self rightButton] setImage:rightArrow forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updatePropertyData];
    
    [[SPGAnalyticsManager sharedManager] updateCompletedPercentage:[self determineCatalogProgressStatus] forPropertyID:self.selectedProperty.propertyId];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

- (void)updatePropertyData
{
    if(self.selectedProperty)
    {
        self.filter = [NSPredicate predicateWithFormat:@"property.propertyId == %@", self.selectedProperty.propertyId];
        [self resetFetchResultsControllerWithPredicate:self.filter];
        self.propertyTitleLabel.text = self.selectedProperty.propertyName.uppercaseString;
    }
    
    if([self numberOfObjectsForSection:0])
    {
        [self reloadData];
    }
}

- (void)reloadData
{
    [super reloadData];
    [self determineCatalogProgressStatus];
    [self determinePropertyDownloadStatus];
    [self setShowPropertyDownloadInterface:!self.propertyIsDownloaded];
    [self updateNavigationButtons];
}

- (void)didPressBackButton:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)determinePropertyDownloadStatus
{
    BOOL downloaded = [self numberOfObjectsForSection:0] > 0 ? YES : NO;
    self.propertyIsDownloaded = downloaded;
}

- (CGFloat)determineCatalogProgressStatus
{
    if(self.fetchedResultsController.fetchedObjects.count)
    {
        NSArray* buildings = self.fetchedResultsController.fetchedObjects;
        float totalCompletedRooms = [[buildings valueForKeyPath:@"@sum.completedRoomCount"] floatValue];
        float totalRooms = [[buildings valueForKeyPath:@"@sum.roomCount"] floatValue];
        float totalProgress = (totalRooms > 0.0 ? totalCompletedRooms / totalRooms : 0.0);
        float transformedProgress = totalProgress * 100.f;
        float roundedProgress = MIN(100.f, transformedProgress);
        
        //check for fractional part and update the format accordingly
        self.propertyCompletionStatusLabel.text = (roundedProgress == (int)roundedProgress ? [NSString stringWithFormat:@"%0.0f%% PROPERTY COMPLETION", roundedProgress] : [NSString stringWithFormat:@"%0.2f%% PROPERTY COMPLETION", roundedProgress]);
		return roundedProgress;
    }
    return 0.f;
}

#pragma mark - JLCoreDataCollectionViewController

- (void)fetchData
{
    if(self.selectedProperty.propertyId.length && self.isDownloadButtonPressed)
    {
        [SPGPropertyManager getRoomFeaturesForPropertyWithInfo:self.selectedProperty block:^(JLTaskStatus taskStatus, NSDictionary *info, NSError *error) {
            
            switch (taskStatus) {
                case JLTaskStatus_Running:
                {
                    [self dataFetchDidStart];
                    break;
                }
                case JLTaskStatus_Progress:
                {
                    NSNumber* progressNumber = info[@"progress"];
                    self.downloadProgress = (CGFloat)progressNumber.doubleValue;
                    break;
                }
                case JLTaskStatus_Success:
                {
                    // Delay wrappers create a smoother visual effect.
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        [[self downloadButton] setButtonState:SPGProgressButtonStateDownloadComplete];
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            
                            [self dataFetchDidSucceed];
                            [self reloadData];
                            [self styleInterface];
                            [self showNoObjectsAlert];
                            [[SPGAnalyticsManager sharedManager] setStartingPercentageComplete:[self determineCatalogProgressStatus] forPropertyID:self.selectedProperty.propertyId];
                            [[SPGAnalyticsManager sharedManager] updateCompletedPercentage:[self determineCatalogProgressStatus] forPropertyID:self.selectedProperty.propertyId];
                            
                            [[self syncStatusController] setSyncState:SPGSyncStatusStateReadyToSync withAnimation:YES];
                            [[[self syncStatusController] countdownLabel] setTargetDate:[NSDate dateWithTimeIntervalSinceNow:((60 * 60) * 24)]];
 
                        });
                    });

                    break;
                }
                case JLTaskStatus_Error:
                {
                    [self dataFetchDidError:error];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self determinePropertyDownloadStatus];
                        [self setShowPropertyDownloadInterface:!self.propertyIsDownloaded animated:YES];
                        if(!self.propertyIsDownloaded)
                        {
                            [self.downloadButton setButtonState:SPGProgressButtonStateReadyToDownload];
                            [self.downloadButton setProgress:0.0];
                        }
                    });
                    break;
                }
                case JLTaskStatus_Finished:
                {
                    [self dataFetchDidFinish];
                    break;
                }
                default:
                    break;
            }
        }];
    }
}

- (void)showNoObjectsAlert
{
    if ([self numberOfObjectsForSection:0] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Property Details Available"
                                                        message:@"Although we succesfully downloaded this property, we don't currently have any buildings or rooms available to present. This may be a development related issue. Please try to download a different property."
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}

- (void)cancelFetch
{
    [SPGPropertyManager cancelGetRoomFeaturesForPropertyWithInfo:self.selectedProperty];
}

#pragma mark - Actions

- (IBAction)didPressViewRoomsButton:(id)sender
{
    [self performSegueWithIdentifier:@"RoomDetailSegue" sender:nil];
}

- (IBAction)didPressLeftNavigationButton:(id)sender
{
    NSInteger destinationPage = self.pageControl.currentPage - 1;
    [self scrollToPage:destinationPage];
}

- (IBAction)didPressRightNavigationButton:(id)sender
{
    NSInteger destinationPage = self.pageControl.currentPage + 1;
    [self scrollToPage:destinationPage];
}

- (void)scrollToPage:(NSInteger)page
{
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)[[self collectionView] collectionViewLayout];
    UIEdgeInsets insets = [layout sectionInset];
    CGFloat inset = insets.left * 2.0f;
    CGFloat cellWidth = layout.itemSize.width;
    CGFloat cellXSpacing = layout.minimumInteritemSpacing;
    
    CGFloat xOrigin = ((inset + ((cellXSpacing + cellWidth) * (float)page)) - (cellWidth / 2.0f));
    [[self pageControl] setCurrentPage:page];
    [[self collectionView] setContentOffset:CGPointMake(xOrigin, self.collectionView.contentOffset.y) animated:YES];
    [self updateNavigationButtons];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger count = [super collectionView:collectionView numberOfItemsInSection:section];
    self.pageControl.numberOfPages = count;
    [self updateNavigationButtons];
    
    return count;
}

#pragma mark - JLCollectionViewController

- (void)fetchedResultsController:(NSFetchedResultsController *)controller configureCell:(SPGOverviewBuildingCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    SPGBuilding* building = self.fetchedResultsController.fetchedObjects[indexPath.row];
    NSArray* rooms = building.rooms.allObjects;
    CGFloat progress = [building percentComplete];
    NSString* roomsCountStr = [NSString stringWithFormat:@"%0.0f", building.completedRoomCount];//@"0";
    NSString* buildingName = [building.buildingName isEqualToString:kBuildingNameUnknown] ? kBuildingDisplayNameDefault : building.buildingName;
    
    cell.titleLabel.text = buildingName.uppercaseString;
    cell.totalRoomsLabel.text = [NSString stringWithFormat:@"OF %lu", (unsigned long)rooms.count];
    
    cell.roomsCountLabel.text = roomsCountStr;
    [[cell progressView] setProgress:progress];
    cell.viewRoomsButton.tag  = indexPath.row;
    cell.largeViewRoomsButton.tag = indexPath.row;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat yCenter = CGRectGetMidY(scrollView.frame);
    CGFloat xCenter = (scrollView.contentOffset.x + scrollView.frame.size.width / 2.0f);
    CGPoint centerPoint = CGPointMake(xCenter, yCenter);
    NSIndexPath *centerCellPath = [[self collectionView] indexPathForItemAtPoint:centerPoint];
    NSInteger cellIndex = [centerCellPath item];
    [[self pageControl] setCurrentPage:cellIndex];
    [self updateNavigationButtons];
}

- (void)updateNavigationButtons
{
    if (self.pageControl.numberOfPages > 1)
    {
        [[self leftButton] setHidden:NO];
        [[self rightButton] setHidden:NO];
        
        if (self.pageControl.currentPage == 0)
        {
            [[self leftButton] setEnabled:NO];
            [[self rightButton] setEnabled:YES];
        }
        else if (self.pageControl.currentPage == (self.pageControl.numberOfPages - 1)) // Last page
        {
            [[self rightButton] setEnabled:NO];
            [[self leftButton] setEnabled:YES];
        }
        else
        {
            [[self rightButton] setEnabled:YES];
            [[self leftButton] setEnabled:YES];
        }
    }
    else
    {
        [[self leftButton] setHidden:YES];
        [[self rightButton] setHidden:YES];
    }
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton*)sender
{
    if([segue.identifier isEqualToString:@"SPGRoomListingViewController"])
    {
        NSInteger selectedIndex = sender.tag;
        if(selectedIndex < self.fetchedResultsController.fetchedObjects.count)
        {
            self.selectedBuilding = self.fetchedResultsController.fetchedObjects[selectedIndex];
        }

        SPGRoomListingViewController* roomListingView = segue.destinationViewController;
        roomListingView.selectedBuilding = self.selectedBuilding;
        roomListingView.selectedProperty = self.selectedProperty;
        
        if (self.selectedProperty.propertyId && self.selectedBuilding.buildingName)
        {
            [[SPGAnalyticsManager sharedManager] logEvent:@"View_Rooms_Button_Pressed" withParameters:@{
                                                                                                        @"PropertyID": self.selectedProperty.propertyId,
                                                                                                        @"Building_Name" : self.selectedBuilding.buildingName
                                                                                                        }];
        }
    }
    else if ([[segue identifier]isEqualToString:@"SyncStatusSegue"])
    {
        SPGSyncStatusEmbededViewController *embededStatus = [segue destinationViewController];
        [embededStatus setSelectedPropertyID:[[self selectedProperty] propertyId]];
        [self setSyncStatusController:embededStatus];
    }
}

#pragma mark - Property Download

- (void)setShowPropertyDownloadInterface:(BOOL)showPropertyDownloadInterface
{
    [[self propertyCompletionStatusLabel] setHidden:showPropertyDownloadInterface];
    [[self propertyDownloadView] setHidden:!showPropertyDownloadInterface];
    [[[self syncStatusController] view] setHidden:showPropertyDownloadInterface];
    [[self propertyTitleView] setHidden:showPropertyDownloadInterface];    

    self.syncStatusController.view.superview.hidden = showPropertyDownloadInterface;
    
    _showPropertyDownloadInterface = showPropertyDownloadInterface;
}

- (void)setShowPropertyDownloadInterface:(BOOL)showPropertyDownloadInterface animated:(BOOL)animate
{
    if (animate)
    {
        [UIView animateWithDuration:.5 animations:^{
            
            [self setShowPropertyDownloadInterface:showPropertyDownloadInterface];
        }];
    }
    else
    {
        [self setShowPropertyDownloadInterface:showPropertyDownloadInterface];
    }
}

- (void)setDownloadProgress:(CGFloat)downloadProgress
{
    _downloadProgress = downloadProgress;
    self.downloadButton.progress = _downloadProgress;
}

#pragma mark - Sync Status Overlay Delegate

- (void)syncStatusControllerWantsSyncAttempt:(SPGSyncStatusEmbededViewController *)controller
{
    [UIView animateWithDuration:.2
                     animations:^{
                         
                         [controller setSyncState:SPGSyncStatusStateSyncing withAnimation:YES];
                         
                     } completion:^(BOOL finished) {
                         
                         // observer is tracked in viewDidLoad
                         [SPGSyncManager syncRoomFeaturesForPropertyWithID:self.selectedProperty.propertyId block:nil];

                     }];
}

#pragma mark - Download Button Delegate

- (void)progressButtonDidPressBeginDownload:(SPGProgressIndicatorButton *)button
{
    self.isDownloadButtonPressed = YES;
    [self fetchData];
    
    if (self.selectedProperty.propertyId && [[SPGAuthenticationManager user] firstName])
    {
        [[SPGAnalyticsManager sharedManager] logEvent:@"Download_Data_Clicked" withParameters:@{
                                                                                                @"PropertyID": self.selectedProperty.propertyId,
                                                                                                @"User" : [NSString stringWithFormat:@"%@ %@", [SPGAuthenticationManager user].firstName, [SPGAuthenticationManager user].lastName]
                                                                                                }];
    }
}

- (void)progressButtonDidPressCancelDownload:(SPGProgressIndicatorButton *)button
{
}

@end
