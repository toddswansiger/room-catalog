//
//  SPGPropertiesViewController.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/15/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "JLCoreDataTableViewController.h"

@interface SPGPropertiesViewController : JLCoreDataTableViewController

@property (nonatomic, weak) IBOutlet UIView *noPropertiesView;
@property (nonatomic, weak) IBOutlet UITextView *noPropertiesText;
@property (nonatomic, weak) IBOutlet UILabel *noPropertiesHeadlineLabel;

@end
