//
//  SPGUserMenuViewController.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/17/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SPGMainViewController.h"

@class SPGUser;
@class SPGCountdownLabel;
@class SPGVersionAlertEmbededViewController;

@interface SPGUserMenuViewController : UITableViewController

@property (strong, nonatomic) SPGUser *user;
@property (weak, nonatomic) IBOutlet UILabel *helpLabel;
@property (weak, nonatomic) IBOutlet UILabel *logoutLabel;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet SPGCountdownLabel *sessionLabel;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (strong, nonatomic) SPGVersionAlertEmbededViewController *versionUpdateRequiredController;
@property (assign, nonatomic) BOOL showUpdateRequired;


@end
