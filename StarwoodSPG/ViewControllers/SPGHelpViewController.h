//
//  SPGHelpViewController.h
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 6/9/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPGHelpViewController : UIViewController <UIWebViewDelegate>

+ (SPGHelpViewController *)helpController;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UIButton *viewHelpButton;

@end
