//
//  SPGRoomFilterViewController.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/11/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    SPGRoomFilterOption_ShowAll,
    SPGRoomFilterOption_Flagged,
    SPGRoomFilterOption_Incomplete,
    SPGRoomFilterOption_Complete,
    SPGRoomFilterOption_ByFloor,
    SPGRoomFilterOption_ByRoomType
}SPGRoomFilterOption;

@protocol SPGRoomFilterDelegate <NSObject>
@optional
- (void)showFloorFilters;
- (void)showRoomTypeFilters;
- (void)didSelectFilterOption:(SPGRoomFilterOption)option;
- (void)didSelectSubFilterName:(NSString*)name;

- (NSArray*)allFloors;
- (NSArray*)allRoomTypes;
@end

@interface SPGRoomFilterViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) id<SPGRoomFilterDelegate> delegate;
@property (nonatomic) SPGRoomFilterOption selectedOption;

@end
