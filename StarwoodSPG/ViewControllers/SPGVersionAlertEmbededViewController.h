//
//  SPGVersionAlertEmbededViewController.h
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 7/2/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPGVersionAlertEmbededViewController : UIViewController

@property (nonatomic, weak) IBOutlet UILabel *contentLabel;
@property (nonatomic, weak) IBOutlet UILabel *versionLabe;
@property (nonatomic, strong) IBOutlet UITapGestureRecognizer *tapRecognizer;

+ (SPGVersionAlertEmbededViewController *)versionAlertController;
- (IBAction)didTapView:(id)sender;

@end
