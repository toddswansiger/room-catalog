//
//  SPGForgotPasswordViewController.h
//  StarwoodSPG
//
//  Created by Jeff Soto on 7/10/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPGForgotPasswordViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextView *forgotPasswordTextView;

@end
