//
//  SPGUserMenuViewController.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/17/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGUserMenuViewController.h"
#import "SPGStyleManager.h"
#import "UIViewController+SPGAdditions.h"
#import "SPGAppDelegate.h"
#import "SPGStyleManager.h"
#import "SPGMainViewController.h"
#import "SPGCountdownLabel.h"
#import "SPGHelpViewController.h"
#import "SPGVersionAlertEmbededViewController.h"
#import "SPGAuthenticationManager.h"

static NSString *TableReuseIdentifier = @"TableReuseIdentifier";
static CGFloat VersionAlertHeight = 100.0f;

@implementation SPGUserMenuViewController

#pragma Actions

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self nameLabel] setText:[[SPGAuthenticationManager currentUserName] uppercaseString]]; // Attempt to pre-fetch this guy
    [self styleInterface];
    [self showAppUpdateIfNeeded];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self populateContent];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[self sessionLabel] setAnimate:NO];
}

- (void)populateContent
{
    [[self nameLabel] setText:nil];
    [[self sessionLabel] setTargetDate:[[SPGAuthenticationManager sharedInstance] expireDate]];
    [[self sessionLabel] setCountdownPrefixString:@"SESSION TIME REMAINING:"];
    [[self sessionLabel] setAnimate:YES];
    [[self versionLabel] setText:[[[UIViewController appDelegate] versionDescriptionString] uppercaseString]];
    [[self nameLabel] setText:[[SPGAuthenticationManager currentUserName] uppercaseString]];
}

- (void)showAppUpdateIfNeeded
{
    BOOL requiresUpdate = [SPGAuthenticationManager isNewVersionAvailable];
    if (requiresUpdate)
    {
        [self setShowUpdateRequired:YES];
    }
}

- (void)setShowUpdateRequired:(BOOL)showUpdateRequired
{
    if (showUpdateRequired)
    {
        if (![self versionUpdateRequiredController])
        {
            SPGVersionAlertEmbededViewController *versionVC = [SPGVersionAlertEmbededViewController versionAlertController];
            [self setVersionUpdateRequiredController:versionVC];
            [self addChildViewController:versionVC];
            CGFloat yOrigin = (self.footerView.bounds.size.height - VersionAlertHeight);
            CGRect versionRect = CGRectMake(0.0f, yOrigin, self.footerView.bounds.size.width, VersionAlertHeight);
            [[versionVC view] setFrame:versionRect];
        }
        
        [[self footerView] addSubview:[[self versionUpdateRequiredController] view]];
    }
    else
    {
        [[[self versionUpdateRequiredController] view] removeFromSuperview];
    }
}

- (void)styleInterface
{
    [[self tableView] setBackgroundColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorIndigo]];
    [[self view] setBackgroundColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorIndigo]];
    [[self tableView] setSeparatorColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorIndigo alpha:.8]];
    
    [[self versionLabel] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontSubheadlineTwo]];
    
    [[UILabel appearanceWhenContainedIn:[UITableViewCell class], nil] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontBody]];
   
    [[self nameLabel] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontHeadline]];
    [[self footerView] setBackgroundColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorIndigo]];
}

- (void)didPressLogout:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:SPGLogoutNotification object:nil userInfo:[NSDictionary dictionary]];
}

#pragma mark TableView

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath row] == 1)
    {
        SPGHelpViewController *helpVC = [SPGHelpViewController helpController];
        UINavigationController *nav = (UINavigationController *)[(SPGMainViewController *)[self parentViewController] centerViewController];
        [(SPGMainViewController *)[self parentViewController] closeDrawerAnimated:YES completion:nil];

        [nav pushViewController:helpVC animated:NO];
    }
    
    if ([indexPath row] == 2)
    {
        [self didPressLogout:self];
    }
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIColor *backgroundColor;
    NSInteger row = [indexPath row];
    switch (row)
    {
        case 0: // header
        {
            backgroundColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorIndigo];
        }
            break;
        case 1:
        case 2: // actionable & footer
        case 3:
        {
            backgroundColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorIndigo alpha:.95];
        }
            break;
    }
    
    [[cell contentView] setBackgroundColor:backgroundColor];
}

@end
