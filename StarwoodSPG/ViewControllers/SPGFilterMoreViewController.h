//
//  SPGFilterMoreViewController.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/11/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SPGMoreRoomFilterType)
{
    SPGMoreRoomFilterTypeFloor,
    SPGMoreRoomFilterTypeRoomType
};

@protocol SPGFilterMoreDelegate <NSObject>

- (void)filterWithFloor:(NSInteger)floor;
- (void)filterWithRoomType:(NSString *)roomType;

@end

@interface SPGFilterMoreViewController : UIViewController

+ (SPGFilterMoreViewController *)filterMoreViewController;

@property (nonatomic, weak) id<SPGFilterMoreDelegate> delegate;
@property (nonatomic, assign) SPGMoreRoomFilterType filterType;
@property (nonatomic, strong) NSArray *floors;
@property (nonatomic, strong) NSArray *roomTypes;

@end
