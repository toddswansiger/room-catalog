//
//  SPGFilterMoreViewController.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/11/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGFilterMoreViewController.h"

@interface SPGFilterMoreViewController ()

@end

@implementation SPGFilterMoreViewController

+ (SPGFilterMoreViewController *)filterMoreViewController
{
    SPGFilterMoreViewController *moreVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:[[self class] description]];
    return moreVC;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
