//
//  SPGMultiOptionViewController.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/29/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "JLCoreDataTableViewController.h"

@interface SPGMultiOptionViewController : JLCoreDataTableViewController

@property (nonatomic, strong) NSArray *attributeCategories;
@property (nonatomic, strong) SPGAttributeCategory* selectedAttribute;
@property (nonatomic, strong) SPGRoomSummary* selectedRoom;

@end
