//
//  SPGNetworkOperationStatusControllerViewController.m
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 6/19/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGSyncStatusEmbededViewController.h"
#import "SPGSyncManager.h"
#import "SPGStyleManager.h"
#import "UIImage+Tint.h"
#import "KSReachability.h"
#import "UIView+TDViewAdditions.h"
#import "SPGAppDelegate.h"
#import "SPGCountdownLabel.h"
#import "SPGHelpViewController.h"
#import "UIAlertView+Blocks.h"
#import "MBProgressHUD.h"

@interface SPGSyncStatusEmbededViewController ()

@property (assign, nonatomic) BOOL animateStatusIcon;
@property (assign, nonatomic) BOOL reachable;
@property (nonatomic, strong) JLTaskObserver* genericObserver;

@end

@implementation SPGSyncStatusEmbededViewController

+ (SPGSyncStatusEmbededViewController *)statusController
{
    SPGSyncStatusEmbededViewController *syncController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:[[SPGSyncStatusEmbededViewController class] description]];
    return syncController;
}

+ (void)presentSyncRequiredAlert
{
    UIView *presentationView = [[[UIViewController mainViewController] rootNavigationController] view];
    
    RIButtonItem *syncItem = [RIButtonItem itemWithLabel:@"Sync" action:^{
        
        [MBProgressHUD showHUDAddedTo:presentationView animated:YES];
        [SPGSyncManager syncRoomFeaturesForPropertyWithID:nil block:^(JLTaskStatus taskStatus, NSDictionary *info, NSError *error) {
            
            [MBProgressHUD hideAllHUDsForView:presentationView animated:YES];
        }];
    }];
    
    RIButtonItem *helpItem = [RIButtonItem itemWithLabel:@"Help"
                                                  action:^{
                                                      
                                                      SPGHelpViewController *helpVC = [SPGHelpViewController helpController];
                                                      
                                                      UINavigationController *nav = (UINavigationController *)[(SPGMainViewController *)[self mainViewController] centerViewController];
                                                      [(SPGMainViewController *)[self mainViewController] closeDrawerAnimated:YES completion:nil];
                                                      
                                                      [nav pushViewController:helpVC animated:YES];
                                                      
                                                  }];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sync Required"
                                                    message:@"Your property data is out of date. You need to sync now in order to continue cataloging."
                                           cancelButtonItem:nil
                                           otherButtonItems:syncItem, helpItem, nil];
    [alert show];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self styleInterface];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleReachabilityNotification:) name:kDefaultNetworkReachabilityChangedNotification object:nil];
    [self setReachable:[[[UIViewController appDelegate] reachability] reachable]];
    
    self.genericObserver = [SPGSyncManager addGenericObserverForSyncRoomFeaturesWithBlock:^(JLTaskStatus taskStatus, NSDictionary *info, NSError *error) {

            [self handleSyncManagerStatus:taskStatus];
  
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupInitialState];
    [[self countdownLabel] setTargetDate:[SPGSyncManager syncExpiryDate]];
    [[self countdownLabel] setCountdownPrefixString:@"PROPERTY DATA EXPIRES IN:"];
    [[self countdownLabel] setAnimate:YES];
    
    if ([SPGSyncManager syncDurationExpired] && [SPGPropertyManager hasDownloadedPropertyData])
    {
        [SPGSyncStatusEmbededViewController presentSyncRequiredAlert];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //check if being dismissed
    if([self isBeingDismissed] || [self isMovingFromParentViewController])
    {
        [SPGPropertyManager removeObserver:self.genericObserver];
        self.genericObserver = nil;
    }
}

- (BOOL)isVisible
{
    BOOL visible = YES;
    if ([[self navigationController] visibleViewController] != [self parentViewController])
    {
        visible = NO;
    }
    
    return visible;
}

- (void)setupInitialState
{
    if ([self reachable] && ![SPGSyncManager isSyncing])
    {
        [self setSyncState:SPGSyncStatusStateReadyToSync withAnimation:NO];
    }
    else if ([self reachable] && [SPGSyncManager isSyncing])
    {
        [self setSyncState:SPGSyncStatusStateSyncing withAnimation:NO];
    }
    else if (![self reachable])
    {
        [self setSyncState:SPGSyncStatusStateOffline withAnimation:NO];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)handleSyncManagerStatus:(JLTaskStatus)taskStatus
{
    SPGSyncStatusState syncState = SPGSyncStatusStateReadyToSync;
    
    switch (taskStatus)
    {
        case JLTaskStatus_Started:
        case JLTaskStatus_Running:
        case JLTaskStatus_Progress:
            syncState = SPGSyncStatusStateSyncing;
            break;
        
        case JLTaskStatus_Success:
            
            syncState = SPGSyncStatusStateSyncComplete;
            break;
            
        case JLTaskStatus_Error:
            
            syncState = SPGSyncStatusStateSyncFailed;
            break;
            
        case JLTaskStatus_Finished:
            
            syncState = SPGSyncStatusStateSyncComplete;
            break;
            
        default:
            break;
    }
    
    if (syncState != [self syncState])
    {
        [self setSyncState:syncState withAnimation:YES];
    }
}

- (void)handleReachabilityNotification:(NSNotification *)notification
{
    KSReachability *reachability = [notification object];
    [self setReachable:[reachability reachable]];
    if ([self reachable])
    {
        if ([SPGSyncManager isSyncing])
        {
            [self setSyncState:SPGSyncStatusStateSyncing withAnimation:YES];
        }
        else
        {
            [self setSyncState:SPGSyncStatusStateReadyToSync withAnimation:YES];
        }
    }
    else
    {
        [self setSyncState:SPGSyncStatusStateOffline withAnimation:YES];
    }
}

- (void)styleInterface
{
    [[self statusLabel] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontSubheadlineOne]];
    [[self detailLabel] setFont:[[SPGStyleManager sharedManager] fontForName:lightFontName size:10]];
}

- (void)setAnimateStatusIcon:(BOOL)animateStatusIcon
{
    if (animateStatusIcon)
    {
        if ([self isVisible]) // Don't bother animating if we're not visible.
        {
            [self animateStatusIconRotation];
        }
    }
    else
    {
        [[[self imageView] layer] removeAllAnimations];
    }
    
    _animateStatusIcon = animateStatusIcon;
}

- (void)animateStatusIconRotation
{
    CGFloat rotations = -(M_PI_2);
    NSTimeInterval duration = 4.0;
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat:(rotations * 2.0 /* full rotation*/ * duration)];
    rotationAnimation.duration = duration;
    rotationAnimation.repeatCount = 200;
    [self.imageView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

- (IBAction)handleTapRecognizer
{
    if (![SPGSyncManager isSyncing] && !([self syncState] == SPGSyncStatusStateOffline))
    {
        // Observer added in -viewDidLoad
        [SPGSyncManager syncRoomFeaturesForPropertyWithID:[self selectedPropertyID] block:nil];
        
        [[SPGAnalyticsManager sharedManager] logEvent:@"Sync_Button_Pressed" withParameters:@{
                                                                                             @"Screen": NSStringFromClass(self.parentViewController.class)
                                                                                             }];
    }
}

- (NSString *)lastSyncDate
{
    NSDate* syncDate = [SPGSyncManager lastSyncDate] ?: [NSDate distantPast];
    NSString *date = [NSDateFormatter localizedStringFromDate:syncDate dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterShortStyle];
    return [[NSString stringWithFormat:@"Last sync: %@", date] uppercaseString];
}

- (void)setSyncState:(SPGSyncStatusState)syncState
{
    [self setSyncState:syncState withAnimation:NO];
}

- (void)setSyncState:(SPGSyncStatusState)syncState withAnimation:(BOOL)animate
{
    if (![self reachable]) // If we're not reachable, the only state we should see is offline.
    {
        syncState = SPGSyncStatusStateOffline;
    }
    
    UIColor *backgroundColor;
    UIColor *textColor = [UIColor whiteColor];
    BOOL animateSpinner = NO;
    NSString *headlineString;
    NSString *detailString;
    SPGStyleManager *styler = [SPGStyleManager sharedManager];
    UIImage *icon;
    BOOL shouldReset = NO; // After a duration, reset state to 'ready to sync';
    
    switch (syncState)
    {
        case SPGSyncStatusStateReadyToSync:
          
            backgroundColor = [styler colorForElement:SPGElementColorGrey alpha:.1];
            textColor = [styler colorForElement:SPGElementColorStarwoodBlue];
            headlineString = [@"Sync with Valhalla" uppercaseString];
            detailString = [self lastSyncDate];
            icon = [[UIImage imageNamed:@"Sync_Icon"] imageTintedWithColor:[styler colorForElement:SPGElementColorStarwoodBlue]];
            break;
            
        case SPGSyncStatusStateSyncing:
          
            backgroundColor = [styler colorForElement:SPGElementColorGreen];
            icon = [[UIImage imageNamed:@"Sync_Icon"] imageTintedWithColor:[styler colorForElement:SPGElementColorWhite]];
            detailString = [self lastSyncDate];
            headlineString = [@"Syncing" uppercaseString];
            animateSpinner = YES;
            break;
            
        case SPGSyncStatusStateSyncComplete:
          
            backgroundColor = [styler colorForElement:SPGElementColorGreen];
            icon = [[UIImage imageNamed:@"Success_Icon"] imageTintedWithColor:[UIColor whiteColor]];
            headlineString = [@"Sync Complete" uppercaseString];
            detailString = [self lastSyncDate];
            shouldReset = YES;
            break;
            
        case SPGSyncStatusStateSyncFailed:
            
            backgroundColor = [styler colorForElement:SPGElementColorRed];
            icon = [[UIImage imageNamed:@"Failed_Icon"] imageTintedWithColor:[UIColor whiteColor]];
            headlineString = [@"Sync Failed" uppercaseString];
            detailString = [@"There was a problem. Please try again." uppercaseString];
            shouldReset = YES;
            break;
            
        case SPGSyncStatusStateOffline:
            
            backgroundColor = [styler colorForElement:SPGElementColorCharcoal alpha:.4];
            icon = nil;
            headlineString = [@"Sync is Offline" uppercaseString];
            detailString = [@"No Internet Connection Available" uppercaseString];
            break;
            
        default:
            break;
    }
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(resetSyncStateToReady) object:nil];
    
    if (animate)
    {
        [UIView animateWithDuration:.5 animations:^{
           
            [[self view] setBackgroundColor:backgroundColor];
            [[self statusLabel] setTextColor:textColor];
            [[self detailLabel] setTextColor:textColor];
            [[self statusLabel] setText:headlineString];
            [[self detailLabel] setText:detailString];
            [[self imageView] setImage:icon];
            
        } completion:^(BOOL finished) {
            
             [self setAnimateStatusIcon:animateSpinner];
            
            if (shouldReset)
            {
                [self performSelector:@selector(resetSyncStateToReady) withObject:nil afterDelay:3];
            }
        }];
    }
    else
    {
        [[self view] setBackgroundColor:backgroundColor];
        [[self statusLabel] setTextColor:textColor];
        [[self detailLabel] setTextColor:textColor];
        [[self statusLabel] setText:headlineString];
        [[self detailLabel] setText:detailString];
        [[self imageView] setImage:icon];
        [self setAnimateStatusIcon:animateSpinner];
        if (shouldReset)
        {
            [self performSelector:@selector(resetSyncStateToReady) withObject:nil afterDelay:3];

        }
    }
    
    [[self countdownLabel] setTargetDate:[SPGSyncManager syncExpiryDate]];
    
    _syncState = syncState;
}

- (void)resetSyncStateToReady
{
    [self setSyncState:SPGSyncStatusStateReadyToSync withAnimation:YES];
}

@end
