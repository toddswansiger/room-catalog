//
//  SPGRoomDetailViewController.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/15/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGRoomDetailViewController.h"
#import "SPGRoomFeatureYesNoCell.h"
#import "SPGRoomFeatureMultiChoiceCell.h"
#import "SPGMultiOptionViewController.h"
#import "SPGAttributeDescriptionViewController.h"
#import "NSString+StringContents.h"
#import "SPGToggleSwitch.h"
#import "UIImage+Tint.h"
#import "SPGRoomDetailHeaderView.h"
#import "UIViewController+SPGAdditions.h"
#import "UIAlertView+Blocks.h"
#import "SPGChangedAttributesCache.h"
#import "SPGSyncStatusEmbededViewController.h"
#import "SPGRoomAttributeSectionHeader.h"

#define RGBAColor(r,g,b,a) [UIColor colorWithRed:(r/255.0f) green:(g/255.0f) blue:(b/255.0f) alpha:(a)]
#define HEADER_VIEW_HEIGHT 38.0f

static NSString *HeaderViewReuseIdentifier = @"HeaderReuseIdenfitier";

@interface SPGRoomDetailViewController () <SPGToggleSwitchDelegate>

@property (nonatomic, strong) NSArray* selectedRoomFeatureCodes;
@property (nonatomic, strong) UIBarButtonItem* flagButtonItem;
@property (nonatomic, weak) UIButton *flagButton;
@property (nonatomic, weak) IBOutlet UIView *percentCompleteView;
@property (nonatomic, weak) IBOutlet UILabel *percentCompleteLabel;
@property (nonatomic, weak) SPGRoomDetailHeaderView *tableHeaderView;
@property (nonatomic, assign) BOOL roomModified;
@property (nonatomic, assign) BOOL hideLockedAttributes;
@property (nonatomic, strong) SPGSyncStatusEmbededViewController *syncController;
@property (nonatomic, assign) BOOL defaultFilterApplied;
@property (nonatomic, strong) JLTaskObserver* genericUpdateObserver;
@property (nonatomic, strong) JLTaskObserver* genericSyncObserver;

- (NSPredicate *)defaultFilter;
- (NSDictionary *)sortedObjects;

@end

@implementation SPGRoomDetailViewController

- (void)viewDidLoad
{
    [SPGAttributeCategory setCurrentRoom:self.selectedRoom];
    
    // configure core data before calling super
    self.entityName = [[SPGAttributeCategory class] description];
    self.cacheName = @"SPGAttributeCategory";
    self.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedStandardCompare:)]];
    self.cellIdentifier = @"Default";
    self.prefetchRelationship = @[@"featureCodes", @"SPGFeatureCode.categoryCode", @"SPGFeatureCode.codeName"];
    self.filter = self.defaultFilter;
    self.canRefresh = NO;
    self.customSortingUsed = YES;
    self.defaultFilterApplied = YES;
    self.hideLockedAttributes = YES;
    

    [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontSubheadlineTwo]];
    [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal]];
    
    [self addCustomBackButton];
    
    [super viewDidLoad];
    
    [[self percentCompleteLabel] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontSubheadlineTwo]];
    
    SPGRoomDetailHeaderView *headerView = [SPGRoomDetailHeaderView roomDetailHeaderView];
    [[self tableView] setTableHeaderView:headerView];
    [self setTableHeaderView:headerView];
    
    [[self tableView] registerClass:[SPGRoomAttributeSectionHeader class] forHeaderFooterViewReuseIdentifier:HeaderViewReuseIdentifier];

    SPGSyncStatusEmbededViewController *syncController = [SPGSyncStatusEmbededViewController statusController];
    syncController.selectedPropertyID = self.selectedRoom.propertyId;
    [[syncController view] setAutoresizesSubviews:NO];
    [[syncController view] setAutoresizingMask:UIViewAutoresizingNone];
    [self addChildViewController:syncController];
    [self setSyncController:syncController];
    
    UITableViewHeaderFooterView *footerView = [[UITableViewHeaderFooterView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.bounds.size.width, 73.0f)];
    [[syncController view] setFrame:[footerView bounds]];
    [footerView addSubview:[syncController view]];
    [syncController setSyncState:SPGSyncStatusStateReadyToSync withAnimation:NO];
    [[self tableView] setTableFooterView:footerView];
    
    __weak SPGRoomDetailViewController* weakSelf = self;
    self.genericUpdateObserver = [SPGPropertyManager addGenericObserverForUpdateRoomWithBlock:^(JLTaskStatus taskStatus, NSDictionary *info, NSError *error) {
        switch (taskStatus) {
            case JLTaskStatus_Running:
                break;
                
            case JLTaskStatus_Success:
            {
                if([self isVisible])
                {
                    [weakSelf reloadData];
                }
                // sync automatically even if rooms or incomplete
                if(weakSelf.selectedRoom.isCompleted)
                {
                    [weakSelf syncAllRooms];
                    [[SPGAnalyticsManager sharedManager] logEvent:@"Automatic_Sync"];
                }
                break;
            }
            case JLTaskStatus_Error:
                break;
            case JLTaskStatus_Finished:
                break;
                
            default:
                break;
        }
    }];
    
    self.genericSyncObserver = [SPGSyncManager addGenericObserverForSyncRoomFeaturesWithBlock:^(JLTaskStatus taskStatus, NSDictionary *info, NSError *error) {
        switch (taskStatus) {
            case JLTaskStatus_Running:
                break;
            case JLTaskStatus_Success:
                if([weakSelf isVisible])
                {
                    [weakSelf reloadData];
                }
                break;
            case JLTaskStatus_Error:
            {
                break;
            }
            case JLTaskStatus_Finished:
                break;
                
            default:
                break;
        }
    }];
}

- (void)updatePercentCompleteView
{
    BOOL isComplete = (self.selectedRoom.percentCompleted >= 1) ? YES : NO;
    UIColor *backgroundColor = isComplete ? [[SPGStyleManager sharedManager] colorForElement:SPGElementColorGreen] : [[SPGStyleManager sharedManager] colorForElement:SPGElementColorIndigo];
    NSString *percentCompleteString = [NSString stringWithFormat:@"%.0f%% Complete", self.selectedRoom.percentCompleted * 100.0f];
    [[self percentCompleteView] setBackgroundColor:backgroundColor];
    [[self percentCompleteLabel] setText:[percentCompleteString uppercaseString]];
}

- (NSPredicate*)defaultFilter
{
    NSPredicate* defaultFilter = nil;
    if(self.selectedRoom) {
        defaultFilter = [NSPredicate predicateWithFormat:@"property.propertyId == %@", self.selectedRoom.propertyId];
    }
    else
    {
        defaultFilter = [NSPredicate predicateWithValue:TRUE];
    }
    return defaultFilter;
}

- (void)populateContent
{
    NSString *roomTypeString = self.selectedRoom.roomFeature.roomTypeName;
    NSString *roomNumber = self.selectedRoom.roomNum;
    [[[self tableHeaderView] roomTypeLabel] setText:roomTypeString];
    [[[self tableHeaderView] descriptionLabel] setText:@"Room / Suite Type"];
    [self updatePercentCompleteView];
    [self setTitle:roomNumber];
    
    for (SPGAttributeCategory *category in [[self sortedObjects] objectForKey:@0]) {
        
        [[SPGChangedAttributesCache sharedCache] markCategoryCodeDirty:category.categoryCode
                                                            roomNumber:self.selectedRoom.roomNum
                                                            buildingID:self.selectedRoom.building.buildingName
                                                            propertyID:self.selectedRoom.building.property.propertyId
                                                           floorNumber:self.selectedRoom.floorNum];
        
    }
    
    [[SPGChangedAttributesCache sharedCache] persistSet];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setRoomModified:NO];
    
    if(self.selectedRoom)
    {
        [SPGAttributeCategory setCurrentRoom:self.selectedRoom];
        
        self.filter = self.defaultFilter;
        [self resetFetchResultsControllerWithPredicate:self.filter];
    }
    
    if(!self.flagButtonItem)
    {
        self.flagButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Flag" style:UIBarButtonItemStylePlain target:self action:@selector(flagRoom)];
        [self.flagButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateNormal];
        [self.flagButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateHighlighted];
        self.flagButtonItem.tintColor = [UIColor whiteColor];
        
        UIButton *flagButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self setFlagButton:flagButton];
        UIImage *flagImage = [UIImage imageNamed:@"Flag_Icon"];
        UIImage *deselectedFlag = [flagImage imageTintedWithColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorPowder alpha:.6]];
        UIImage *selectedFlag = [flagImage imageTintedWithColor:[UIColor whiteColor]];
        CGRect flagButtonRect = CGRectMake(0.0f, 0.0f, 14.0f, 18.0f);
        [flagButton setFrame:flagButtonRect];
        [flagButton setImage:deselectedFlag forState:UIControlStateNormal];
        [flagButton setImage:selectedFlag forState:UIControlStateSelected];
        [flagButton addTarget:self action:@selector(didPressFlagRoomButton:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *flagItem = [[UIBarButtonItem alloc] initWithCustomView:flagButton];
        [[self navigationItem] setRightBarButtonItem:flagItem];
    }
    
    [[self flagButton] setSelected:[[[self selectedRoom] isFlagged] boolValue]];
    
    if([self numberOfObjectsForSection:0])
    {
        [self reloadData];
        [self performSelector:@selector(reloadData) withObject:nil afterDelay:1.0];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //check if being dismissed
    if([self isBeingDismissed] || [self isMovingFromParentViewController])
    {
        [SPGPropertyManager removeObserver:self.genericUpdateObserver];
        [SPGSyncManager removeObserver:self.genericSyncObserver];
        self.genericUpdateObserver = nil;
        self.genericSyncObserver = nil;
    }
}

- (void)reloadData
{
    [super reloadData];
    
    // must refetch room here since we are not using a fetchedResultsController for this room
    if(self.selectedRoom)
    {
        [self.selectedRoom.managedObjectContext refreshObject:self.selectedRoom mergeChanges:YES];
    }
    
    if(self.selectedRoom.isFlagged.boolValue)
    {
        [self.flagButtonItem setTitle:@"UnFlag"];
    }
    else
    {
        [self.flagButtonItem setTitle:@"Flag"];
    }
    
    [self populateContent];
}


#pragma mark - Custom Sorting

- (NSDictionary*)sortedObjects
{
    NSArray* objects = self.fetchedResultsController.fetchedObjects;
    NSOrderedSet* orderedObjects = self.selectedRoom.building.property.attributeCategories;
    objects = orderedObjects.array;
    
    NSArray* lockedObjects = [objects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isLocked == YES"]];
    NSArray* unlockedObjects = [objects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isLocked == NO"]];
    NSMutableDictionary* sortedObjects = [NSMutableDictionary dictionaryWithCapacity:2];
    
    if(unlockedObjects.count)
        sortedObjects[@(0)] = unlockedObjects;
    
    if(lockedObjects.count)
        sortedObjects[@(1)] = lockedObjects;
    
    return sortedObjects;
}

- (NSIndexPath*)indexPathForTag:(NSInteger)tag
{
    NSInteger section = tag / 1000;
    NSInteger row = tag % 1000;
    return [NSIndexPath indexPathForRow:row inSection:section];
}

- (NSInteger)tagForIndexPath:(NSIndexPath*)indexPath
{
    NSInteger tag = indexPath.section * 1000 + indexPath.row;
    return tag;
}

#pragma mark - JLCoreDataTableViewController

- (void)fetchData
{
    if(self.isRefreshRunning)
    {
        [SPGPropertyManager getRoomFeaturesForPropertyWithInfo:nil block:^(JLTaskStatus taskStatus, NSDictionary *info, NSError *error) {
            
            switch (taskStatus) {
                case JLTaskStatus_Running:
                    [self dataFetchDidStart];
                    break;
                    
                case JLTaskStatus_Success:

                    [self dataFetchDidSucceed];
                    [self reloadData];
                    
                    for (SPGAttributeCategory *category in [[self sortedObjects] objectForKey:@0]) {
                        
                        [[SPGChangedAttributesCache sharedCache] markCategoryCodeDirty:category.categoryCode
                                                                            roomNumber:self.selectedRoom.roomNum
                                                                            buildingID:self.selectedRoom.building.buildingName
                                                                            propertyID:self.selectedRoom.building.property.propertyId
                                                                           floorNumber:self.selectedRoom.floorNum];
                        
                    }
                    
                    [[SPGChangedAttributesCache sharedCache] persistSet];
                    
                    break;
                case JLTaskStatus_Error:

                    [self dataFetchDidError:error];
                    break;
                case JLTaskStatus_Finished:
                    [self dataFetchDidFinish];

                    break;
                    
                default:
                    break;
            }
        }];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HEADER_VIEW_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger number = [self.sortedObjects[@(section)] count];
    if ([self hideLockedAttributes] && section == 1) // 1 is 'locked' attribtues section
    {
        number = 0;
    }
    
    return number;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numberOfSection = self.sortedObjects.count;
    return numberOfSection;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SPGRoomAttributeSectionHeader *headerView = [[self tableView] dequeueReusableHeaderFooterViewWithIdentifier:HeaderViewReuseIdentifier];
    [[headerView disclosureButton] addTarget:self action:@selector(didPressToggleLockedAttributeVisibilityButton:) forControlEvents:UIControlEventTouchUpInside];
    [headerView.contentView setBackgroundColor:[UIColor whiteColor]];
    
    NSString* title = section == 0 ? @"Editable Attributes" : @"Locked Attributes";
    [[headerView titleLabel] setText:title];
    
    [[headerView disclosureButton] setTitle:[self titleForDisclosureLockedAttributesButton] forState:UIControlStateNormal];
    
    if (section == 1)
    {
        [headerView setShowsDisclosureButton:YES];
    }
    else
    {
        [headerView setShowsDisclosureButton:NO];
    }
    
    return headerView;
}

- (NSString *)titleForDisclosureLockedAttributesButton
{
    return [self hideLockedAttributes] ? @"Show" : @"Hide";
}

- (void)didPressToggleLockedAttributeVisibilityButton:(UIButton *)button
{
    [self setHideLockedAttributes:!_hideLockedAttributes];
}

- (void)setHideLockedAttributes:(BOOL)hideLockedAttributes
{
    _hideLockedAttributes = hideLockedAttributes;
    [[self tableView] reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
    if (!hideLockedAttributes)
    {
        [[self tableView] scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SPGRoomFeatureCell* cell = nil;
    NSArray* attributes = self.sortedObjects[@(indexPath.section)];
    if(indexPath.row < attributes.count)
    {
        SPGAttributeCategory* attribute = attributes[indexPath.row];
        
        SPGAttributeMapping* mapping = [self.selectedRoom selectedMappingForAttribute:attribute];
        
        BOOL isCompleted = (mapping != nil);
        BOOL isLocked = [attribute isLocked];
        
        switch (attribute.attributeType) {
            case SPGAttributeType_YesNo:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:[[SPGRoomFeatureYesNoCell class] description]];
                
                SPGToggleSwitch *cellSwitch = [(SPGRoomFeatureYesNoCell*)cell toggleSwitch];
                
                if(mapping)
                {
                    BOOL yesNoState = [mapping.displayText equalToString:@"yes" ignoreCase:YES] ? YES : NO;
                    if (yesNoState)
                    {
                        [cellSwitch setState:(isLocked ? SPGToggleSwitchStateReadOnlyYes : SPGToggleSwitchStateYes) animated:NO];
                    }
                    else
                    {
                        [cellSwitch setState:(isLocked ? SPGToggleSwitchStateReadOnlyNo : SPGToggleSwitchStateNo) animated:NO];
                    }
                }
                else
                {
                    [cellSwitch setState:SPGToggleSwitchStateDefault animated:NO];
                }
                
                
                [cellSwitch setTag:[self tagForIndexPath:indexPath]];
                [cellSwitch setDelegate:self];
                
                break;
            }
            case SPGAttributeType_MultiChoice:
            default:
            {
                
                cell = [tableView dequeueReusableCellWithIdentifier:[[SPGRoomFeatureMultiChoiceCell class] description]];
                
                SPGMultiChoiceCellState cellState = SPGMultiChoiceCellStateDefault;
                
                NSString* displayText = @"Choose";
                if(mapping)
                {
                    displayText = mapping.displayText;
                    cellState = SPGMultiChoiceCellStateSelected;
                    
                }
                
                // BUG: cannot set title properly on disabled UIButton
                // so I must ensure button is enabled before setting title
                cell.chooseButton.enabled = YES;
                [cell.chooseButton setTitle:displayText forState:UIControlStateNormal];
                
                if(isLocked)
                {
                    cell.chooseButton.enabled = NO;
                    cellState = SPGMultiChoiceCellStateLocked;
                }
                else
                {
                    cell.chooseButton.enabled = YES;
                }
                
                [(SPGRoomFeatureMultiChoiceCell *)cell setCellState:cellState];
                
                break;
            }
        }
        
        if (isLocked)
        {
            cell.featureStatus = SPGRoomFeatureStatusLocked;
        }
        else if (isCompleted)
        {
            cell.featureStatus = SPGRoomFeatureStatusComplete;
        }
        else if (!isCompleted)
        {
            cell.featureStatus = SPGRoomFeatureStatusIncomplete;
        }
        
        cell.chooseButton.tag = [self tagForIndexPath:indexPath];
        cell.infoButton.tag = [self tagForIndexPath:indexPath];
        cell.largeInfoButton.tag = [self tagForIndexPath:indexPath];
        cell.titleLabel.text = [[NSString stringWithFormat:@"%@", attribute.name] uppercaseString];
        cell.titleLabel.font = [[SPGStyleManager sharedManager] fontForElement:SPGElementFontSubheadlineTwo];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Flag

- (void)didPressFlagRoomButton:(id)sender
{
    UIButton *button = (UIButton *)sender;
    [button setSelected:![sender isSelected]];
    [self flagRoom];

    [[SPGAnalyticsManager sharedManager] logEvent:@"Flag_Icon_Clicked"];
}

- (void)flagRoom
{
    [SPGPropertyManager flagRoom:self.selectedRoom block:^(JLTaskStatus taskStatus, NSDictionary *info, NSError *error) {
        if(taskStatus == JLTaskStatus_Success)
        {
            [self reloadData];
        }
    }];
}

- (void)didPressBackButton:(id)sender
{
    if ([self roomModifiedButNotCompleted])
    {
        RIButtonItem *exitItem = [RIButtonItem itemWithLabel:@"Leave Room" action:^{
            
            [[self navigationController] popViewControllerAnimated:YES];
        }];
        
        RIButtonItem *cancelItem = [RIButtonItem itemWithLabel:@"Cancel"];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Incomplete Room" message:@"You started cataloging this room, but didn't finish. If you continue, this room will remain incomplete. Press cancel to continue working on this room."
                                                   cancelButtonItem:cancelItem
                                                   otherButtonItems:exitItem, nil];
        [alertView show];
        
    }
    else
    {
        [[self navigationController] popViewControllerAnimated:YES];
    }
}

- (BOOL)roomModifiedButNotCompleted
{
    BOOL modifiedButNotCompleted = NO;
    if (self.selectedRoom.percentCompleted < 1.0f && self.selectedRoom.isModified)
    {
        modifiedButNotCompleted = YES;
    }
    return modifiedButNotCompleted;
}

#pragma mark Toggle Switch

- (void)toggleSwitchDidSelectYes:(SPGToggleSwitch *)toggleSwitch
{
    [self updateSelectedValueForRow:toggleSwitch.tag enabled:YES];
}

- (void)toggleSwitchDidSelectNo:(SPGToggleSwitch *)toggleSwitch
{
    [self updateSelectedValueForRow:toggleSwitch.tag enabled:NO];
}

- (void)toggleSwitchDidReset:(SPGToggleSwitch *)toggleSwitch
{
    [self removeSelectedValueForRow:toggleSwitch.tag];
}

- (void)removeSelectedValueForRow:(NSInteger)row
{
    NSIndexPath* indexPath = [self indexPathForTag:row];
    NSInteger selectedIndex = indexPath.row;
    NSArray* attributes = self.sortedObjects[@(indexPath.section)];
    
    // delete attribute
    SPGAttributeCategory* attribute = attributes[selectedIndex];
    [SPGPropertyManager updateRoom:self.selectedRoom withAttribute:attribute mapping:nil updateType:SPGRoomUpdateType_Delete block:nil];
}

- (void)updateSelectedValueForRow:(NSInteger)row enabled:(BOOL)enabled
{
    NSIndexPath* indexPath = [self indexPathForTag:row];
    NSInteger selectedIndex = indexPath.row;
    NSArray* attributes = self.sortedObjects[@(indexPath.section)];
    
    if(selectedIndex < attributes.count)
    {
        [self setRoomModified:YES];
        
        SPGAttributeCategory* attribute = attributes[selectedIndex];
        
        NSString* displayText = enabled ? @"YES" : @"NO";
        
        NSArray* mappings = [attribute.attributeMapping.allObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"displayText contains[c] %@", displayText]];
        
        SPGAttributeMapping* mapping = mappings.count ? mappings[0] : nil;
        if(mapping)
        {
            NSArray *unlockedObjects = [[self sortedObjects] objectForKey:@0];
            
            for (SPGAttributeCategory *category in unlockedObjects) {
                                
                [[SPGChangedAttributesCache sharedCache] markCategoryCodeDirty:category.categoryCode
                                                                    roomNumber:self.selectedRoom.roomNum
                                                                    buildingID:self.selectedRoom.building.buildingName
                                                                    propertyID:self.selectedRoom.building.property.propertyId
                                                                   floorNumber:self.selectedRoom.floorNum];
                
            }
            
            [[SPGChangedAttributesCache sharedCache] persistSet];
            
            
            [SPGPropertyManager updateRoom:self.selectedRoom withAttribute:attribute mapping:mapping updateType:SPGRoomUpdateType_Add block:nil];
        }
        
    }
}

- (void)syncAllRooms
{
    [SPGSyncManager syncRoomFeaturesForPropertyWithID:self.selectedRoom.roomFeature.roomFeatureSummary.propertyId block:nil];
}

#pragma mark - Segueue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton*)sender
{
    if([segue.identifier isEqualToString:[[SPGMultiOptionViewController class] description]])
    {
        NSIndexPath* indexPath = [self indexPathForTag:sender.tag];
        NSInteger selectedIndex = indexPath.row;
        NSArray* attributes = self.sortedObjects[@(indexPath.section)];
        
        if(selectedIndex < attributes.count)
        {
            SPGAttributeCategory* selectedObject = attributes[selectedIndex];
            SPGMultiOptionViewController* viewController = segue.destinationViewController;
            viewController.attributeCategories = [[self sortedObjects] objectForKey:@0];
            viewController.selectedAttribute = selectedObject;
            viewController.selectedRoom = self.selectedRoom;
        }
    }
    else if([segue.identifier isEqualToString:[[SPGAttributeDescriptionViewController class] description]])
    {
        NSIndexPath* indexPath = [self indexPathForTag:sender.tag];
        NSInteger selectedIndex = indexPath.row;
        NSArray* attributes = self.sortedObjects[@(indexPath.section)];
        
        if(selectedIndex < attributes.count)
        {
            SPGAttributeCategory* selectedObject = attributes[selectedIndex];
            SPGAttributeDescriptionViewController* viewController = segue.destinationViewController;
            viewController.selectedAttribute = selectedObject;
        }
    }
}

@end
