//
//  SPGRoomFilterViewController.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 6/11/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGRoomFilterViewController.h"
#import "SPGRoomFilterOptionCell.h"
#import "SPGRoomFilterMoreCell.h"

@implementation UIView (ImageRepresenation)

- (UIImage *)imageRepresentation
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, [[UIScreen mainScreen] scale]);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

@end

@interface SPGRoomFilterViewController ()

@property (nonatomic, strong) NSArray* filterOptions;
@property (nonatomic, strong) NSArray* filterOptionTitles;

@end

@implementation SPGRoomFilterViewController

- (void)awakeFromNib
{
    [[self view] setBackgroundColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal alpha:.07]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.filterOptions = @[@(SPGRoomFilterOption_ShowAll),
                           @(SPGRoomFilterOption_Flagged),
                           @(SPGRoomFilterOption_Incomplete),
                           @(SPGRoomFilterOption_Complete),
                           @(SPGRoomFilterOption_ByFloor),
                           @(SPGRoomFilterOption_ByRoomType)
                           ];
    
    self.filterOptionTitles = @[@"Show All",
                                @"Flagged",
                                @"Incomplete",
                                @"Complete",
                                @"By Floor",
                                @"By Room Type"
                                ];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.view.clipsToBounds = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(self.selectedOption != SPGRoomFilterOption_ShowAll)
    {
        self.selectedOption = SPGRoomFilterOption_ShowAll;
        [self.tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowCount = 0;
    switch (self.selectedOption) {
        case SPGRoomFilterOption_ByFloor:
            rowCount = [[self delegate] respondsToSelector:@selector(allFloors)] ? [[self delegate] allFloors].count : 0;
            break;
        case SPGRoomFilterOption_ByRoomType:
            rowCount = [[self delegate] respondsToSelector:@selector(allRoomTypes)] ? [[self delegate] allRoomTypes].count : 0;
            break;
        default:
            rowCount = self.filterOptionTitles.count;
            break;
    }
    return rowCount;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(self.selectedOption == SPGRoomFilterOption_ByFloor)
    {
        NSArray* objects = [[self delegate] respondsToSelector:@selector(allFloors)] ? [[self delegate] allFloors] : nil;
        SPGRoomFilterMoreCell* moreCell = [tableView dequeueReusableCellWithIdentifier:@"FilterMoreCell"];
        if(indexPath.row < objects.count)
        {
            NSString* title = objects[indexPath.row];
            moreCell.titleLabel.text = [NSString stringWithFormat:@"  Floor %@", title.length ? title : @"Unknown"];
        }
        return moreCell;
    }
    else if(self.selectedOption == SPGRoomFilterOption_ByRoomType)
    {
        NSArray* objects = [[self delegate] respondsToSelector:@selector(allFloors)] ? [[self delegate] allRoomTypes] : nil;
        SPGRoomFilterMoreCell* moreCell = [tableView dequeueReusableCellWithIdentifier:@"FilterMoreCell"];
        if(indexPath.row < objects.count)
        {
            NSString* title = objects[indexPath.row];
            moreCell.titleLabel.text = title.length ? title : @"Unknown";
        }
        return moreCell;
    }
    else
    {
        SPGRoomFilterOption option = [self.filterOptions[indexPath.row] intValue];
        
        switch (option) {
            case SPGRoomFilterOption_ByFloor:
            case SPGRoomFilterOption_ByRoomType:
            {
                SPGRoomFilterMoreCell* moreCell = [tableView dequeueReusableCellWithIdentifier:@"FilterMoreCell"];
                NSString* title = self.filterOptionTitles[indexPath.row];
                moreCell.titleLabel.text = title;
                return moreCell;
                break;
            }
            default:
            {
                SPGRoomFilterOptionCell* optionCell = [tableView dequeueReusableCellWithIdentifier:@"FilterOptionCell"];
                NSString* title = self.filterOptionTitles[indexPath.row];
                optionCell.titleLabel.text = title;
                return optionCell;
                break;
            }
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.selectedOption == SPGRoomFilterOption_ByFloor)
    {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];

        NSArray* objects = [[self delegate] respondsToSelector:@selector(allFloors)] ? [[self delegate] allFloors] : nil;
        
        NSString* name =  indexPath.row < objects.count ? objects[indexPath.row] : nil;
        if([self.delegate respondsToSelector:@selector(didSelectSubFilterName:)])
        {
            [self.delegate didSelectSubFilterName:name];
        }
    }
    else if(self.selectedOption == SPGRoomFilterOption_ByRoomType)
    {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];

        NSArray* objects = [[self delegate] respondsToSelector:@selector(allRoomTypes)] ? [[self delegate] allRoomTypes] : nil;
        
        NSString* name =  indexPath.row < objects.count ? objects[indexPath.row] : nil;
        if([self.delegate respondsToSelector:@selector(didSelectSubFilterName:)])
        {
            [self.delegate didSelectSubFilterName:name];
        }
    }
    else
    {
        SPGRoomFilterOption option = [self.filterOptions[indexPath.row] intValue];
        
        self.selectedOption = option;
        
        if([self.delegate respondsToSelector:@selector(didSelectFilterOption:)])
        {
            [self.delegate didSelectFilterOption:option];
        }
        
        if(option == SPGRoomFilterOption_ByFloor || option == SPGRoomFilterOption_ByRoomType)
        {
            [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            
            UIImage* tableViewImage = [self.tableView imageRepresentation];
            [self.tableView reloadData];
            
            __block UIImageView* imageView = [[UIImageView alloc] initWithImage:tableViewImage];
            imageView.frame = self.tableView.frame;
            [self.tableView.superview addSubview:imageView];
            __block CGRect tableViewFrameOrig = self.tableView.frame;
            CGRect tableViewFrameNew = tableViewFrameOrig;
            tableViewFrameNew.origin.x += tableViewFrameNew.size.width;
            self.tableView.frame = tableViewFrameNew;
            CGRect imageViewFrame = imageView.frame;
            CGRect imageViewFrameNew = imageViewFrame;
            imageViewFrameNew.origin.x -= imageViewFrameNew.size.width;
            __weak SPGRoomFilterViewController* weakSelf = self;
            
            [UIView animateWithDuration:0.25
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^ {
                                 weakSelf.tableView.frame = tableViewFrameOrig;
                                 imageView.frame = imageViewFrameNew;
                             }
                             completion:^(BOOL finished) {
                                 [imageView removeFromSuperview];
                                 weakSelf.tableView.userInteractionEnabled = YES;
                                 
                             }];
            
        }
    }
}

- (void)showFloorFilterView
{
    [[self delegate] showFloorFilters];
}

- (void)showRoomTypesFilterView
{
    [[self delegate] showRoomTypeFilters];
}


@end
