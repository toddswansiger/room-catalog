//
//  SPGNetworkOperationStatusControllerViewController.h
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 6/19/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SPGSyncStatusState)
{
    SPGSyncStatusStateReadyToSync,
    SPGSyncStatusStateSyncing,
    SPGSyncStatusStateSyncComplete,
    SPGSyncStatusStateSyncFailed,
    SPGSyncStatusStateOffline
};

@class SPGCountdownLabel;

@interface SPGSyncStatusEmbededViewController : UIViewController

@property (assign, nonatomic) SPGSyncStatusState syncState;
@property (strong, nonatomic) NSString *selectedPropertyID;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UILabel *dataExpiresLabel;
@property (weak, nonatomic) IBOutlet SPGCountdownLabel *countdownLabel;
@property (weak, nonatomic) IBOutlet UIView *countdownBackgroundView;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapRecognizer;

+ (SPGSyncStatusEmbededViewController *)statusController;
+ (void)presentSyncRequiredAlert;
- (void)setSyncState:(SPGSyncStatusState)syncState withAnimation:(BOOL)animate;

@end
