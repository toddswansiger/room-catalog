//
//  SPGRoomListingViewController.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/15/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGRoomListingViewController.h"
#import "SPGRoomDetailViewController.h"
#import "SPGRoomFilterViewController.h"
#import "SPGRoomCell.h"
#import "UIImage+Tint.h"
#import <QuartzCore/QuartzCore.h>
#import "SPGSyncStatusEmbededViewController.h"
#import "SPGFilterMoreViewController.h"

#define kFilterViewCoverRatio 0.75
#define kSyncStatusViewHeight 73

// This is defined in Math.h
#define M_PI   3.14159265358979323846264338327950288   /* pi */

// Our conversion definition
#define DEGREES_TO_RADIANS(angle) (angle / 180.0 * M_PI)


@interface SPGRoomListingViewController () <SPGRoomFilterDelegate>

@property (weak, nonatomic) UIColor *completedStatusColor;
@property (weak, nonatomic) IBOutlet UIButton *filterButton;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet SPGRoomFilterViewController* filterViewController;
@property (weak, nonatomic) IBOutlet UIView *filterSearchBarView;
@property (nonatomic) SPGRoomFilterOption selectedOption;
@property (strong, nonatomic) NSString* searchString;
@property ( nonatomic, strong) UIButton *syncButton;
@property (nonatomic) BOOL isShowingFilterView;
@property (nonatomic, assign) BOOL isShowingSyncStatusView;
@property (nonatomic, assign) CGRect originalTableRect;
@property (strong, nonatomic) SPGSyncStatusEmbededViewController *syncStatusViewController;
@property (nonatomic, strong) NSArray *baseSortDescriptors;
@property (nonatomic, weak) IBOutlet UIView *progressHeaderView;
@property (nonatomic, weak) IBOutlet UILabel *progressLabel;

@property (nonatomic, strong) NSArray* allFloorNames;
@property (nonatomic, strong) NSArray* allRoomTypeNames;
@property (nonatomic, copy) NSString* subFilterName;
@property (nonatomic, strong) JLTaskObserver* genericObserver;

@property (nonatomic, strong) NSArray *filteredObjects;
@property (nonatomic, strong) NSCache *completionCache;

- (NSPredicate*)defaultFilter;

@end

@implementation SPGRoomListingViewController

- (void)viewDidLoad
{
    self.completionCache = [[NSCache alloc] init];
    // configure core data before calling super
    self.entityName = [[SPGRoomSummary class] description];
    self.cacheName = @"SPGRoomSummaries";
    self.baseSortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"roomNum" ascending:YES selector:@selector(localizedStandardCompare:)]];
    self.sortDescriptors = self.baseSortDescriptors;
    self.cellIdentifier = @"Default";
    self.filter = self.defaultFilter;
    self.showIndex = NO;
    self.searchField = @"roomNum";
    self.prefetchRelationship = @[@"roomFeature", @"building"];
    self.canRefresh = NO;
    self.customSortingUsed = YES;
    [super viewDidLoad];
    
    self.completedStatusColor = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorGreen];
    
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor whiteColor];
    [self addCustomBackButton];
    
    NSString *propertyName = [[self selectedProperty] propertyName];
    self.title = [propertyName uppercaseString];
    
    [self addSyncButton];
    
    UIColor *blueBackground = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorStarwoodBlue];
    [[[self filterButton] titleLabel] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontBody]];
    [[[self filterButton] titleLabel] setTextColor:[UIColor whiteColor]];
    [[self filterSearchBarView] setBackgroundColor:blueBackground];
    [[self filterButton] setBackgroundColor:blueBackground];
    [[self filterButton] setTitleEdgeInsets:UIEdgeInsetsMake(4.0f, 0.0f, 0.0f, 0.0f)];
    [[self filterSearchBarView] setBackgroundColor:[[SPGStyleManager sharedManager] colorWithRedValue:51 greenValue:110 blueValue:190]];
    [[self filterSearchBarView] setTintColor:[UIColor whiteColor]];
    
    [[self progressHeaderView] setBackgroundColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorIndigo]];
    [[self progressLabel] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontSubheadlineTwo]];
   
    [self styleSearchBar];
    
    self.tableView.scrollsToTop = YES;
    [self.filterButton addTarget:self action:@selector(filterButtonSelected:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
    
    __weak SPGRoomListingViewController* weakSelf = self;
    self.genericObserver = [SPGSyncManager addGenericObserverForSyncRoomFeaturesWithBlock:^(JLTaskStatus taskStatus, NSDictionary *info, NSError *error) {
        
        switch (taskStatus) {
            case JLTaskStatus_Running:
                if(weakSelf.isVisible) {
                    [weakSelf startAnimatingSyncButton];
                }
                break;
            case JLTaskStatus_Success:
                [weakSelf stopAnimatingSyncButton];
                [weakSelf reloadData];
                break;
            case JLTaskStatus_Error:
                break;
            case JLTaskStatus_Finished:
                [weakSelf stopAnimatingSyncButton];
                [weakSelf reloadData];
                break;
                
            default:
                break;
        }
    }];
}


- (NSPredicate*)defaultFilter
{
    NSPredicate* defaultFilter = nil;
    if(self.selectedBuilding)
    {
        defaultFilter = [NSPredicate predicateWithFormat:@"building == %@ && roomFeature.roomFeatureSummary.propertyId == %@", self.selectedBuilding, self.selectedBuilding.property.propertyId];
    }
    else
    {
        defaultFilter = [NSPredicate predicateWithValue:TRUE];
    }
    
    if(self.subFilterName)
    {
        NSPredicate* subFilter = nil;
        
        if(self.selectedOption == SPGRoomFilterOption_ByFloor)
        {
            subFilter = [NSPredicate predicateWithFormat:@"floorNum == %@", self.subFilterName.length ? self.subFilterName : nil];
        }
        else if(self.selectedOption == SPGRoomFilterOption_ByRoomType)
        {
            subFilter = [NSPredicate predicateWithFormat:@"roomFeature.roomTypeName == %@", self.subFilterName.length ? self.subFilterName : nil];

        }
        defaultFilter = [NSCompoundPredicate andPredicateWithSubpredicates:@[defaultFilter, subFilter]];
    }
    return defaultFilter;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(self.selectedBuilding.buildingName.length)
    {
        self.filter = self.defaultFilter;
        [self resetFetchResultsControllerWithPredicate:self.filter];
    }

    [self updateBuildingProgress];
    
    if([SPGSyncManager isSyncing])
    {
        [self startAnimatingSyncButton];
    }
    else
    {
        [self stopAnimatingSyncButton];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //check if being dismissed
    if([self isBeingDismissed] || [self isMovingFromParentViewController])
    {
        [SPGPropertyManager removeObserver:self.genericObserver];
        self.genericObserver = nil;
    }
    [self stopAnimatingSyncButton];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.searchBar resignFirstResponder];
    
    [self stopAnimatingSyncButton];
}

- (CGFloat)tableYOrigin
{
    return (self.progressHeaderView.frame.origin.y + self.progressHeaderView.frame.size.height);
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}

- (void)styleSearchBar
{
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontBody]];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor whiteColor]];
    
    UIColor *blueBackground = [[SPGStyleManager sharedManager] colorForElement:SPGElementColorStarwoodBlue];
    [[self searchBar] setBackgroundColor:blueBackground];
    [[self searchBar] setTintColor:[UIColor whiteColor]];
    [[self searchBar] setPlaceholder:@"Room Number"];
}

- (void)addSyncButton
{
    UIImage *syncImage = [UIImage imageNamed:@"Sync_Icon"];
    syncImage = [syncImage imageTintedWithColor:[UIColor whiteColor]];
    CGRect buttonRect = CGRectMake(0.0f, 0.0f, 24.0f, 18.0f);
    UIButton *syncButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [syncButton setFrame:buttonRect];
    [syncButton setImage:syncImage forState:UIControlStateNormal];
    [syncButton addTarget:self action:@selector(didPressSyncButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *syncItem = [[UIBarButtonItem alloc] initWithCustomView:syncButton];
    [[self navigationItem] setRightBarButtonItem:syncItem];
    self.syncButton = syncButton;
}

- (void)runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat:(rotations * 2.0 /* full rotation*/ * duration)];
    rotationAnimation.duration = duration;
    rotationAnimation.repeatCount = repeat;
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

- (void)startAnimatingSyncButton
{
    UIView* rotateView = self.syncButton.imageView;
    if(!rotateView.layer.animationKeys.count)
    {
        [self runSpinAnimationOnView:rotateView duration:4.0 rotations:-(M_PI_2) repeat:200];
    }
}

- (void)stopAnimatingSyncButton
{
    UIView* rotateView = self.syncButton.imageView;

    if(rotateView.layer.animationKeys.count)
    {
        [rotateView.layer removeAllAnimations];
    }
    self.syncButton.imageView.image = self.syncButton.imageView.image;
}

- (void)syncAllRooms
{
    [self showSyncStatusViewWithAnimation:YES];

    [SPGSyncManager syncRoomFeaturesForPropertyWithID:self.selectedBuilding.property.propertyId block:^(JLTaskStatus taskStatus, NSDictionary *info, NSError *error) {
      
        switch (taskStatus) {
            case JLTaskStatus_Running:
                break;
            case JLTaskStatus_Success:
            {
                [self reloadData];

            }
                break;
            case JLTaskStatus_Error:
            {

            }
                break;
            case JLTaskStatus_Finished:
            {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self hideSyncStatus];
                });
            }
    
                break;
                
            default:
            {

            }
                break;
        }
    }];
}


#pragma mark - JLCoreDataTableViewController

- (void)fetchData
{
    [SPGPropertyManager getRoomFeaturesForPropertyWithInfo:self.selectedBuilding.property block:^(JLTaskStatus taskStatus, NSDictionary *info, NSError *error) {
        
        switch (taskStatus) {
            case JLTaskStatus_Running:
                [self dataFetchDidStart];
                break;
            case JLTaskStatus_Success:
                [self dataFetchDidSucceed];
                [self reloadData];
                break;
            case JLTaskStatus_Error:
                [self dataFetchDidError:error];
                break;
            case JLTaskStatus_Finished:
                [self dataFetchDidFinish];
                break;
                
            default:
                break;
        }
    }];
}

#pragma mark - UITableViewDataSource

- (NSArray*)filteredObjects
{
    if (!_filteredObjects) {
        
        NSArray* filteredObjects = self.fetchedResultsController.fetchedObjects;
        if(self.selectedOption == SPGRoomFilterOption_Complete)
        {
            filteredObjects = [filteredObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isCompleted == YES"]];
        }
        else if(self.selectedOption == SPGRoomFilterOption_Incomplete)
        {
            filteredObjects = [filteredObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isCompleted == NO"]];
        }
        else if(self.selectedOption == SPGRoomFilterOption_Flagged)
        {
            filteredObjects = [filteredObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isFlagged.boolValue == YES"]];
        }
        else if (self.selectedOption == SPGRoomFilterOption_ByFloor)
        {
            
        }
        else if (self.selectedOption == SPGRoomFilterOption_ByRoomType)
        {
            
        }
        
        if(self.searchString.length)
        {
            filteredObjects = [filteredObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(roomNum contains[cd] %@)", self.searchString]];
        }
        
        _filteredObjects = filteredObjects;
    }
    
    return _filteredObjects;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	id<NSFetchedResultsSectionInfo> sectionInfo = [[[self fetchedResultsControllerForTableView:tableView] sections] objectAtIndex:section];
    NSString* title =  [sectionInfo name].length ? [NSString stringWithFormat:@"  %@", [sectionInfo name]] : nil;
    
    if(self.selectedOption == SPGRoomFilterOption_ByFloor)
    {
        title = [NSString stringWithFormat:@"  Floor %@", [sectionInfo name].length ? [sectionInfo name] : @"Unknown"];
    }
    
	return title;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SPGRoomCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
    SPGRoomSummary* roomSummary = nil;
    NSArray* filteredObjects = self.filteredObjects;
    if([[self.fetchedResultsController sections] count] == 1)
    {
        roomSummary = filteredObjects[indexPath.row];
    }
    else
    {
        roomSummary = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
    
    cell.roomNumberLabel.text = roomSummary.roomNum;

    NSNumber *cachedCompletion = [[self completionCache] objectForKey:roomSummary.roomNum] ?: @(roomSummary.isCompleted);
    cell.isComplete = cachedCompletion.boolValue;

    BOOL show = roomSummary.isFlagged.boolValue;
    cell.showFlagIndicator = show;
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	id<NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    NSInteger number = [sectionInfo numberOfObjects];
    if([[self.fetchedResultsController sections] count] == 1)
    {
        number = [[self filteredObjects] count];
    }
	return number;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SPGRoomSummary* roomSummary = nil;
    NSArray* filteredObjects = self.filteredObjects;
    if([[self.fetchedResultsController sections] count] == 1)
    {
        roomSummary = filteredObjects[indexPath.row];
    }
    else
    {
        roomSummary = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
    
    self.selectedRoom = roomSummary;
    if ([self isShowingFilterView])
    {
        [self hideFilterViewWithAnimation:YES];
    }
    else
    {
        [self performSegueWithIdentifier:@"SPGRoomDetailViewController" sender:nil];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Sync View

- (void)showSyncStatusViewWithAnimation:(BOOL)animate
{
    if ([self isShowingSyncStatusView]) return;
    
    if (![self syncStatusViewController])
    {
        SPGSyncStatusEmbededViewController *syncController = [SPGSyncStatusEmbededViewController statusController];
        [self setSyncStatusViewController:syncController];
        [self setOriginalTableRect:self.tableView.frame];
    }
    
    [self addChildViewController:[self syncStatusViewController]];
    
    CGRect syncStartFrame = CGRectMake(0.0f, self.view.bounds.size.height, self.view.bounds.size.width, kSyncStatusViewHeight);
    CGRect endStartFrame = CGRectMake(0.0f, (self.view.bounds.size.height - kSyncStatusViewHeight), self.view.bounds.size.width, kSyncStatusViewHeight);
    CGRect tableFrame = self.originalTableRect;
    tableFrame.size.height = (tableFrame.size.height - kSyncStatusViewHeight);
    
    [[self view] addSubview:[[self syncStatusViewController] view]];
    
    [[self syncStatusViewController] beginAppearanceTransition:YES animated:animate];
    [[self syncStatusViewController] endAppearanceTransition];
    [[[self syncStatusViewController] view] setFrame:syncStartFrame];
    
    if (animate)
    {
        [UIView animateWithDuration:.5
                         animations:^{
                             
                             self.tableView.frame = tableFrame;
                             self.syncStatusViewController.view.frame = endStartFrame;
                             
                         }];
    }
}


- (void)hideSyncStatus
{
    CGRect tableFrame = self.originalTableRect;
    CGRect syncFrame = CGRectMake(0.0f, self.view.bounds.size.height, self.view.bounds.size.width, kSyncStatusViewHeight);
    
    [[self syncStatusViewController] beginAppearanceTransition:NO animated:YES];
    
    [UIView animateWithDuration:.5
                     animations:^{
                         
                         [[[self syncStatusViewController] view] setFrame:syncFrame];
                         [[self tableView] setFrame:tableFrame];
                         
                         
                     } completion:^(BOOL finished) {
                         

                         [[[self syncStatusViewController] view] removeFromSuperview];
                         [[self syncStatusViewController] removeFromParentViewController];
                         [[self syncStatusViewController] endAppearanceTransition];

                         
                         
                         
                     }];
}


- (NSArray*)allFloors
{
    if(!_allFloorNames)
    {
        if(self.selectedOption == SPGRoomFilterOption_ByFloor)
        {
            NSArray *objects = [[[self fetchedResultsController] sections] valueForKey:@"name"];
            _allFloorNames = [objects copy];
        }
    }
    return _allFloorNames;
}

- (NSArray*)allRoomTypes
{
    if(!_allRoomTypeNames)
    {
        if(self.selectedOption == SPGRoomFilterOption_ByRoomType)
        {
            NSArray *objects = [[[self fetchedResultsController] sections] valueForKey:@"name"];
            _allRoomTypeNames = [objects copy];
        }
    }
    return _allRoomTypeNames;
}


- (void)showFilterViewWithAnimation:(BOOL)animate
{
    if(self.isShowingFilterView)
        return;
    
    [self setSearchString:nil];
    [[self searchBar] setText:nil];
    
    [self.searchBar resignFirstResponder];
    
    self.isShowingFilterView = YES;
    
    if(!self.filterViewController)
    {
        [self setOriginalTableRect:self.tableView.frame];
        self.filterViewController = [self.storyboard instantiateViewControllerWithIdentifier:[[SPGRoomFilterViewController class] description]];
        [self.filterViewController willMoveToParentViewController:self];
        [self addChildViewController:self.filterViewController];
        self.view.clipsToBounds = YES;
        self.filterViewController.delegate = self;
        self.filterViewController.tableView.scrollsToTop = NO;
        
        [self.view addSubview:self.filterViewController.view];
    }

    CGFloat filterViewWidth = self.view.frame.size.width * kFilterViewCoverRatio;
 
    CGRect endFrame = CGRectMake(0, [self tableYOrigin], filterViewWidth, self.originalTableRect.size.height);
    CGRect startFrame = CGRectMake(-filterViewWidth, [self tableYOrigin], filterViewWidth, self.originalTableRect.size.height);
   
    
    CGRect mainTableEndFrame = CGRectMake(filterViewWidth, [self tableYOrigin], self.view.frame.size.width, self.tableView.frame.size.height);

    [self.filterViewController beginAppearanceTransition:YES animated:animate];
    [self.filterViewController endAppearanceTransition];

    self.filterViewController.view.frame = startFrame;
    
    if(animate)
    {
        [UIView animateWithDuration:0.3 animations:^{
            
            self.filterViewController.view.frame = endFrame;
            self.tableView.frame = mainTableEndFrame;
            self.filterButton.imageView.transform = CGAffineTransformMakeRotation(M_PI);
            
        } completion:^(BOOL finished) {
            
            UITableViewCell *cell = [[[self filterViewController] tableView] cellForRowAtIndexPath:[NSIndexPath indexPathForItem:self.selectedOption inSection:0]];
            [cell setSelected:YES];
            
        }];
    }
    else
    {
        self.filterViewController.view.frame = endFrame;
        self.tableView.frame = mainTableEndFrame;
    }
}

- (void)hideFilterViewWithAnimation:(BOOL)animate
{
    if(!self.isShowingFilterView)
        return;
    
    self.isShowingFilterView = NO;
    
    CGFloat filterViewWidth = self.view.frame.size.width * kFilterViewCoverRatio;
    CGRect endFrame = CGRectMake(0, [self tableYOrigin], filterViewWidth, self.view.frame.size.height - CGRectGetMaxY(self.filterButton.superview.frame));
    CGRect startFrame = CGRectMake(-filterViewWidth, [self tableYOrigin], filterViewWidth, self.view.frame.size.height - CGRectGetMaxY(self.filterButton.superview.frame));
    CGRect mainTableStartFrame = CGRectMake(0, [self tableYOrigin], self.view.frame.size.width, self.tableView.frame.size.height);
    CGRect mainTableEndFrame = CGRectMake(filterViewWidth, [self tableYOrigin], self.view.frame.size.width, self.tableView.frame.size.height);
    
    [self.filterViewController beginAppearanceTransition:NO animated:animate];

    if(animate)
    {
        [UIView animateWithDuration:0.3 animations:^{
            
            self.filterViewController.view.frame = startFrame;
            self.tableView.frame = mainTableStartFrame;
            self.filterButton.imageView.transform = CGAffineTransformIdentity;
            
        } completion:^(BOOL finished) {
            [self.filterViewController endAppearanceTransition];
        }];
    }
    else
    {
        self.filterViewController.view.frame = endFrame;
        self.tableView.frame = mainTableEndFrame;
        [self.filterViewController endAppearanceTransition];
    }
}

- (void)updateBuildingProgress
{
    float roundedProgress = [[self selectedBuilding] percentComplete] * 100.0f;
    NSString *progressString = (roundedProgress == (int)roundedProgress ? [NSString stringWithFormat:@"%0.0f%% COMPLETE", roundedProgress] : [NSString stringWithFormat:@"%0.2f%% COMPLETE", roundedProgress]);
    
    [[self progressLabel] setText:progressString];
}

#pragma mark - Actions

- (IBAction)filterButtonSelected:(UIButton*)sender
{
    if(!self.isShowingFilterView)
    {
        //reset my filter
        self.subFilterName = nil;
        [self resetFilterForOption:SPGRoomFilterOption_ShowAll];
        [self showFilterViewWithAnimation:YES];
    }
    else
    {
        [self hideFilterViewWithAnimation:YES];
    }
}

- (void)didPressSyncButton:(id)sender
{
    // TODO: Something's not quite right... disabling for build.
    
    if([SPGRoomSummary partiallyCatalogedRooms].count)
    {
        [SPGAlertView showAlertWithTitle:@"Partially cataloged rooms detected" message:@"The data you are about to upload contains some rooms which are partially cataloged. We suggest fully completing these before uploading if possible.\nDo you wish to continue uploading?" completionBlock:^(NSInteger buttonIndex, JLAlertView *alertView) {
         
            if(buttonIndex == 1)
            {
                [self syncAllRooms];
            }
        } cancelButtonTitle:@"Cancel" otherButtonTitles:@"Upload", nil];
    }
    else
    {
        [self syncAllRooms];
    }
    [[SPGAnalyticsManager sharedManager] logEvent:@"Sync_Button_Pressed" withParameters:@{
                                                                                         @"Screen": NSStringFromClass(self.class)
                                                                                         }];
}

#pragma mark search bar delegate
- (void)setSearchString:(NSString *)searchString
{
    if(![_searchString isEqualToString:searchString])
    {
        _searchString = searchString;
        
        NSMutableArray *subPredicates = [NSMutableArray arrayWithObject:self.defaultFilter];
        
        if ([searchString length]) {
            [subPredicates addObject:[NSPredicate predicateWithFormat:@"(roomNum contains[cd] %@)", self.searchString]];
        }
        
        self.filter = [NSCompoundPredicate andPredicateWithSubpredicates:subPredicates];
        
        [self resetFetchResultsControllerWithPredicate:self.filter];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    self.searchString = searchText;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
	[searchBar setText:@""];
}


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self hideFilterViewWithAnimation:YES];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)aSearchBar
{
    //    [self startSearch];
//    [self setSearchString:nil];
    [aSearchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;
{
    [searchBar resignFirstResponder];
}

#pragma mark - SPGRoomFilterDelegate

- (void)resetFilterForOption:(SPGRoomFilterOption)option
{
    self.selectedOption = option;

    switch (option) {
        case SPGRoomFilterOption_ByFloor:
        {
            self.filter = self.defaultFilter;
            self.sectionKeyPath = @"floorNum";
            self.sortDescriptors = [@[[NSSortDescriptor sortDescriptorWithKey:@"floorNum" ascending:YES selector:@selector(localizedStandardCompare:)]] arrayByAddingObjectsFromArray:self.baseSortDescriptors];
            [self resetFetchResultsControllerWithPredicate:self.filter];
            [[SPGAnalyticsManager sharedManager] logEvent:@"Filter_Pressed" withParameters:@{@"Type": @"Floor"}];
            break;
        }
        case SPGRoomFilterOption_ByRoomType:
        {
            self.filter = self.defaultFilter;
            self.sectionKeyPath = @"roomFeature.roomTypeName";
            self.sortDescriptors = [@[[NSSortDescriptor sortDescriptorWithKey:@"roomFeature.roomTypeName" ascending:YES selector:@selector(localizedStandardCompare:)]] arrayByAddingObjectsFromArray:self.baseSortDescriptors];
            [self resetFetchResultsControllerWithPredicate:self.filter];
            [[SPGAnalyticsManager sharedManager] logEvent:@"Filter_Pressed" withParameters:@{@"Type": @"Room_Type"}];
            break;
        }
        case SPGRoomFilterOption_Complete:
        {
            self.filter = self.defaultFilter;
            self.sectionKeyPath = nil;
            self.sortDescriptors = self.baseSortDescriptors;
            [self resetFetchResultsControllerWithPredicate:self.filter];
            [[SPGAnalyticsManager sharedManager] logEvent:@"Filter_Pressed" withParameters:@{@"Type": @"Complete"}];
            break;
        }
        case SPGRoomFilterOption_Incomplete:
        {
            self.filter = self.defaultFilter;
            self.sectionKeyPath = nil;
            self.sortDescriptors = self.baseSortDescriptors;
            [self resetFetchResultsControllerWithPredicate:self.filter];
            [[SPGAnalyticsManager sharedManager] logEvent:@"Filter_Pressed" withParameters:@{@"Type": @"Incomplete"}];
            break;
        }
        case SPGRoomFilterOption_Flagged:
        {
            self.filter = self.defaultFilter;
            self.sectionKeyPath = nil;
            self.sortDescriptors = self.baseSortDescriptors;
            [self resetFetchResultsControllerWithPredicate:self.filter];
            [[SPGAnalyticsManager sharedManager] logEvent:@"Filter_Pressed" withParameters:@{@"Type": @"Flagged"}];
            break;
        }
        case SPGRoomFilterOption_ShowAll:
        default:
        {
            self.filter = self.defaultFilter;
            self.sectionKeyPath = nil;
            self.sortDescriptors = self.baseSortDescriptors;
            [self resetFetchResultsControllerWithPredicate:self.filter];
            break;
        }
    }

}
- (void)didSelectFilterOption:(SPGRoomFilterOption)option
{
    BOOL shouldHideFilterView = (option != SPGRoomFilterOption_ByFloor && option != SPGRoomFilterOption_ByRoomType) ? YES : NO;
    
    if(option != SPGRoomFilterOption_ByFloor && option != SPGRoomFilterOption_ByRoomType)
    {
        //reset subfilter name
        self.subFilterName = nil;
    }
    
    [self resetFilterForOption:option];
    if(shouldHideFilterView)
        [self hideFilterViewWithAnimation:YES];
}

- (void)didSelectSubFilterName:(NSString *)name
{
    BOOL shouldHideFilterView = YES;

    if(self.selectedOption == SPGRoomFilterOption_ByFloor || self.selectedOption == SPGRoomFilterOption_ByRoomType)
    {
        self.subFilterName = name;
        // reset the filterusing above method
        [self didSelectFilterOption:self.selectedOption];
    }
    if(shouldHideFilterView)
        [self hideFilterViewWithAnimation:YES];
}
    
#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    SPGRoomDetailViewController* detailViewController = (SPGRoomDetailViewController*)segue.destinationViewController;
    detailViewController.selectedRoom = self.selectedRoom;
}

#pragma mark -
#pragma mark Helpers
- (NSFetchedResultsController *)fetchedResultsControllerForTableView:(UITableView *)tableView
{
	return tableView == self.tableView ? self.fetchedResultsController : self.searchFetchedResultsController;
}

- (UITableView *)tableViewForFetchedResultsController:(NSFetchedResultsController *)controller
{
	return controller == self.fetchedResultsController ? self.tableView : self.searchDisplayController.searchResultsTableView;
}

#pragma mark - ???

- (void)reloadData
{
    [self setFilteredObjects:nil];
    
    [self.completionCache removeAllObjects];
    
    for (SPGRoomSummary *summary in self.fetchedResultsController.fetchedObjects) {
        [[self completionCache] setObject:@(summary.isCompleted) forKey:summary.roomNum];
    }
    
    [super reloadData];
}

@end
