//
//  SPGLoginViewController.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/15/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

@import CoreText;

#import "SPGLoginViewController.h"
#import "JBKenBurnsView.h"
#import "UIViewController+SPGAdditions.h"
#import "SPGAppDelegate.h"
#import "MBProgressHUD.h"
#import "UIActionSheet+Blocks.h"
#import "KSReachability.h"
#import "SPGAuthenticationManager.h"
#import "SPGVersionAlertEmbededViewController.h"

@interface SPGLoginViewController ()

@property (weak, nonatomic) IBOutlet JBKenBurnsView *slideShowView;
@property (assign, nonatomic) BOOL reachable;

@end

@implementation SPGLoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [self updateVersionNumber];
    [self styleInterface];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleReachabilityChanged:)
                                                 name:kDefaultNetworkReachabilityChangedNotification
                                               object:nil];
    
    [self checkForNewAppVersion];
}

- (void)checkForNewAppVersion
{
    if ([SPGAuthenticationManager isNewVersionAvailable])
    {
        SPGVersionAlertEmbededViewController *versionAlert = [SPGVersionAlertEmbededViewController versionAlertController];
        CGFloat height = 100.0f;
        CGFloat yOrigin = self.view.bounds.size.height - height;
        CGRect versionRect = CGRectMake(0.0f, yOrigin, self.view.bounds.size.width, height);
        [[versionAlert view] setFrame:versionRect];
        [self addChildViewController:versionAlert];
        [[self view] addSubview:[versionAlert view]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [[self slideShowView] stopAnimation];
}

- (void)updateVersionNumber
{
    NSString *versionString = [[UIViewController appDelegate] versionDescriptionString];
    [[self versionLabel] setText:versionString];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)handleReachabilityChanged:(NSNotification *)notification
{
    KSReachability *reachability = [notification object];
    [self setReachable:[reachability reachable]];
}

- (void)styleInterface
{
    SPGStyleManager *style = [SPGStyleManager sharedManager];
    UIFont *headlineFont = [style fontForName:boldCondensedFontName size:32];
    UIFont *lightFont = [style fontForElement:SPGElementFontBody];
    
    [[self headlineLabel] setFont:headlineFont];
    [[self loginDirectionsLabel] setFont:lightFont];
    [[self versionLabel] setFont:lightFont];
    [[self versionLabel] setText:[[UIViewController appDelegate] versionDescriptionString]];
    [[self usernameField] setFont:lightFont];
    [[self passwordField] setFont:lightFont];
    
    NSDictionary *forgotAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                       NSFontAttributeName : lightFont,
                                       NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)};
    
    NSAttributedString *forgottenString = [[NSAttributedString alloc] initWithString:@"Forgotten your password?" attributes:forgotAttributes];
    [[self forgotPasswordButton] setAttributedTitle:forgottenString forState:UIControlStateNormal];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];

    [self setupSlideShow];
    
    [[self usernameField] setText:nil];
    [[self passwordField] setText:nil];
    //[[self environmentLabel] setText:[self nameForEnvironment:[[UIViewController appDelegate] environment]]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //[self flashEnvironmentLabel];
}

- (void)testLogin
{
    self.usernameField.text = @"ttestus1";
    self.passwordField.text = @"proptestusr597";
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (void)setupSlideShow
{
    NSMutableArray *images = [[NSMutableArray alloc] initWithCapacity:5];
    for (int i = 1; i <= 5; i++)
    {
        NSString *name = [NSString stringWithFormat:@"starwood%u.jpg", i];
        UIImage *image = [UIImage imageNamed:name];
        [images addObject:image];
    }

    [[self slideShowView] animateWithImages:images transitionDuration:20 loop:YES isLandscape:YES];
}

# pragma mark - Developer Action Menu

//- (IBAction)didPressDeveloperActionsButton:(id)sender
//{
//    RIButtonItem *populateItem = [RIButtonItem itemWithLabel:@"Populate Credentials" action:^{
//       
//        [self testLogin];
//    }];
//    
//    RIButtonItem *qaItem = [RIButtonItem itemWithLabel:@"QA Environment" action:^{
//       
//        [self switchToEnvironment:SPGEnvironmentQA];
//  
//    }];
//    
//    RIButtonItem *stagingItem = [RIButtonItem itemWithLabel:@"Staging Environment" action:^{
//        
//        [self switchToEnvironment:SPGEnvironmentStaging];
//        
//    }];
//    
//    RIButtonItem *producitonItem = [RIButtonItem itemWithLabel:@"Production Environment" action:^{
//        
//        [self switchToEnvironment:SPGEnvironmentProduction];
//        
//    }];
//    
//    RIButtonItem *cancelItem = [RIButtonItem itemWithLabel:@"Cancel"];
//    
//    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Development Options" cancelButtonItem:cancelItem
//                                           destructiveButtonItem:nil
//                                                otherButtonItems:populateItem, qaItem, stagingItem, producitonItem, nil];
//    [action showInView:[[self navigationController] view]];
//
//}
//
//- (void)resetEnvironment
//{
//    [self flashEnvironmentLabel];
//}
//
//- (void)switchToEnvironment:(SPGEnvironment)environment
//{
//    [self resetEnvironment];
//    [[UIViewController appDelegate] setEnvironment:environment];
//    [[self environmentLabel] setText:[self nameForEnvironment:environment]];
//    [self flashEnvironmentLabel];
//}
//
//- (void)flashEnvironmentLabel
//{
//    [UIView animateWithDuration:.5 animations:^{
//        
//        [[self environmentLabel] setHidden:NO];
//        
//    } completion:^(BOOL finished) {
//       
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            
//            [UIView animateWithDuration:1
//                             animations:^{
//                                 
//                                 [[self environmentLabel] setHidden:YES];
//                                 
//                             }];
//        });
//    }];
//}

- (NSString *)nameForEnvironment:(SPGEnvironment)environment
{
    NSString *string = nil;
    switch (environment)
    {
        case SPGEnvironmentQA:
            string = @"QA Environment";
            break;
            
        case SPGEnvironmentStaging:
            string = @"Staging Environment";
            break;
            
        case SPGEnvironmentProduction:
            string = @"Production Environment";
            break;
    }
    
    return string;
}

#pragma mark - Keyboard Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if ([[self usernameField] text] && [[self passwordField] text])
    {
        [self login:[self loginButton]];
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [[self passwordField] setText:nil];
    return YES;
}

#pragma mark - Action

- (IBAction)handleDismissRecognizer:(id)sender
{
    [[self usernameField] resignFirstResponder];
    [[self passwordField] resignFirstResponder];
}

- (IBAction)login:(UIButton *)sender
{
    if (![self reachable])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Connection"
                                                        message:@"You must be connected to the internet to login."
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    NSString* user = self.usernameField.text;
    NSString* password = self.passwordField.text;
    
    //strip any whitespace and special characters
    user = [user stringByReplacingOccurrencesOfString:@" " withString:@""];
    password = [password stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    //reset text field to new value
    self.usernameField.text = user;
    self.passwordField.text = password;
    
    __weak SPGLoginViewController* weakSelf = self;
    if(![SPGAuthenticationManager isAuthenticationRunning])
    {
        [SPGAuthenticationManager authenticateUser:user withPassword:password block:^(JLTaskStatus taskStatus, NSDictionary *info, NSError *error) {
            
            switch (taskStatus) {
                case JLTaskStatus_Running:
                
                    [MBProgressHUD showHUDAddedTo:[self view] animated:YES];
                    
                    break;
                    
                case JLTaskStatus_Success:
                    
                    [MBProgressHUD hideAllHUDsForView:[self view] animated:YES];
                    if([SPGAuthenticationManager sharedInstance].token.length)
                    {
                        [[UIViewController appDelegate] scheduleTokenExpirationUserNotificationWithExpirationDate:[[SPGAuthenticationManager sharedInstance] expireDate]];
                        [weakSelf performSegueWithIdentifier:@"SPGPropertiesViewController" sender:self];
                        [[NSNotificationCenter defaultCenter] postNotificationName:SPGLoginNotification object:nil];
                        [[SPGAnalyticsManager sharedManager] logEvent:@"User_Login" withParameters:@{
                                                                                                     @"Username" : user
                                                                                                     }];
                    }
                    break;
                
                case JLTaskStatus_Error:
                    [MBProgressHUD hideAllHUDsForView:[self view] animated:YES];
                    break;
                case JLTaskStatus_Finished:
                    [MBProgressHUD hideAllHUDsForView:[self view] animated:YES];
                    break;
                default:
                    [MBProgressHUD hideAllHUDsForView:[self view] animated:YES];
                    break;
            }
        }];
    }
}

@end
