//
//  SPGHelpWebViewController.h
//  StarwoodSPG
//
//  Created by Jeff Soto on 7/11/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPGHelpWebViewController : UIViewController

@property (nonatomic,strong) IBOutlet UIWebView *helpWebView;

@end
