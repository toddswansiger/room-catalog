//
//  SPGVersionAlertEmbededViewController.m
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 7/2/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGVersionAlertEmbededViewController.h"
#import "UIViewController+SPGAdditions.h"
#import "SPGAppDelegate.h"
#import "SPGStyleManager.h"
#import "UIAlertView+Blocks.h"

@interface SPGVersionAlertEmbededViewController ()

@end

@implementation SPGVersionAlertEmbededViewController

+ (SPGVersionAlertEmbededViewController *)versionAlertController
{
    SPGVersionAlertEmbededViewController *alertController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:[[self class] description]];
    return alertController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self versionLabe] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontSubheadlineTwo]];
    [[self contentLabel] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontBody]];
    [[self versionLabe] setText:[[UIViewController appDelegate] versionDescriptionString]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didTapView:(id)sender
{
    NSLog(@"did tap to update version, don't know where to take them.");
}

@end
