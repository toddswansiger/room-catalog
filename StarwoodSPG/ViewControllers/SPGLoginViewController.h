//
//  SPGLoginViewController.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/15/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MRCircularProgressView;

@interface SPGLoginViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UILabel *headlineLabel;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UILabel *loginDirectionsLabel;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *dismissRecognizer;

@property (weak, nonatomic) IBOutlet UIButton *developerOptionsButton;
@property (weak, nonatomic) IBOutlet UILabel *environmentLabel;

- (IBAction)login:(UIButton*)sender;
- (IBAction)handleDismissRecognizer:(id)sender;

@end
