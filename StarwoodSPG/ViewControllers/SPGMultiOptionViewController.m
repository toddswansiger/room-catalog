//
//  SPGMultiOptionViewController.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/29/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGMultiOptionViewController.h"
#import "SPGChangedAttributesCache.h"

@interface SPGMultiOptionViewController ()

@end

@implementation SPGMultiOptionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    // configure core data before calling super
    self.entityName = [[SPGAttributeMapping class] description];
    self.cacheName = @"SPGAttributeMappings";
    self.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"displaySequence" ascending:YES selector:@selector(compare:)]];
    self.cellIdentifier = @"Default";
    self.filter = nil;
    
    self.canRefresh = YES;
    
    [super viewDidLoad];
    
    NSString *title = [[[self selectedAttribute] name] uppercaseString];
    self.title = title;
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(self.selectedAttribute)
    {
        self.filter = [NSPredicate predicateWithFormat:@"attributeCategory.property.propertyId == %@ && attributeCategory.categoryCode == %@", self.selectedAttribute.property.propertyId,  self.selectedAttribute.categoryCode];
        [self resetFetchResultsControllerWithPredicate:self.filter];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.fetchedResultsController.fetchedObjects.count ? self.fetchedResultsController.fetchedObjects.count + 1 : 0;
}

- (void)fetchedResultsController:(NSFetchedResultsController *)controller configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSArray* objects = self.fetchedResultsController.fetchedObjects;
    NSInteger index = indexPath.row - 1;
    if(index < 0)
    {
        cell.textLabel.text = @"Blank";
        if(![self.selectedRoom hasAttributeCategory:self.selectedAttribute])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    else if(index < objects.count)
    {
        SPGAttributeMapping* obj = objects[index];
        cell.textLabel.text = obj.displayText;
        if([self.selectedRoom hasAttributeMapping:obj])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray* objects = self.fetchedResultsController.fetchedObjects;
    
    
    NSInteger index = indexPath.row - 1;
    if(index < 0)
    {
        // delete attribute
        if(self.selectedAttribute)
        {
            for (SPGAttributeCategory *category in self.attributeCategories) {
                                
                [[SPGChangedAttributesCache sharedCache] markCategoryCodeDirty:category.categoryCode
                                                                    roomNumber:self.selectedRoom.roomNum
                                                                    buildingID:self.selectedRoom.building.buildingName
                                                                    propertyID:self.selectedRoom.building.property.propertyId
                                                                   floorNumber:self.selectedRoom.floorNum];
            }
            
            [[SPGChangedAttributesCache sharedCache] persistSet];
            
            
            [SPGPropertyManager updateRoom:self.selectedRoom withAttribute:self.selectedAttribute mapping:nil updateType:SPGRoomUpdateType_Delete block:^(JLTaskStatus taskStatus, NSDictionary *info, NSError *error) {
                switch (taskStatus) {
                    case JLTaskStatus_Running:
                    break;
                    
                    case JLTaskStatus_Success:
                    [self reloadData];
                    break;
                    case JLTaskStatus_Error:
                    break;
                    case JLTaskStatus_Finished:
                    break;
                    
                    default:
                    break;
                }
            }];
        }
    }
    else if(index < objects.count)
    {
        SPGAttributeMapping* mapping = objects[index];
        if(mapping)
        {
            
            [[SPGChangedAttributesCache sharedCache] markCategoryCodeDirty:self.selectedAttribute.categoryCode
                                                                roomNumber:self.selectedRoom.roomNum
                                                                buildingID:self.selectedRoom.building.buildingName
                                                                propertyID:self.selectedRoom.building.property.propertyId
                                                               floorNumber:self.selectedRoom.floorNum];
            
            [SPGPropertyManager updateRoom:self.selectedRoom withAttribute:self.selectedAttribute mapping:mapping updateType:SPGRoomUpdateType_Add block:^(JLTaskStatus taskStatus, NSDictionary *info, NSError *error) {
                switch (taskStatus) {
                    case JLTaskStatus_Running:
                    break;
                    
                    case JLTaskStatus_Success:
                    [self reloadData];
                    break;
                    case JLTaskStatus_Error:
                    break;
                    case JLTaskStatus_Finished:
                    break;
                    
                    default:
                    break;
                }
            }];
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.navigationController popViewControllerAnimated:YES];

}

@end
