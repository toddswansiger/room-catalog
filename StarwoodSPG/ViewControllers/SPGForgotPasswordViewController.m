//
//  SPGForgotPasswordViewController.m
//  StarwoodSPG
//
//  Created by Jeff Soto on 7/10/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGForgotPasswordViewController.h"

@interface SPGForgotPasswordViewController ()

@end

@implementation SPGForgotPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"FORGOT PASSWORD";

    // Do any additional setup after loading the view.
    [[self forgotPasswordTextView] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontBody]];
    [[self forgotPasswordTextView] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
