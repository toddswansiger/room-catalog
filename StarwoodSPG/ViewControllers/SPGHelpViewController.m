//
//  SPGHelpViewController.m
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 6/9/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGHelpViewController.h"
#import "SPGHelpWebViewController.h"

@interface SPGHelpViewController ()

@end

@implementation SPGHelpViewController

+ (SPGHelpViewController *)helpController
{
    SPGHelpViewController *helpController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SPGHelpViewController"];
    helpController.navigationItem.title = @"HELP";
    return helpController;
}

- (IBAction)didPressDone:(id)sender
{
    UIViewController *topViewController = [[[UIViewController mainViewController] rootNavigationController] topViewController];
    [topViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.viewHelpButton addTarget:self action:@selector(showHelpPDF:) forControlEvents:UIControlEventTouchUpInside];
    
	[self styleInterface];
}

- (void)styleInterface
{
    [[self titleLabel] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontHeadline]];
    [[self titleLabel] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal]];
    [[self descriptionLabel] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontBody]];
    [[self descriptionLabel] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(IBAction)showHelpPDF:(id)sender
{
    SPGHelpWebViewController *helpWebVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SPGHelpWebViewController"];
    [[self navigationController] pushViewController:helpWebVC animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
