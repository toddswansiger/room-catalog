//
//  SPGAttributeDescriptionViewController.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/29/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPGAttributeDescriptionViewController : UIViewController

@property (nonatomic, strong) SPGAttributeCategory* selectedAttribute;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
