//
//  SPGOverviewViewController.h
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/15/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPGProgressIndicatorButton.h"

@class SPGProperty;

@interface SPGOverviewViewController : JLCoreDataCollectionViewController

@property (weak, nonatomic) IBOutlet UILabel *propertyTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *propertyTitleView;
@property (weak, nonatomic) IBOutlet UILabel *propertyCompletionStatusLabel;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) SPGProperty* selectedProperty;
@property (weak, nonatomic) IBOutlet UIView *propertyDownloadView;
@property (weak, nonatomic) IBOutlet UITextView *propertyDownloadDescriptionTextView;
@property (weak, nonatomic) IBOutlet SPGProgressIndicatorButton *downloadButton;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;

@end
