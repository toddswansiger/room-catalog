//
//  SPGPropertiesViewController.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/15/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGPropertiesViewController.h"
#import "SPGOverviewViewController.h"

@interface SPGPropertiesViewController ()

@property (nonatomic, assign) BOOL showsNoPropertiesView;

@end

@implementation SPGPropertiesViewController

- (void)viewDidLoad
{
    // configure core data before calling super
    self.entityName = [[SPGProperty class] description];
    self.cacheName = @"SPGProperties";
    self.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"propertyId" ascending:YES selector:@selector(compare:)]];
    self.cellIdentifier = @"Default";
    self.filter = nil;
    self.navigationItem.hidesBackButton = YES;
    
    _showsNoPropertiesView = NO;
    
    [[UILabel appearanceWhenContainedIn:[UITableViewCell class], nil] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontBody]];
    UIView *backgroundView = [[UIView alloc] initWithFrame:self.view.bounds];
    [backgroundView setBackgroundColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorGrey alpha:.15]];
    [[self view] insertSubview:backgroundView belowSubview:[self tableView]];
    [[self view] setBackgroundColor:[UIColor whiteColor]];
    [[self tableView] setBackgroundColor:[UIColor clearColor]];
    
    UIView *tableFooter = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.bounds.size.width, 1.0f)];
    [tableFooter setBackgroundColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorGrey alpha:.15]];
    [[self tableView] setTableFooterView:tableFooter];
    
    // Dev
    self.canRefresh = NO;
    
    [super viewDidLoad];
    
    self.title = @"YOUR PROPERTIES";
    
    [self.navigationController addMenuButton];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation
{
    return UIStatusBarAnimationFade;
}

#pragma mark - JLCoreDataTableViewController

- (void)fetchData
{
    __weak SPGPropertiesViewController* weakSelf = self;

    [SPGPropertyManager getPropertiesForUserWithInfo:nil block:^(JLTaskStatus taskStatus, NSDictionary *info, NSError *error) {
        
        switch (taskStatus) {
            case JLTaskStatus_Running:
                [weakSelf dataFetchDidStart];
                break;
            case JLTaskStatus_Success:
                [weakSelf dataFetchDidSucceed];
                [weakSelf reloadData];
                break;
            case JLTaskStatus_Error:
                [weakSelf dataFetchDidError:error];
                break;
            case JLTaskStatus_Finished:
                [weakSelf dataFetchDidFinish];
                break;
                
            default:
                break;
        }
    }];
}

- (void)reloadData
{
    if([self numberOfObjectsForSection:0] == 0)
    {
        self.canRefresh = YES;
    }
    else
    {
        self.canRefresh = NO;
    }
}

- (void)fetchedResultsController:(NSFetchedResultsController *)controller configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSArray* properties = controller.fetchedObjects;
    
    if(indexPath.row < properties.count)
    {
        SPGProperty* property = properties[indexPath.row];
        NSString *propertyTitle = [property propertyName];
        
        if (!propertyTitle)
        {
            [self setShowsNoPropertiesView:YES];
        }
        
        cell.textLabel.text = propertyTitle;
        cell.tag = indexPath.row;
    }
}

- (void)setShowsNoPropertiesView:(BOOL)showsNoPropertiesView
{
    if (_showsNoPropertiesView == showsNoPropertiesView)
    {
        return;
    }
    
    [[self noPropertiesText] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontBody]];
    [[self noPropertiesText] setTintColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorStarwoodBlue]];
    [[self noPropertiesText] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal]];
    [[self noPropertiesHeadlineLabel] setFont:[[SPGStyleManager sharedManager] fontForElement:SPGElementFontHeadline]];
    [[self noPropertiesHeadlineLabel] setTextColor:[[SPGStyleManager sharedManager] colorForElement:SPGElementColorCharcoal]];
    
    [UIView animateWithDuration:.5
                     animations:^{

                         [[self noPropertiesView] setHidden:!showsNoPropertiesView];
                         
                     }];
    
    _showsNoPropertiesView = showsNoPropertiesView;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor whiteColor]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray* properties = self.fetchedResultsController.fetchedObjects;
    if(indexPath.row < properties.count)
    {
        [self performSegueWithIdentifier:@"SPGOverviewViewController" sender:nil];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Segueue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton*)sender
{
    if([segue.identifier isEqualToString:@"SPGOverviewViewController"])
    {
        NSInteger selectedIndex = [self.tableView indexPathForSelectedRow].row;
        if(selectedIndex < self.fetchedResultsController.fetchedObjects.count)
        {
            SPGProperty* selectedProperty = self.fetchedResultsController.fetchedObjects[selectedIndex];
            SPGOverviewViewController* overviewVC = segue.destinationViewController;
            overviewVC.selectedProperty = selectedProperty;
        }
    }
}

@end

