//
//  SPGAppDelegate.m
//  StarwoodSPG
//
//  Created by Jonathan Lott on 5/15/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGAppDelegate.h"
#import "KSReachability.h"
#import "SPGAuthenticationManager.h"
#import "MBProgressHUD.h"
#import <CoreData+MagicalRecord.h>
#import "SPGManagers.h"
#import "Flurry.h"
#import "SPGChangedAttributesCache.h"
#import "UIAlertView+Blocks.h"
#import "SPGHelpViewController.h"
#import "SPGSyncStatusEmbededViewController.h"

static NSString * const kEnvironmentKey = @"SPGProperties.environmentKey";

@interface SPGAppDelegate () <UIAlertViewDelegate>

@property (weak, nonatomic) UIAlertView *authenticationAlertView;

- (void)resetPersistentStore;

@end

@implementation SPGAppDelegate

@synthesize environment = _environment;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self resetPersistentStore];
    
    [JLDataManager setupDataManagerWithStoreName:kSPGStoreName];
    
    KSReachability *reachability = [KSReachability reachabilityToHost:[self environmentURL]];
    [reachability setOnInitializationComplete:^(KSReachability* reachability) {
    
        [self setReachability:reachability];
        [[NSNotificationCenter defaultCenter] postNotificationName:kDefaultNetworkReachabilityChangedNotification object:reachability];
    }];
    
    [reachability setOnReachabilityChanged:^(KSReachability* reachability) {
        
        [self setReachability:reachability];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kDefaultNetworkReachabilityChangedNotification object:reachability];
            
        });
    }];
    
    [self setEnvironment:SPGEnvironmentProduction];
    //[self setEnvironment:SPGEnvironmentQA];

    UIImage *backImage = [UIImage imageNamed:@"Left-Arrow_Icon"];

    [[UINavigationBar appearance] setBackIndicatorImage:backImage];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:backImage];

    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor clearColor]} forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor clearColor]} forState:UIControlStateHighlighted];
    
    [[UIBarButtonItem appearance] setTitlePositionAdjustment:UIOffsetMake(0, 0) forBarMetrics:UIBarMetricsDefault];
    
    [Flurry setCrashReportingEnabled:YES];
    [Flurry startSession:kFlurryAPIKey];
    
    return YES;
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    if ([notification isEqual:[self authenticationWillExpireWarningNotification]])
    {
        [self handleExpirationWarningNotification];
    }
    else if ([notification isEqual:[self authenticationExpiredNotification]])
    {
        [self handleExpirationNotification];
    }
    else if ([notification isEqual:[self syncDataWillExpireNotification]])
    {
        [SPGSyncStatusEmbededViewController presentSyncRequiredAlert];
    }
}
				
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    if([[SPGAuthenticationManager sharedInstance] token].length && ![[SPGAuthenticationManager sharedInstance] isUserAuthenticated])
    {
        [self handleExpirationNotification];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [[SPGAnalyticsManager sharedManager] logPercentageEvents];

    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [self setAuthenticationExpiredNotification:nil];
    [self setAuthenticationWillExpireWarningNotification:nil];
    [self setSyncDataWillExpireNotification:nil];
    [self resetPersistentStore];
}

- (void)resetPersistentStore
{
    [[SPGChangedAttributesCache sharedCache] clearEverything];
    NSURL *url = [NSPersistentStore MR_urlForStoreName:kSPGStoreName];
    [[NSFileManager defaultManager] removeItemAtURL:url error:NULL];
}

#pragma mark - Application Update

- (void)handleApplicationUpdateRequest
{
    [SPGAlertView showAlertWithTitle:@"Update" message:@"At this point, you should be redirected to the appropriate download location for the new version." completionBlock:^(NSInteger buttonIndex, JLAlertView *alertView) {
        
    } cancelButtonTitle:@"Ok" otherButtonTitles:nil];
}

- (NSString *)versionDescriptionString
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle]infoDictionary];
    NSString *build = infoDictionary[(NSString*)kCFBundleVersionKey];
    NSString *versionString = [NSString stringWithFormat:@"Version %@", build];
    return versionString;
}

#pragma mark - Session Notifications

- (void)scheduleSyncDataExpirationNotificationWithExpirationDate:(NSDate *)expiration
{
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    NSTimeInterval fiveMinutes = -(60 * 5);
    NSDate *warningDate = [expiration dateByAddingTimeInterval:fiveMinutes];
    [notification setFireDate:warningDate];
    [self setSyncDataWillExpireNotification:notification];
    [notification setTimeZone:[NSTimeZone defaultTimeZone]];
    [notification setSoundName:UILocalNotificationDefaultSoundName];
    [notification setAlertAction:@"Sync Data"];
    [notification setAlertBody:@"Your room data hasn't been synchronized in almost 24 hours. In order to continue working and avoid loosing any data you've entered, you must sync in the next few minutes."];
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}

- (void)scheduleTokenExpirationUserNotificationWithExpirationDate:(NSDate *)expiration
{    
    UILocalNotification *willExpireNotification = [[UILocalNotification alloc] init];

    NSTimeInterval fiveMinuteInterval = -(60 * 5);
    NSDate *warningDate = [expiration dateByAddingTimeInterval:fiveMinuteInterval];
    [willExpireNotification setFireDate:warningDate];
    [self setAuthenticationWillExpireWarningNotification:willExpireNotification];
    [willExpireNotification setTimeZone:[NSTimeZone defaultTimeZone]];
    [willExpireNotification setSoundName:UILocalNotificationDefaultSoundName];
    [willExpireNotification setAlertBody:@"Your authentication session will expire in 5 minutes."];
    [[UIApplication sharedApplication] scheduleLocalNotification:willExpireNotification];
    
    UILocalNotification *expireNotification = [[UILocalNotification alloc] init];
    [expireNotification setFireDate:expiration];
    [expireNotification setTimeZone:[NSTimeZone defaultTimeZone]];
    [expireNotification setAlertBody:@"Your authentication session has expired. You will now be logged out."];
    [expireNotification setSoundName:UILocalNotificationDefaultSoundName];
    [self setAuthenticationExpiredNotification:expireNotification];
    [[UIApplication sharedApplication] scheduleLocalNotification:expireNotification];
}

- (void)handleExpirationWarningNotification
{
    NSString* title = @"Authenticate";
    NSString* message = @"Your authentication session will expire in 5 minutes.";
    
    if(![SPGAlertView alertWithTitle:title message:message].visible)
    {
        SPGAlertView* expiredAlert = [[SPGAlertView alloc] initWithTitle:title message:message completionBlock:^(NSInteger buttonIndex, JLAlertView *alertView) {

        } cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        
        [expiredAlert show];
    }
    [[SPGAnalyticsManager sharedManager] logEvent:@"Session_Expiration_Warning"];
}

- (void)handleExpirationNotification
{
    [SPGAlertView showAlertWithTitle:@"Session Expired" message:@"Your authenticated session has expired. You will now be logged-off." completionBlock:^(NSInteger buttonIndex, JLAlertView *alertView) {
        [[NSNotificationCenter defaultCenter] postNotificationName:SPGLogoutNotification object:nil userInfo:[NSDictionary dictionary]];

    } cancelButtonTitle:@"Ok" otherButtonTitles:nil];
   
    [[SPGAnalyticsManager sharedManager] logEvent:@"Session_Expired"];
}

- (UINavigationController *)applicationRootNavigationController
{
    SPGMainViewController *mainVC = (SPGMainViewController *)[[self window] rootViewController];
    return [mainVC rootNavigationController];
}

#pragma mark - AlertView Authenticaion Delegate

- (void)attemptAuthenticationWithAlertView:(UIAlertView *)alertView
{
    NSString *user = [[alertView textFieldAtIndex:0] text];
    NSString *pass = [[alertView textFieldAtIndex:1] text];
    if (user && pass)
    {
        UIView *topView = [[[[[self window] rootViewController] navigationController] topViewController] view];
        if (topView)
        {
            [MBProgressHUD showHUDAddedTo:topView animated:YES];
        }
        
        [SPGAuthenticationManager authenticateUser:user
                                      withPassword:pass
                                             block:^(JLTaskStatus taskStatus, NSDictionary *info, NSError *error) {
                                                 
                                                 [MBProgressHUD hideAllHUDsForView:topView animated:YES];
                                                 
                                                 switch (taskStatus)
                                                 {
                                                     case JLTaskStatus_Success:
                                                     {
                                                         NSTimeInterval oneHour = (60 * 60);
                                                         [self scheduleTokenExpirationUserNotificationWithExpirationDate:[NSDate dateWithTimeIntervalSinceNow:oneHour]];
                                                     }
                                                         break;
                                                         
                                                     case JLTaskStatus_Error:
                                                         [[NSNotificationCenter defaultCenter] postNotificationName:SPGLogoutNotification object:nil userInfo:[NSDictionary dictionary]];
                                                         
                                                         [SPGAlertView showAlertWithTitle:@"Unable to Authenticate" message:@"The attempt to re-authenticate did not succeed. We recommend you sync your work, log off, and then try to login in again." completionBlock:^(NSInteger buttonIndex, JLAlertView *alertView) {
                                                             
                                                         } cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                                                         break;
                                                         
                                                     default:
                                                     {
                                                     }
                                                         
                                                         break;
                                                         
                                                 }
                                                 
                                             }];
    }
}

#pragma mark - Environment

- (SPGEnvironment)environment
{
    if (!_environmentURL && !_environmentAPIKey)
    {
        NSLog(@"kEnvironmentKey = %@", kEnvironmentKey);
        NSInteger environment = [[NSUserDefaults standardUserDefaults] integerForKey:kEnvironmentKey];
        NSLog(@"environment = %ld", (long)environment);
        if ((environment >= SPGEnvironmentStaging) && (environment <= SPGEnvironmentProduction))
        {
            [self setEnvironment:environment];
        }
        else
        {
          [self setEnvironment:SPGEnvironmentQA];
        }
    }
    
    return  _environment;
}

- (void)setEnvironment:(SPGEnvironment)environment
{
    [[NSUserDefaults standardUserDefaults] setInteger:environment forKey:kEnvironmentKey];
    _environmentAPIKey = [self APIKeyForEnvironment:environment];
    _environmentURL = [self environmentURLForEnvironment:environment];
    _environmentSecretKey = [self SecretKeyForEnvironment:environment];

    _environment = environment;
}

- (NSString *)environmentURLForEnvironment:(SPGEnvironment)environmentType
{
    NSString *environment = nil;
    switch (environmentType)
    {
        case SPGEnvironmentQA:
            environment = kQAEnvironmentURL;
            break;
            
        case SPGEnvironmentStaging:
            environment = kStagingEnvironmentURL;
            break;
            
        case SPGEnvironmentProduction:
            environment = kProductionEnvironmentURL;
            break;
    }
    
    return environment;
}

- (NSString *)APIKeyForEnvironment:(SPGEnvironment)environmentType
{
    NSString *key = nil;
    switch (environmentType)
    {
        case SPGEnvironmentQA:
            key = kQAAPIKey;
            break;
            
        case SPGEnvironmentStaging:
            key = kStagingAPIKey;
            break;
            
        case SPGEnvironmentProduction:
            key = kProductionAPIKey;
            break;
    }
    
    return key;
}

- (NSString *)SecretKeyForEnvironment:(SPGEnvironment)environmentType
{
    NSString *key = nil;
    switch (environmentType)
    {
        case SPGEnvironmentQA:
            key = kQASecretKey;
            break;
            
        case SPGEnvironmentStaging:
            key = kStagingSecretKey;
            break;
            
        case SPGEnvironmentProduction:
            key = kProductionSecretKey;
            break;
    }
    
    return key;
}


#pragma mark - MOC

- (NSManagedObjectContext*)managedObjectContext
{
    return [JLDataManager managedObjectContext];
}

#pragma mark - JLTaskManagerErrorHandlingDelegate

- (void)handleTaskManagerError:(SPGError*)error forTaskWithURL:(NSURL*)taskURL
{
    if([SPGAuthenticationManager isTaskInQueueWithURL:taskURL taskInfo:nil])
    {
        //process login errors
        [SPGAlertView showAlertWithError:(SPGError*)error completionBlock:nil];
    }
    else if([SPGSyncManager isTaskInQueueWithURL:taskURL taskInfo:nil])
    {
        [SPGAlertView showAlertWithError:(SPGError*)error completionBlock:nil];
    }
    else if([SPGPropertyManager isTaskInQueueWithURL:taskURL taskInfo:nil])
    {
        [SPGAlertView showAlertWithError:(SPGError*)error completionBlock:nil];
    }
}


@end
