//
//  SPGStyleManager.m
//  StarwoodSPG
//
//  Created by Isaac Schmidt on 5/23/14.
//  Copyright (c) 2014 Tendigi. All rights reserved.
//

#import "SPGStyleManager.h"

#define SPGFont(n,s) [UIFont fontWithName:n size:s]
#define RGBColor(r,g,b) [UIColor colorWithRed:(r/255.0f) green:(g/255.0f) blue:(b/255.0f) alpha:(1.0f)]

NSString * const condensedFontName = @"HelveticaNeueLTPro-Cn";
NSString * const boldCondensedFontName = @"HelveticaNeueLTPro-BdCn";
NSString * const lightFontName = @"HelveticaNeueLTPro-Lt";

@interface SPGStyleManager ()

@property (strong, nonatomic) NSDictionary *colors;
@property (strong, nonatomic) NSDictionary *fonts;

@end

@implementation SPGStyleManager

+ (SPGStyleManager *)sharedManager
{
    static SPGStyleManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        manager = [[SPGStyleManager alloc] init];
    });
    
    return manager;
}

+ (void)listInstalledFonts
{
    for (NSString* family in [UIFont familyNames])
    {
        NSLog(@"%@", family);
        
        for (NSString* name in [UIFont fontNamesForFamilyName: family])
        {
            NSLog(@"  %@", name);
        }
    }
}

- (UIColor *)colorForElement:(SPGElementColor)element
{
    return [[self colors] objectForKey:@(element)];
}

- (UIFont *)fontForElement:(SPGElementFont)element
{
    return [[self fonts] objectForKey:@(element)];
}

- (UIFont *)fontForName:(NSString *)name size:(CGFloat)size
{
    return SPGFont(name, size);
}

- (NSDictionary *)fonts
{    
    if (!_fonts)
    {
        NSDictionary *fonts = @{
                                @(SPGElementFontHeadline) : SPGFont(condensedFontName, 26.0f),
                                @(SPGElementFontSubheadlineOne) : SPGFont(boldCondensedFontName, 16.0f),
                                @(SPGElementFontSubheadlineTwo) : SPGFont(boldCondensedFontName, 12.0f),
                                @(SPGElementFontBody) : SPGFont(lightFontName, 16.0f)
                                };

        _fonts = fonts;
    }
    
    return _fonts;
}

- (NSDictionary *)colors
{
    if (!_colors)
    {
        NSDictionary *colors = @{
                                 @(SPGElementColorStarwoodBlue) : RGBColor(72.0f, 130.0f, 208.0f),
                                 @(SPGElementColorAqua) : RGBColor(154.0f, 216.0f, 255.0f),
                                 @(SPGElementColorIndigo) : RGBColor(13.0f, 41.0f, 63.0f),
                                 @(SPGElementColorWhite) : [UIColor whiteColor],
                                 @(SPGElementColorGrey) : RGBColor(128.0f, 137.0f, 145.0f),
                                 @(SPGElementColorPowder) : RGBColor(220.f, 231.0f, 240.0f),
                                 @(SPGElementColorGreen) : RGBColor(127.0f, 222.0f, 90.0f),
                                 @(SPGElementColorRed) : RGBColor(255.0f, 67.0f, 36.0f),
                                 @(SPGElementColorCharcoal) : RGBColor(70.0f, 76.0f, 80.0f)
                                 };
        
        _colors = colors;
    }
    
    return _colors;
}

- (UIColor *)colorForElement:(SPGElementColor)element alpha:(float)alpha
{
    UIColor *color = [self colorForElement:element];
    return [color colorWithAlphaComponent:alpha];
}

- (UIColor *)colorWithRedValue:(CGFloat)red greenValue:(CGFloat)green blueValue:(CGFloat)blue
{
    return [UIColor colorWithRed:(red / 255.0f) green:(green / 255.0f) blue:(blue / 255.0f) alpha:1.0f];
}

@end
